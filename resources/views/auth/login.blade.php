
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Main CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/main.css') }}">
    <!-- Font-icon css-->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>{{ $_settings->company_name }}</title>
    <link rel="shortcut icon" href="{{ asset('assets/images/brick.png') }}">
  </head>
  <body>
    <section class="material-half-bg">
      <div class="cover"></div>
    </section>
    <section class="login-content">
      <div class="logo">
        <h1>{{ $_settings->company_name }}</h1>
      </div>
      <div class="login-box">
        
        <form method="POST" action="{{ route('login.process') }}" class="login-form">
          @csrf

          <h3 class="login-head"><i class="fa fa-lg fa-fw fa-user"></i>SIGN IN</h3>

          @if (session('error'))
              <div class="alert alert-danger">{{ session('error') }}</div>
          @endif

          <div class="form-group">
            <label class="control-label">USERNAME</label>
            <input type="text" name="user" value="{{ old('user') }}" required autocomplete="user" autofocus class="form-control @error('user') is-invalid @enderror" placeholder="User" autofocus>
            @error('user')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
          </div>
          <div class="form-group">
            <label class="control-label">PASSWORD</label>
            <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password" required autocomplete="current-password">
          </div>
          <div class="form-group btn-container">
            <button class="btn btn-primary btn-block"><i class="fa fa-sign-in fa-lg fa-fw"></i>SIGN IN</button>
          </div>
        </form>
      </div>
    </section>
    <!-- Essential javascripts for application to work-->
    <script src="{{ asset('assets/') }}/js/jquery-3.3.1.min.js"></script>
    <script src="{{ asset('assets/') }}/js/popper.min.js"></script>
    <script src="{{ asset('assets/') }}/js/bootstrap.min.js"></script>
    <script src="{{ asset('assets/') }}/js/main.js"></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src="{{ asset('assets/') }}/js/plugins/pace.min.js"></script>
    <script type="text/javascript">
      // Login Page Flipbox control
      $('.login-content [data-toggle="flip"]').click(function() {
      	$('.login-box').toggleClass('flipped');
      	return false;
      });
    </script>
  </body>
</html>