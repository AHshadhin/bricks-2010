@extends('layouts.master')

@push('css')
    
@endpush

@section('content')

<div class="app-title">
    <div>
        <h1><i class="fa fa-product-hunt"></i> Edit Product</h1>
    </div>
    <div>
        <a href="{{ route('product.index') }}" class="btn btn-dark">
            <i class="fa fa-arrow-left"></i> Back
        </a>
    </div>
</div>
  
<div class="row">
    <div class="col-md-6 offset-md-3">
      <div class="tile">
        <div class="tile-body">
          
            <form action="{{ route('product.update', $product->id) }}" method="post">
                @csrf
                @method('PUT')

                <div class="form-group row">
                    <label class="col-md-3">Product ID <span class="text-danger">*</span> </label>
                    <div class="col-md-9">
                        <input type="text" value="{{ $product->product_id }}" readonly class="form-control">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-3" for="product_name">Product Name <span class="text-danger">*</span> </label>
                    <div class="col-md-9">
                        <input type="text" name="product_name" id="product_name" value="{{ $product->product_name }}" class="form-control">
                        @error('product_name')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-3" for="product_price">Product Price <span class="text-danger">*</span> </label>
                    <div class="col-md-9">
                        <input type="text" name="product_price" id="product_price" value="{{ $product->product_price }}" class="form-control">
                        @error('product_price')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-9 offset-md-3">
                        <input type="submit" value="Update" class="btn btn-info">
                    </div>
                </div>

            </form>

        </div>
      </div>
    </div>
  </div>

@endsection

@push('js')

@endpush