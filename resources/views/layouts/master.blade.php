
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>{{ $_settings->company_name }}</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Main CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/') }}/css/main.css">
    <!-- Font-icon css-->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="{{ asset('assets/') }}/css/style.css">

    <link rel="shortcut icon" href="{{ asset('assets/images/brick.png') }}">

    @stack('css')

  </head>
  <body class="app sidebar-mini">

    <div id="loader" style="background: url({{ asset('assets/images/loader.gif') }}) 50% 50% no-repeat white"></div>

    <!-- Navbar-->
    @include('partial.header')

    <!-- Sidebar menu-->
    @include('partial.sidebar')


    {{-- Main content --}}
    <main class="app-content">
      
        @yield('content')

    </main>

    <!-- Essential javascripts for application to work-->
    <script src="{{ asset('assets') }}/js/jquery-3.3.1.min.js"></script>
    <script src="{{ asset('assets') }}/js/popper.min.js"></script>
    <script src="{{ asset('assets') }}/js/bootstrap.min.js"></script>
    <script src="{{ asset('assets') }}/js/main.js"></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src="{{ asset('assets') }}/js/plugins/pace.min.js"></script>
    <!-- Page specific javascripts-->
    
    <script type="text/javascript">
      $(window).on('load', function(){
          $("#loader").fadeOut("slow");
      });

      setInterval(function() {
          var currentTime = new Date ( );    
          var currentHours = currentTime.getHours ( );   
          var currentMinutes = currentTime.getMinutes ( );   
          var currentSeconds = currentTime.getSeconds ( );
          currentMinutes = ( currentMinutes < 10 ? "0" : "" ) + currentMinutes;   
          currentSeconds = ( currentSeconds < 10 ? "0" : "" ) + currentSeconds;    
          var timeOfDay = ( currentHours < 12 ) ? "AM" : "PM";    
          currentHours = ( currentHours > 12 ) ? currentHours - 12 : currentHours;    
          currentHours = ( currentHours == 0 ) ? 12 : currentHours;    
          var currentTimeString = currentHours + ":" + currentMinutes + ":" + currentSeconds + " " + timeOfDay;
          document.getElementById("clock").innerHTML = currentTimeString;
      }, 1000);
    </script>

    @stack('js')

  </body>
</html>