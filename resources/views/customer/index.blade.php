@extends('layouts.master')

@push('css')
    
@endpush

@section('content')

<div class="app-title">
    <div>
        <h1><i class="fa fa-users"></i> Customers</h1>
    </div>
    <div>
        <a href="{{ route('customer.create') }}" class="btn btn-primary">
            <i class="fa fa-user-plus"></i> Add New Customer
        </a>
    </div>
</div>

@if (session('message'))
    {!! session('message') !!}
@endif
  
<div class="row">
    <div class="col-md-12">
      <div class="tile">
        <div class="tile-body">
          <div class="table-responsive">
            <table class="table table-hover table-bordered" id="sampleTable">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Customer Name</th>
                  <th>Contact Person</th>
                  <th>Phone</th>
                  <th>Status</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($customers as $key => $customer)
                  <tr>
                    <td>{{ ++$key }}</td>
                    <td>{{ $customer->customer_id }}</td>
                    <td>{{ $customer->company_name }}</td>
                    <td>{{ $customer->contact_person }}</td>
                    <td>{{ $customer->phone }}</td>
                    <td>
                        @if ((Auth::user()->isAdmin == 1) && ($customer->isApproved == 0))
                            <a href="{{ route('customer.approve', $customer->id) }}" class="btn btn-warning btn-sm">Approve</a>
                        @elseif ($customer->isApproved == 0)
                            <span class="badge badge-warning">Pending</span>
                        @elseif ($customer->isApproved == 1)
                            <span class="badge badge-success">Approved</span>
                        @endif
                    </td>
                    <td>
                        <div class="btn-group">
                            <button type="button" data-id="{{ $customer->id }}" class="show-modal btn btn-dark btn-sm">
                                <i class="fa fa-eye"></i>
                            </button>
                            <a href="{{ route('customer.edit', $customer->id) }}" class="btn btn-info btn-sm">
                                <i class="fa fa-edit"></i>
                            </a>
                            @can('admin-only', Auth::user())
                            <form action="{{ route('customer.destroy', $customer->id) }}" method="post">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="delete_button btn btn-danger btn-sm"> 
                                    <i class="fa fa-trash"></i> 
                                </button>
                            </form>
                            @endcan
                        </div>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <div class="modal-header p-3">
            <h5 class="modal-title" id="exampleModalLabel">Customer Information</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          
          <div class="my-3 ml-4 mr-4">
            <table class="table table-borderless _table">
                <tr>
                    <th>Customer ID</th>
                    <th>:</th>
                    <td id="c_id"></td>
                </tr>
                <tr>
                    <th>Company Name</th>
                    <th>:</th>
                    <td id="c_customer_name"></td>
                </tr>
                <tr>
                    <th>Contact Person</th>
                    <th>:</th>
                    <td id="c_contact_person"></td>
                </tr>
                <tr>
                    <th>Phone</th>
                    <th>:</th>
                    <td id="c_phone"></td>
                </tr>
                <tr>
                    <th>Area</th>
                    <th>:</th>
                    <td id="c_area"></td>
                </tr>
                <tr>
                    <th>Address</th>
                    <th>:</th>
                    <td id="c_address"></td>
                </tr>
                <tr>
                    <th>Customer Type</th>
                    <th>:</th>
                    <td id="c_customer_type"></td>
                </tr>
                <tr>
                    <th>Marketing Officer</th>
                    <th>:</th>
                    <td id="c_marketing_officer"></td>
                </tr>
                <tr>
                    <th>Saved By</th>
                    <th>:</th>
                    <td id="c_sb"></td>
                </tr>
                <tr>
                    <th>Updated By</th>
                    <th>:</th>
                    <td id="c_ub"></td>
                </tr>
            </table>
          </div>
      </div>
    </div>
  </div>

@endsection

@push('js')
<script type="text/javascript" src="{{ asset('assets/') }}/js/plugins/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="{{ asset('assets/') }}/js/plugins/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="{{ asset('assets/') }}/js/sweetalert2@9.js"></script>
<script type="text/javascript">

    $('#sampleTable').DataTable();

    $(document).on('click', '.show-modal', function() {
        var this_id = $(this).data('id');
        if ('' != this_id) {
            var route = '{{ route('customer.show', ':id') }}';
            var _url = route.replace(':id', this_id);
            $.ajax({
                url: _url,
                method: 'GET',
                success: function(response) {
                    $('#c_id').text(response.customer_id);
                    $('#c_customer_name').text(response.company_name);
                    $('#c_contact_person').text(response.contact_person);
                    $('#c_phone').text(response.phone);
                    $('#c_area').text(response.area);
                    $('#c_address').text(response.address);
                    $('#c_customer_type').text(response.customer_type_name);
                    $('#c_marketing_officer').text(response.marketing_officer.employee_name);
                    if (response.saved_by.username != null) {
                        $('#c_sb').text(response.saved_by.username);
                    } else {
                        $('#c_sb').text(response.saved_by.employee_name);
                    }
                    if (response.updated_by != null) {
                        if (response.updated_by.username != null) {
                            $('#c_ub').text(response.updated_by.username);
                        } else {
                            $('#c_ub').text(response.updated_by.employee_name);
                        }
                    }

                    $('._table').children('tbody').children('tr').children('td').each(function() {
                        if ($(this).text() == '') {
                            $(this).closest('tr').remove();
                        }
                    });

                    $('.bd-example-modal-lg').modal('show');
                }
            });
        }
    });

    $(document).on('click', '.delete_button', function (e) {
        e.preventDefault();
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                $(this).closest('form').submit();
            }
        });
    });
</script>
@endpush