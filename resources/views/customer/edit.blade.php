@extends('layouts.master')

@push('css')
    
@endpush

@section('content')

<div class="app-title">
    <div>
        <h1><i class="fa fa-user-plus"></i> Edit Customer</h1>
    </div>
    <div>
        <a href="{{ route('customer.index') }}" class="btn btn-dark">
            <i class="fa fa-arrow-left"></i> Back
        </a>
    </div>
</div>
  
<div class="row">
    <div class="col-md-12">
      <div class="tile">
        <div class="tile-body">

            <form action="{{ route('customer.update', $customer->id) }}" method="post">
                @csrf
                @method('PUT')

                <div class="row">

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Customer ID</label>
                            <input type="text" value="{{ $customer->customer_id }}" readonly class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="company_name">Company Name <span class="text-danger">*</span> </label>
                            <input type="text" name="company_name" id="company_name" value="{{ $customer->company_name }}" class="form-control">
                            @error('company_name')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="contact_person">Contact Person <span class="text-danger">*</span> </label>
                            <input type="text" name="contact_person" id="contact_person" value="{{ $customer->contact_person }}" class="form-control">
                            @error('contact_person')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="phone">Phone <span class="text-danger">*</span> </label>
                            <input type="text" name="phone" id="phone" value="{{ $customer->phone }}" class="form-control">
                            @error('phone')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="area">Area</label>
                            <input type="text" name="area" id="area" value="{{ $customer->area }}" class="form-control">
                            @error('area')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="address">Address <span class="text-danger">*</span> </label>
                            <textarea name="address" id="address" class="form-control">{{ $customer->address }}</textarea>
                            @error('address')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="customer_type">Customer Type <span class="text-danger">*</span> </label>
                            <select name="customer_type" id="customer_type" class="form-control">
                                <option value="">-- Select Customer Type --</option>
                                @foreach ($customer_types as $c_type)
                                    <option value="{{ $c_type->id }}" {{ $c_type->id == $customer->customer_type_id ? 'selected' : '' }}>{{ $c_type->type }}</option>
                                @endforeach
                            </select>
                            @error('customer_type')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        @can('admin-only', Auth::user())
                            <div class="form-group">
                                <label for="marketing_officer">Marketing Officer <span class="text-danger">*</span> </label>
                                <select name="marketing_officer" id="marketing_officer" class="form-control">
                                    <option value="">-- Select Marketing Officer --</option>
                                    @foreach ($employees as $employee)
                                    <option value="{{ $employee->id }}" {{ $employee->id == $customer->marketing_officer_id ? 'selected' : '' }}>{{ $employee->employee_name }}</option>
                                    @endforeach
                                </select>
                                @error('marketing_officer')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        @endcan
                    </div>
                </div>

                <div class="col-md-6 offset-md-3">
                    <input type="submit" value="Update" class="btn btn-info btn-block">
                </div>

            </form>

        </div>
      </div>
    </div>
  </div>

@endsection

@push('js')
  
@endpush