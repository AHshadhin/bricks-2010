@extends('layouts.master')

@push('css')
    
@endpush

@section('content')

<div class="app-title">
    <div>
        <h1><i class="fa fa-cog"></i> Settings</h1>
    </div>
    <div>
        <a href="{{ route('home') }}" class="btn btn-dark">
            <i class="fa fa-arrow-left"></i> Back
        </a>
    </div>
</div>
  
<div class="row">
    <div class="col-md-12">
      <div class="tile">
        <div class="tile-body">
            
            @if (session('message'))
                {!! session('message') !!}
            @endif
          
            <form action="{{ route('settings.update') }}" method="post">
                @csrf

                <div class="row">
                    <div class="col-md-6">
                        <h3>About Organization</h3><hr>
                        <div class="form-group row">
                            <label class="col-md-3" for="company_name">Name <span class="text-danger">*</span> </label>
                            <div class="col-md-9">
                                <input type="text" name="company_name" id="company_name" value="{{ $settings->company_name }}" class="form-control">
                                @error('company_name')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3" for="phone_1">Phone 1 <span class="text-danger">*</span> </label>
                            <div class="col-md-9">
                                <input type="text" name="phone_1" id="phone_1" value="{{ $settings->phone_1 }}" class="form-control">
                                @error('phone_1')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3" for="phone_2">Phone 2</label>
                            <div class="col-md-9">
                                <input type="text" name="phone_2" id="phone_2" value="{{ $settings->phone_2 }}" class="form-control">
                                @error('phone_2')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3" for="email_1">E-Mail 1 <span class="text-danger">*</span> </label>
                            <div class="col-md-9">
                                <input type="email" name="email_1" id="email_1" value="{{ $settings->email_1 }}" class="form-control">
                                @error('email_1')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3" for="email_2">E-Mail 2</label>
                            <div class="col-md-9">
                                <input type="email" name="email_2" id="email_2" value="{{ $settings->email_2 }}" class="form-control">
                                @error('email_2')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3" for="address">Address</label>
                            <div class="col-md-9">
                                <textarea name="address" id="address" class="form-control">{{ $settings->address }}</textarea>
                                @error('address')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h3>Others</h3><hr>
                        <div class="form-group row">
                            <label class="col-md-3" for="tax">TAX (%) <span class="text-danger">*</span> </label>
                            <div class="col-md-9">
                                <input type="text" name="tax" id="tax" value="{{ $settings->tax }}" class="form-control">
                                @error('tax')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="form-group col-md-6 offset-md-3">
                    <input type="submit" value="Update" class="btn btn-info btn-block">
                </div>

            </form>

        </div>
      </div>
    </div>
  </div>

@endsection

@push('js')

@endpush