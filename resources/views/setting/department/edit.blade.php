@extends('layouts.master')

@push('css')
    
@endpush

@section('content')

<div class="app-title">
    <div>
        <h1><i class="fa fa-user-plus"></i> Edit Department</h1>
    </div>
    <div>
        <a href="{{ route('department.index') }}" class="btn btn-dark">
            <i class="fa fa-arrow-left"></i> Back
        </a>
    </div>
</div>
  
<div class="row">
    <div class="col-md-6 offset-md-3">
      <div class="tile">
        <div class="tile-body">
          
            <form action="{{ route('department.update', $employeeDepartment->id) }}" method="post">
                @csrf
                @method('PUT')

                <div class="form-group row">
                    <label class="col-md-3" for="department_name">Department Name <span class="text-danger">*</span> </label>
                    <div class="col-md-9">
                        <input type="text" name="department_name" id="department_name" value="{{ $employeeDepartment->department_name }}" class="form-control">
                        @error('department_name')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-3" for="department_description">Description </label>
                    <div class="col-md-9">
                        <textarea name="department_description" id="department_description" class="form-control">{{ $employeeDepartment->department_description }}</textarea>
                        @error('department_description')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-9 offset-md-3">
                        <input type="submit" value="Update" class="btn btn-primary">
                    </div>
                </div>

            </form>

        </div>
      </div>
    </div>
  </div>

@endsection

@push('js')

@endpush