@extends('layouts.master')

@push('css')
    
@endpush

@section('content')

<div class="app-title">
    <div>
        <h1><i class="fa fa-user-plus"></i> Add Post</h1>
    </div>
    <div>
        <a href="{{ route('post.index') }}" class="btn btn-dark">
            <i class="fa fa-arrow-left"></i> Back
        </a>
    </div>
</div>
  
<div class="row">
    <div class="col-md-6 offset-md-3">
      <div class="tile">
        <div class="tile-body">
          
            <form action="{{ route('post.store') }}" method="post">
                @csrf

                <div class="form-group row">
                    <label class="col-md-3" for="post_name">Post Name <span class="text-danger">*</span> </label>
                    <div class="col-md-9">
                        <input type="text" name="post_name" id="post_name" value="{{ old('post_name') }}" class="form-control">
                        @error('post_name')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-3" for="post_description">Description </label>
                    <div class="col-md-9">
                        <textarea name="post_description" id="post_description" class="form-control">{{ old('post_description') }}</textarea>
                        @error('post_description')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-9 offset-md-3">
                        <input type="submit" value="Submit" class="btn btn-info">
                    </div>
                </div>

            </form>

        </div>
      </div>
    </div>
  </div>

@endsection

@push('js')

@endpush