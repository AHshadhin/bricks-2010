@extends('layouts.master')

@push('css')
<style>
    table tr td .form-control {
        border: 1px solid black;
        height: unset;
        padding: unset;
        padding: 3px 5px;
        border-radius: unset;
    }
</style>
@endpush

@section('content')

<div class="app-title">
    <div>
        <h1><i class="fa fa-product-hunt"></i> Delivery</h1>
    </div>
    <div>
        <a href="{{ route('orders') }}" class="btn btn-dark">
            <i class="fa fa-arrow-left"></i> Back
        </a>
    </div>
</div>
  
<div class="row">
    <div class="col-md-10 offset-md-1">
      <div class="tile">
        <div class="tile-body">

            <div class="row">
                <div class="col-md-6">
                    <h4>Customer Information</h4><hr>
                    <table class="table table-borderless">
                        <tr>
                            <th>Customer Id</th>
                            <th>:</th>
                            <td>{{ $order->customer->customer_id }}</td>
                        </tr>
                        <tr>
                            <th>Company Name</th>
                            <th>:</th>
                            <td>{{ $order->customer->company_name }}</td>
                        </tr>
                        <tr>
                            <th>Phone</th>
                            <th>:</th>
                            <td>{{ $order->customer->phone }}</td>
                        </tr>
                        <tr>
                            <th>Address</th>
                            <th>:</th>
                            <td>{{ $order->customer->address }}</td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-6">
                    <h4>Order Information</h4><hr>
                    <table class="table table-borderless">
                        <tr>
                            <th>Project</th>
                            <th>:</th>
                            <td>{{ optional($order->project)->project_name }}</td>
                        </tr>
                        <tr>
                            <th>Order Date</th>
                            <th>:</th>
                            <td>{{ date('D d F, Y', strtotime($order->order_date)) }}</td>
                        </tr>
                        <tr>
                            <th>Delivery Date</th>
                            <th>:</th>
                            <td>{{ date('D d F, Y', strtotime($order->delivery_date)) }}</td>
                        </tr>
                        <tr>
                            <th>Delivery Address</th>
                            <th>:</th>
                            <td>{{ $order->delivery_address }}</td>
                        </tr>
                    </table>
                </div>
            </div>

            @if (session('message'))
                {!! session('message') !!}
            @endif
          
            @if ($order->status != 2)

                <form action="{{ route('order.delivery.process') }}" method="post">
                    @csrf

                    <input type="hidden" name="order_id" value="{{ $order->id }}">

                    <table class="table table-bordered">
                        <tr>
                            <th>#</th>
                            <th>Product</th>
                            <th>Quantity</th>
                            <th>Previous Delivery</th>
                            <th>Current Delivery</th>
                            <th>Due Quantity</th>
                        </tr>
                        @foreach ($order->orderProducts as $key => $op)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td>{{ $op->product->product_name }}</td>
                                <td>{{ $op->quantity }}</td>
                                <td>{{ $op->deliveryProducts->sum('quantity') }}</td>
                                <td>
                                    <input type="number" name="order_product[{{ $op->id }}]" 
                                    data-qty="{{ $op->quantity }}" 
                                    data-pre_delivery_qty="{{ $op->deliveryProducts->sum('quantity') }}" 
                                    data-due_qty="{{ $op->quantity - $op->deliveryProducts->sum('quantity') }}" 
                                    max="{{ $op->quantity - $op->deliveryProducts->sum('quantity') }}"
                                    min="1" class="current_quantity form-control">
                                </td>
                                <td class="due-qty">
                                    {{ $op->quantity - $op->deliveryProducts->sum('quantity') }}
                                </td>
                            </tr>
                        @endforeach
                    </table>

                    <p class="text-right">
                        <input type="submit" value="Submit" class="btn btn-primary">
                    </p>

                </form>

            @endif

        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-10 offset-md-1">
      <div class="tile">
        <div class="tile-body">

            <h4 class="mb-3">Delivery Information</h4>

            @foreach ($deliveryProducts as $collection)
            <table class="table table-bordered">
                <tr>
                    <th>#</th>
                    <th>Date</th>
                    <th>Product</th>
                    <th>Rate</th>
                    <th>Quantity</th>
                    <th>Amount</th>
                    <th>Delivered by</th>
                </tr>
                @php($total = 0)
                @php($batch_id = null)
                @foreach ($collection as $k => $dp)
                    @if ($loop->first)
                        @php($batch_id = $dp->delivery_batch)
                    @endif
                    @php($total += $dp->orderProduct->product->product_price * $dp->quantity)
                    <tr>
                        <td>{{ ++$k }}</td>
                        <td>{{ date('D d F, Y', strtotime($dp->created_at)) }}</td>
                        <td>{{ $dp->orderProduct->product->product_name }}</td>
                        <td>{{ $dp->orderProduct->rate }}</td>
                        <td>{{ $dp->quantity }}</td>
                        <td>{{ number_format($dp->orderProduct->product->product_price * $dp->quantity, 2) }}</td>
                        <td>{{ ucfirst($dp->savedBy->employee_name ?? $dp->savedBy->username) }}</td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="6" class="text-right">Total = {{ number_format($total, 2) }}</td>
                    <td>
                        @if ($batch_id != null)
                        <a href="{{ route('order.delivery.chalan', [$order->id, $batch_id]) }}" target="_blank" class="btn btn-info btn-sm">Chalan</a>
                        @endif
                    </td>
                </tr>
            </table>
            @endforeach

        </div>
      </div>
    </div>
  </div>

@endsection

@push('js')
  <script>
      $('.current_quantity').on('keyup change', function() {
          var qty = $(this).data('qty');
          var preDeliveryQty = $(this).data('pre_delivery_qty');
          var dueQty = $(this).data('due_qty');
          var currentQty = $(this).val();
          if (currentQty > dueQty) {
            alert('Current Delivery quantity can not be greater than Due quantity');
            $(this).val('').parent().parent().find('.due-qty').text(dueQty);
          } else {
            $(this).parent().parent().find('.due-qty').text(dueQty - currentQty);
          }
      });
  </script>
@endpush