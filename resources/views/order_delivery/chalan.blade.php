@extends('layouts.master')

@section('content')
<div class="row">
    <div class="col-md-10 offset-md-1">
        <div class="clearfix">
            <button onclick="printT('invoice')" class="btn btn-sm btn-dark mb-2 float-left">
                <i class="fa fa-print"></i>
            </button>
            <a href="{{ url()->previous() }}" class="btn btn-sm btn-warning float-right">Cancel</a>
        </div>
        <div id="invoice">
            <div class="p-5">
                <div class="row">
                    <div class="col">
                        <h3 class="font-weight-normal mt-4 company-name">{{ $_settings->company_name }}</h3>
                    </div>
                    <div class="col text-right">
                        <h5>{{ $_settings->company_name }}</h5>
                        <p class="text">
                            {{ $_settings->address }} <br>
                            {{ $_settings->phone_2 ?? $_settings->phone_1 }} <br>
                            {{ $_settings->email_2 ?? $_settings->email }}
                        </p>
                    </div>
                </div>

                <p class="border text-center mb-4 mt-2" style="font-size:20px">CHALAN</p>

                <div class="row">
                    <div class="col">
                        <p class="mb-2 text">CHALAN TO:</p>
                        <h4>{{ $order->customer->company_name }}</h4>
                        <p class="text">
                            Address : {{ $order->delivery_address }} <br>
                            Phone : {{ $order->customer->phone }}
                        </p>
                    </div>
                    <div class="col text-right">
                        <h5>Chalan {{ $chalanNumber }}</h5>
                        <p class="text">
                            Date of Chalan : {{ $chalanDate }} <br>
                            Date of Order : {{ date('d-m-Y', strtotime($order->order_date)) }} <br>
                        </p>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <tr>
                            <th>#</th>
                            <th>Product</th>
                            <th>Rate</th>
                            <th>Quantity</th>
                            <th>Amount</th>
                        </tr>
                        @php($sub_total = 0)
                        @foreach ($order->deliveryProducts as $key => $dp)
                            @php($sub_total += $dp->orderProduct->rate * $dp->quantity)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td>{{ $dp->orderProduct->product->product_name }}</td>
                                <td>{{ number_format($dp->orderProduct->rate, 2) }}</td>
                                <td>{{ $dp->quantity }}</td>
                                <td>{{ number_format($dp->orderProduct->rate * $dp->quantity, 2) }}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
                <div class="row">
                    <div class="col">
                        <h3 class="font-weight-normal">Thank you!</h3>
                    </div>
                    <div class="col">
                        <table class="table table-borderless total">
                            <tr>
                                <th>Sub Total</th>
                                <th class="colon">:</th>
                                <td>{{ number_format($sub_total, 2) }}</td>
                            </tr>
                            <tr>
                                <th>TAX ({{ $tax }} %)</th>
                                <th>:</th>
                                <td>{{ number_format(($sub_total * $tax) / 100, 2) }}</td>
                            </tr>
                            <tr>
                                <th>Grand Total</th>
                                <th>:</th>
                                <td>{{ number_format($sub_total + (($sub_total * $tax) / 100), 2) }}</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="divFooter">
                    <p class="text">{{ $_settings->company_name }}</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
<script>
    function printT(el) {
        var rp = document.body.innerHTML;
        var pc = document.getElementById(el).innerHTML;
        document.body.innerHTML = pc;
        window.print();
        document.body.innerHTML = rp;
    }
</script>
@endpush

@push('css')
<style>
@media screen {
    div.divFooter {
        display: none;
    }
}
@page {
  size: A4;
  margin: 11mm 17mm 17mm 17mm;
}
@media print {
    div.divFooter {
        position: fixed;
        bottom: 0;
        border-top: 2px solid gray;
        text-align: center;
        font-size: 20px;
        width: 90%;
    }
    table.table {
        font-size: 20px;
    }
    p.text {
        font-size: 20px;
    }
    table.total {
        font-size: 22px;
    }
}

* {
    letter-spacing: 1px;
}
.company-name {
    font-size: 40px;
}
#invoice {
    background: white;
}
.table {
    font-size: 18px;
}
.text {
    font-size: 17px;
}
.total {
    width: 80%;
    margin-left: auto;
    font-size: 20px;
}
.total th, .total td {
    padding: 0;
    text-align: right;
}

.colon {
    width: 10%;
}
</style>
@endpush