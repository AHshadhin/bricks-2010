@extends('layouts.master')

@push('css')
    <link rel="stylesheet" href="{{ asset('assets/jquery-ui/jquery-ui.css') }}">
@endpush

@section('content')

<div class="app-title">
    <div>
        <h1><i class="fa fa-user-plus"></i> Add Visit</h1>
    </div>
    <div>
        <a href="{{ route('visits') }}" class="btn btn-dark">
            <i class="fa fa-arrow-left"></i> Back
        </a>
    </div>
</div>
  
<div class="row">
    <div class="col-md-12">
      <div class="tile">
        <div class="tile-body">

            <form action="{{ route('visit.store') }}" method="post">
                @csrf

                <div class="row">

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="customer"> Company Name <span class="text-danger">*</span> </label>
                            <select name="customer" id="customer" class="form-control">
                                <option value="">--- Select Company ---</option>
                                @foreach ($customers as $customer)
                                    <option value="{{ $customer->id }}">{{ $customer->company_name }}</option>
                                @endforeach
                            </select>
                            @error('customer')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="contact_person">Contact Person <span class="text-danger">*</span> </label>
                            <input type="text" name="contact_person" id="contact_person" readonly value="{{ old('contact_person') }}" class="form-control">
                            @error('contact_person')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="phone">Phone <span class="text-danger">*</span> </label>
                            <input type="text" name="phone" id="phone" value="{{ old('phone') }}" readonly class="form-control">
                            @error('phone')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="area">Area <span class="text-danger">*</span> </label>
                            <input type="text" name="area" id="area" value="{{ old('area') }}" readonly class="form-control">
                            @error('area')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="address">Address <span class="text-danger">*</span> </label>
                            <textarea name="address" id="address" readonly class="form-control">{{ old('address') }}</textarea>
                            @error('address')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="visit_date">Visit Date <span class="text-danger">*</span> </label>
                            <input type="text" name="visit_date" id="visit_date" value="{{ old('visit_date') }}" class="form-control">
                            @error('visit_date')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="company">For Company <span class="text-danger ml-3">*</span></label>
                            <select name="company" id="company" class="form-control">
                                <option value="">--- Select Company ---</option>
                                @foreach ($companies as $company)
                                    <option value="{{ $company->id }}" {{ old('company') == $company->id ? 'selected' : '' }}>
                                        {{ $company->name }}
                                    </option>
                                @endforeach
                            </select>
                            @error('company')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="authorising_person">Authorising Person <span class="text-danger">*</span> </label>
                            <select name="authorising_person" id="authorising_person" class="form-control">
                                <option value="">--- Select Authorising Person ---</option>
                                @foreach ($employees as $employee)
                                    <option value="{{ $employee->id }}" {{ old('authorising_person') == $employee->id ? 'selected' : '' }}>
                                        {{ $employee->employee_name }}
                                    </option>
                                @endforeach
                            </select>
                            @error('authorising_person')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="job_type">Job Type <span class="text-danger">*</span> </label>
                            <select name="job_type" id="job_type" class="form-control">
                                <option value="">--- Select Job Type ---</option>
                                @foreach ($job_types as $job_type)
                                    <option value="{{ $job_type->id }}" {{ old('job_type') == $job_type->id ? 'selected' : '' }}>
                                        {{ $job_type->type }}
                                    </option>
                                @endforeach
                            </select>
                            @error('job_type')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="mode_of_transport">Mode of Transport <span class="text-danger">*</span> </label>
                            <select name="mode_of_transport" id="mode_of_transport" class="form-control">
                                <option value="">--- Select Mode of Transport ---</option>
                                @foreach ($transports as $transport)
                                    <option value="{{ $transport->id }}" {{ old('mode_of_transport') == $transport->id ? 'selected' : '' }}>
                                        {{ $transport->transport_name }}
                                    </option>
                                @endforeach
                            </select>
                            @error('mode_of_transport')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="comment">Comment</label>
                            <textarea name="comment" id="comment" class="form-control">{{ old('comment') }}</textarea>
                            @error('comment')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="col-md-6 offset-md-3">
                    <input type="submit" value="Submit" class="btn btn-info btn-block">
                </div>

            </form>

        </div>
      </div>
    </div>
  </div>

@endsection

@push('js')
  <script src="{{ asset('assets/jquery-ui/jquery-ui.js') }}"></script>
  <script>
      $(function() {
            $("#visit_date").datepicker({
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true
            }).datepicker("setDate", new Date());

            $('#customer').on('change', function() {
                var id = $(this).val();
                if (id != '') {
                    var route = "{{ route('customer.show', ':id') }}";
                    var _url = route.replace(':id', id);
                    $.ajax({
                        url: _url,
                        method: "GET",
                        success: function(res) {
                            $('#contact_person').val(res.contact_person);
                            $('#phone').val(res.phone);
                            $('#area').val(res.area);
                            $('#address').val(res.address);
                        }
                    });
                }
            });
        });
  </script>
@endpush