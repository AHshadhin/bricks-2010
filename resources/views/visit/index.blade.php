@extends('layouts.master')

@push('css')
    
@endpush

@section('content')

<div class="app-title">
    <div>
        <h1><i class="fa fa-car"></i> Visits</h1>
    </div>
    <div>
        <a href="{{ route('visit.create') }}" class="btn btn-primary">
            <i class="fa fa-plus"></i> Add New Visit
        </a>
    </div>
</div>

@if (session('message'))
    {!! session('message') !!}
@endif
  
<div class="row">
    <div class="col-md-12">
      <div class="tile">
        <div class="tile-body">
          <div class="table-responsive">
            <table class="table table-hover table-bordered" id="sampleTable">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Date</th>
                  <th>Company Name</th>
                  <th>Saved By</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($visits as $key => $visit)
                  <tr>
                    <td>{{ ++$key }}</td>
                    <td>{{ date('D d F, Y', strtotime($visit->visit_date)) }}</td>
                    <td>{{ $visit->company_name ?? $visit->company->name }}</td>
                    <td>{{ $visit->savedBy->employee_name ?? $visit->savedBy->username }}</td>
                    <td>
                        <div class="btn-group">
                            <button type="button" data-id="{{ $visit->id }}" class="show-modal btn btn-dark btn-sm">
                                <i class="fa fa-eye"></i>
                            </button>
                            <a href="{{ route('visit.edit', $visit->id) }}" class="btn btn-info btn-sm">
                                <i class="fa fa-edit"></i>
                            </a>
                            @can('admin-only', Auth::user())
                            <form action="{{ route('visit.destroy', $visit->id) }}" method="post">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="delete_button btn btn-danger btn-sm"> 
                                    <i class="fa fa-trash"></i> 
                                </button>
                            </form>
                            @endcan
                        </div>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <div class="modal-header p-3">
            <h5 class="modal-title" id="exampleModalLabel">Visit Information</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          
          <div class="my-3 ml-4 mr-4">
            <table class="table table-borderless _table">
                <tr>
                    <th>Visit Date</th>
                    <th>:</th>
                    <td id="v_date"></td>
                </tr>
                <tr>
                    <th>Company Name</th>
                    <th>:</th>
                    <td id="v_company_name"></td>
                </tr>
                <tr>
                    <th>Contact Person</th>
                    <th>:</th>
                    <td id="v_contact_person"></td>
                </tr>
                <tr>
                    <th>Authorising Person</th>
                    <th>:</th>
                    <td id="v_reference_by"></td>
                </tr>
                <tr>
                    <th>Job Type</th>
                    <th>:</th>
                    <td id="v_job_type"></td>
                </tr>
                <tr>
                    <th>Mode of Transport</th>
                    <th>:</th>
                    <td id="v_transport_mode"></td>
                </tr>
                <tr>
                    <th>Phone</th>
                    <th>:</th>
                    <td id="v_phone"></td>
                </tr>
                <tr>
                    <th>Address</th>
                    <th>:</th>
                    <td id="v_address"></td>
                </tr>
                <tr>
                    <th>Area</th>
                    <th>:</th>
                    <td id="v_area"></td>
                </tr>
                <tr>
                    <th>Comment</th>
                    <th>:</th>
                    <td id="v_comment"></td>
                </tr>
                <tr>
                    <th>Saved By</th>
                    <th>:</th>
                    <td id="v_sb"></td>
                </tr>
                <tr>
                    <th>Updated By</th>
                    <th>:</th>
                    <td id="v_ub"></td>
                </tr>
            </table>
          </div>

          <div class="mx-3 mb-4" id="map">
            <iframe class="w-100" style="min-height:300px"></iframe>
          </div>

      </div>
    </div>
  </div>

@endsection

@push('js')
<script type="text/javascript" src="{{ asset('assets/') }}/js/plugins/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="{{ asset('assets/') }}/js/plugins/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="{{ asset('assets/') }}/js/sweetalert2@9.js"></script>
<script type="text/javascript">

    $('#sampleTable').DataTable();

    $(document).on('click', '.show-modal', function() {
        var this_id = $(this).data('id');
        if ('' != this_id) {
            var route = '{{ route('visit.show', ':id') }}';
            var _url = route.replace(':id', this_id);
            $.ajax({
                url: _url,
                method: 'GET',
                success: function(response) {
                    $('#v_date').text(response.visit_date);
                    if (response.company_name != null) {
                        $('#v_company_name').text(response.company_name); 
                    } else {
                        $('#v_company_name').text(response.company.name);
                    }
                    $('#v_contact_person').text(response.contact_person);
                    $('#v_reference_by').text(response.reference_by.employee_name);
                    $('#v_job_type').text(response.job_type.type);
                    $('#v_transport_mode').text(response.transport_mode.transport_name);
                    $('#v_phone').text(response.phone);
                    $('#v_address').text(response.address);
                    $('#v_area').text(response.area);
                    if (response.location != null) {
                        $('#map iframe').show().attr('src', 'https://maps.google.com/maps?q='+ response.location +'&hl=es;z=14&output=embed');
                    } else {
                        $('#map iframe').hide();
                    }
                    $('#v_comment').text(response.comment);
                    if (response.saved_by.username != null) {
                        $('#v_sb').text(response.saved_by.username);
                    } else {
                        $('#v_sb').text(response.saved_by.employee_name);
                    }
                    if (response.updated_by != null) {
                        if (response.updated_by.username != null) {
                            $('#v_ub').text(response.updated_by.username);
                        } else {
                            $('#v_ub').text(response.updated_by.employee_name);
                        }
                    }

                    $('._table').children('tbody').children('tr').children('td').each(function() {
                        if ($(this).text() == '') {
                            $(this).closest('tr').remove();
                        }
                    });
                    
                    $('.bd-example-modal-lg').modal('show');
                }
            });
        }
    });

    $(document).on('click', '.delete_button', function (e) {
        e.preventDefault();
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                $(this).closest('form').submit();
            }
        });
    });
</script>
@endpush