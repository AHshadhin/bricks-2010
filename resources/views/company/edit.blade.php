@extends('layouts.master')

@push('css')
    
@endpush

@section('content')

<div class="app-title">
    <div>
        <h1><i class="fa fa-product-hunt"></i> Edit Company</h1>
    </div>
    <div>
        <a href="{{ route('company.index') }}" class="btn btn-dark">
            <i class="fa fa-arrow-left"></i> Back
        </a>
    </div>
</div>
  
<div class="row">
    <div class="col-md-6 offset-md-3">
      <div class="tile">
        <div class="tile-body">

            @if (session('message'))
                {!! session('message') !!}
            @endif
          
            <form action="{{ route('company.update', $company->id) }}" method="post">
                @csrf
                @method('PUT')

                <div class="form-group row">
                    <label class="col-md-3" for="name">Name <span class="text-danger">*</span> </label>
                    <div class="col-md-9">
                        <input type="text" name="name" id="name" value="{{ $company->name }}" class="form-control">
                        @error('name')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-3" for="description">Description</label>
                    <div class="col-md-9">
                        <textarea name="description" id="description" class="form-control">{{ $company->description }}</textarea>
                        @error('description')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-9 offset-md-3">
                        <input type="submit" value="Update" class="btn btn-info">
                    </div>
                </div>

            </form>

        </div>
      </div>
    </div>
  </div>

@endsection

@push('js')

@endpush