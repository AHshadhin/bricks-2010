<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
    <aside class="app-sidebar">
      <div class="app-sidebar__user">
        @if (Auth::user()->employee_picture)
        <img class="app-sidebar__user-avatar" src="{{ asset(Auth::user()->employee_picture) }}" height="50" alt="User Image">
        @else
        <img class="app-sidebar__user-avatar" src="{{ asset('assets/images/male-avatar.jpg') }}" height="50" alt="User Image">
        @endif
        <div>
          <p class="app-sidebar__user-name">{{ Auth::user()->employee_name ?? ucfirst(Auth::user()->username) }}</p>
          <p class="app-sidebar__user-designation">{{ Auth::user()->employee_post->post_name ?? '' }}</p>
        </div>
      </div>
      <ul class="app-menu">
        <li>
          <a class="app-menu__item {{ request()->is('home*') ? 'active' : '' }}" href="{{ route('home') }}">
            <i class="app-menu__icon fa fa-dashboard"></i><span class="app-menu__label">Dashboard</span>
          </a>
        </li>
        <li>
          <a class="app-menu__item {{ request()->is('attendance*') ? 'active' : '' }}" href="{{ route('attendance') }}">
            <i class="app-menu__icon fa fa-clock-o"></i><span class="app-menu__label">Attendance</span>
          </a>
        </li>
        <li class="treeview {{ request()->is('*order*') ? 'is-expanded' : '' }}">
          <a class="app-menu__item" href="javascript:" data-toggle="treeview">
            <i class="app-menu__icon fa fa-shopping-cart"></i>
            <span class="app-menu__label">Order</span>
            <i class="treeview-indicator fa fa-angle-right"></i>
          </a>
          <ul class="treeview-menu">
            <li>
              <a class="app-menu__item {{ request()->is('order*') ? 'active' : '' }}" href="{{ route('orders') }}">
                <i class="app-menu__icon fa fa-shopping-cart"></i><span class="app-menu__label">Orders</span>
              </a>
            </li>
            <li>
              <a class="app-menu__item {{ request()->is('pending-orders') ? 'active' : '' }}" href="{{ route('order.pending') }}">
                <i class="app-menu__icon fa fa-clock-o"></i><span class="app-menu__label">Pending Order</span>
              </a>
            </li>
            <li>
              <a class="app-menu__item {{ request()->is('archive-orders') ? 'active' : '' }}" href="{{ route('order.archive') }}">
                <i class="app-menu__icon fa fa-archive"></i><span class="app-menu__label">Archive Order</span>
              </a>
            </li>
          </ul>
        </li>
        @can('admin-only', Auth::user())
        <li>
          <a class="app-menu__item {{ request()->is('employee*') ? 'active' : '' }}" href="{{ route('employee.index') }}">
            <i class="app-menu__icon fa fa-users"></i><span class="app-menu__label">Employee</span>
          </a>
        </li>
        @endcan
        <li>
          <a class="app-menu__item {{ request()->is('visit*') ? 'active' : '' }}" href="{{ route('visits') }}">
            <i class="app-menu__icon fa fa-car"></i><span class="app-menu__label">Visit</span>
          </a>
        </li>
        <li>
          <a class="app-menu__item {{ request()->is('customer*') ? 'active' : '' }}" href="{{ route('customer.index') }}">
            <i class="app-menu__icon fa fa-users"></i><span class="app-menu__label">Customer</span>
          </a>
        </li>
        <li>
          <a class="app-menu__item {{ request()->is('project*') ? 'active' : '' }}" href="{{ route('project.index') }}">
            <i class="app-menu__icon fa fa-tasks"></i><span class="app-menu__label">Project</span>
          </a>
        </li>
        <li>
          <a class="app-menu__item {{ request()->is('product*') ? 'active' : '' }}" href="{{ route('product.index') }}">
            <i class="app-menu__icon fa fa-list"></i><span class="app-menu__label">Product</span>
          </a>
        </li>
        <li class="treeview">
          <a class="app-menu__item" href="javascript:" data-toggle="treeview">
            <i class="app-menu__icon fa fa-file"></i>
            <span class="app-menu__label">Report</span>
            <i class="treeview-indicator fa fa-angle-right"></i>
          </a>
          <ul class="treeview-menu">
            <li>
              <a href="{{ route('report.visit-report') }}" class="app-menu__item">
                  <i class="app-menu__icon fa fa-list"></i> Visit Report
              </a>
            </li>
            <li>
              <a href="{{ route('report.order-report') }}" class="app-menu__item">
                  <i class="app-menu__icon fa fa-list"></i> Order Report
              </a>
            </li>
          </ul>
        </li>
        @can('admin-only', Auth::user())
        <li class="treeview {{ request()->is('setup*') ? 'is-expanded' : '' }}">
          <a class="app-menu__item" href="javascript:" data-toggle="treeview">
            <i class="app-menu__icon fa fa-cog"></i>
            <span class="app-menu__label">Setup</span>
            <i class="treeview-indicator fa fa-angle-right"></i>
          </a>
          <ul class="treeview-menu">
            <li>
              <a href="{{ route('post.index') }}" class="app-menu__item {{ request()->is('setup/post*') ? 'active' : '' }}">
                  <i class="app-menu__icon fa fa-list"></i> Employee Post
              </a>
            </li>
            <li>
              <a href="{{ route('department.index') }}" class="app-menu__item {{ request()->is('setup/department*') ? 'active' : '' }}">
                  <i class="app-menu__icon fa fa-building-o"></i> Employee Department
              </a>
            </li>
            <li>
              <a href="{{ route('company.index') }}" class="app-menu__item {{ request()->is('setup/company*') ? 'active' : '' }}">
                  <i class="app-menu__icon fa fa-building-o"></i> Company
              </a>
            </li>
            <li>
              <a href="{{ route('customer.types') }}" class="app-menu__item {{ request()->is('setup/type*') ? 'active' : '' }}">
                  <i class="app-menu__icon fa fa-building-o"></i> Customer Type
              </a>
            </li>
            <li>
              <a href="{{ route('transports') }}" class="app-menu__item {{ request()->is('setup/transport*') ? 'active' : '' }}">
                  <i class="app-menu__icon fa fa-truck"></i> Transport
              </a>
            </li>
            <li>
              <a href="{{ route('job.types') }}" class="app-menu__item {{ request()->is('setup/job-type*') ? 'active' : '' }}">
                  <i class="app-menu__icon fa fa-tasks"></i> Job Type
              </a>
            </li>
          </ul>
        </li>
        <li>
          <a href="{{ route('settings') }}" class="app-menu__item {{ request()->is('settings') ? 'active' : '' }}">
              <i class="app-menu__icon fa fa-cog"></i><span class="app-menu__label">Setting</span>
          </a>
        </li>
        @endcan

        

        {{-- <li class="treeview {{ request()->is('employee*') ? 'is-expanded' : '' }}">
          <a class="app-menu__item" href="javascript:" data-toggle="treeview">
            <i class="app-menu__icon fa fa-user"></i>
            <span class="app-menu__label">Employee</span>
            <i class="treeview-indicator fa fa-angle-right"></i>
          </a>
          <ul class="treeview-menu">
            <li>
              <a href="" class="app-menu__item {{ request()->is('employee/create') ? 'active' : '' }}">
                  <i class="app-menu__icon fa fa-plus"></i> Add New
              </a>
            </li>
          </ul>
        </li> --}}

      </ul>
    </aside>