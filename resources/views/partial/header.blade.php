<header class="app-header">
  <a class="app-header__logo" href="{{ route('home') }}">{{ $_settings->company_name }}</a>
  <!-- Sidebar toggle button-->
  <a class="app-sidebar__toggle" href="javascript:" data-toggle="sidebar" aria-label="Hide Sidebar"></a>
  <!-- Navbar Right Menu-->
  <ul class="app-nav">
    <li>
      <div class="timer">
        <i class="fa fa-clock-o"></i> {{ date('l') }}, {{ date('d F Y') }}, <span id="clock"></span>
      </div>
    </li>
    <!-- User Menu-->
    <li class="dropdown"><a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu"><i class="fa fa-user fa-lg"></i></a>
      <ul class="dropdown-menu settings-menu dropdown-menu-right">
        <li>
          <a class="dropdown-item" href="{{ route('employee.profile') }}">
            <i class="fa fa-user fa-lg"></i> Profile
          </a>
        </li>
        <li>
          <a class="dropdown-item" href="{{ route('logout.process') }}"
              onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
              <i class="fa fa-sign-out fa-lg"></i> {{ __('Logout') }}
          </a>

          <form id="logout-form" action="{{ route('logout.process') }}" method="POST" style="display: none;">
              @csrf
          </form>
        </li>
      </ul>
    </li>
  </ul>
</header>