@extends('layouts.master')

@push('css')
    
@endpush

@section('content')

<div class="app-title">
    <div class="float-left">
      <h1><i class="fa fa-dashboard"></i> Dashboard</h1>
    </div>
    <div class="float-right">
      <a href="{{ asset('android_app/Bricks-2010-App.apk') }}" class="text-decoration-none">
        <i class="fa fa-download mr-1"></i> Android App
      </a>
    </div>
</div>
  <div class="row">
    @if ($total_employee > 0)
      <div class="col-md-6 col-lg-3">
        <div class="widget-small info coloured-icon"><i class="icon fa fa-users fa-3x"></i>
          <div class="info">
            <h4>Employee</h4>
            <p><b>{{ $total_employee }}</b></p>
          </div>
        </div>
      </div>
    @endif
    <div class="col-md-6 col-lg-3">
      <div class="widget-small primary coloured-icon"><i class="icon fa fa-users fa-3x"></i>
        <div class="info">
          <h4>Customer</h4>
          <p><b>{{ $total_customer }}</b></p>
        </div>
      </div>
    </div>
    <div class="col-md-6 col-lg-3">
      <div class="widget-small info coloured-icon"><i class="icon fa fa-car fa-3x"></i>
        <div class="info">
          <h4>Visit</h4>
          <p><b>{{ $total_visit }}</b></p>
        </div>
      </div>
    </div>
    <div class="col-md-6 col-lg-3">
      <div class="widget-small info coloured-icon"><i class="icon fa fa-product-hunt fa-3x"></i>
        <div class="info">
          <h4>Product</h4>
          <p><b>{{ $total_product }}</b></p>
        </div>
      </div>
    </div>
    <div class="col-md-6 col-lg-3">
      <div class="widget-small info coloured-icon"><i class="icon fa fa-shopping-cart fa-3x"></i>
        <div class="info">
          <h4>Order</h4>
          <p><b>{{ $total_pending_order + $total_archive_order }}</b></p>
        </div>
      </div>
    </div>
    <div class="col-md-6 col-lg-4">
      <div class="widget-small warning coloured-icon"><i class="icon fa fa-clock-o fa-3x"></i>
        <div class="info">
          <h4>Pending Order</h4>
          <p><b>{{ $total_pending_order }}</b></p>
        </div>
      </div>
    </div>
    <div class="col-md-6 col-lg-4">
      <div class="widget-small primary coloured-icon"><i class="icon fa fa-archive fa-3x"></i>
        <div class="info">
          <h4>Archive Order</h4>
          <p><b>{{ $total_archive_order }}</b></p>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6">
      <div class="tile">
        <h3 class="tile-title">This Year Monthly Sales</h3>
        <div class="embed-responsive embed-responsive-16by9">
          <canvas class="embed-responsive-item" id="lineChartDemo"></canvas>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="tile">
        <h3 class="tile-title">Today Summary</h3>
        <div class="">
          <table class="table table-borderless">
            <tr>
              <th class="w-25">New Customer</th>
              <th class="w-25">:</th>
              <td>{{ $todayNewCustomer }}</td>
            </tr>
            <tr>
              <th>New Visit</th>
              <th>:</th>
              <td>{{ $todayNewVisit }}</td>
            </tr>
            <tr>
              <th>New Order</th>
              <th>:</th>
              <td>{{ $todayNewOrder }}</td>
            </tr>
            <tr>
              <th>Archive Order</th>
              <th>:</th>
              <td>{{ $todayArciveOrder }}</td>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>

@endsection

@push('js')
<script type="text/javascript" src="{{ asset('assets') }}/js/plugins/chart.js"></script>
<script>
  var data = {
    labels: ["January", "February", "March", "April", "May","June","July","August","September","October","November","December"],
    datasets: [
      {
        label: "My Second dataset",
        fillColor: "rgba(151,187,205,0.2)",
        strokeColor: "rgba(151,187,205,1)",
        pointColor: "rgba(151,187,205,1)",
        pointStrokeColor: "#fff",
        pointHighlightFill: "#fff",
        pointHighlightStroke: "rgba(151,187,205,1)",
        data: {!! $this_year_orders_count !!}
      }
    ]
  };
  
  var ctxl = $("#lineChartDemo").get(0).getContext("2d");
  var lineChart = new Chart(ctxl).Line(data);

</script>
@endpush