@extends('layouts.master')

@push('css')

@endpush

@section('content')

<div class="app-title">
    <div>
        <h1><i class="fa fa-shopping-cart"></i> Order</h1>
    </div>
    <div>
        <a href="{{ route('orders') }}" class="btn btn-dark">
            <i class="fa fa-arrow-left"></i> Back
        </a>
    </div>
</div>
  
<div class="row">
    <div class="col-md-12">
      <div class="tile">
        <div class="tile-body">

            <div class="row">
                <div class="col-md-6">
                    <h4>Customer Information</h4> <hr>
                    <table class="table table-borderless">
                        <tr>
                            <th>Customer Id</th>
                            <th>:</th>
                            <td>{{ $order->customer->customer_id }}</td>
                        </tr>
                        <tr>
                            <th>Company Name</th>
                            <th>:</th>
                            <td>{{ $order->customer->company_name }}</td>
                        </tr>
                        <tr>
                            <th>Phone</th>
                            <th>:</th>
                            <td>{{ $order->customer->phone }}</td>
                        </tr>
                        <tr>
                            <th>Address</th>
                            <th>:</th>
                            <td>{{ $order->customer->address }}</td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-6">
                    <h4>Order Information</h4> <hr>
                    <table class="table table-borderless">
                        <tr>
                            <th>Project</th>
                            <th>:</th>
                            <td>{{ optional($order->project)->project_name }}</td>
                        </tr>
                        <tr>
                            <th>Order Date</th>
                            <th>:</th>
                            <td>{{ date('D d F, Y', strtotime($order->order_date)) }}</td>
                        </tr>
                        <tr>
                            <th>Delivery Date</th>
                            <th>:</th>
                            <td>{{ date('D d F, Y', strtotime($order->delivery_date)) }}</td>
                        </tr>
                        <tr>
                            <th>Delivery Address</th>
                            <th>:</th>
                            <td>{{ $order->delivery_address }}</td>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <h4>Products</h4>
                    <table class="table table-bordered">
                        <tr>
                            <th>#</th>
                            <th>Product</th>
                            <th>Rate</th>
                            <th>Quantity</th>
                            <th>Amount</th>
                        </tr>
                        @php($sub_total = 0)
                        @foreach ($order->orderProducts as $k => $p)
                            @php($sub_total += $p->rate * $p->quantity)
                            <tr>
                                <td>{{ ++$k }}</td>
                                <td>{{ $p->product->product_name }}</td>
                                <td>{{ $p->rate }}</td>
                                <td>{{ $p->quantity }}</td>
                                <td>{{ number_format($p->rate * $p->quantity, 2) }}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
                <div class="col-md-6">
                    <table class="table table-borderless mt-4">
                        <tr>
                            <th class="w-25">Sub Total</th>
                            <th>:</th>
                            <th>{{ number_format($sub_total, 2) }}</th>
                        </tr>
                        <tr>
                            <th>TAX</th>
                            <th>:</th>
                            <th>{{ ($sub_total * $order->tax) / 100 }}</th>
                        </tr>
                        <tr>
                            <th>Grand Total</th>
                            <th>:</th>
                            <th>{{ number_format($sub_total + (($sub_total * $order->tax) / 100), 2) }}</th>
                        </tr>
                    </table>
                </div>
            </div>

            <p class="text-right">
                <a href="{{ route('order.invoice', $order->id) }}" class="btn btn-info">
                    <i class="fa fa-file"></i> Invoice
                </a>
            </p>

        </div>
      </div>
    </div>
  </div>

@endsection

@push('js')

@endpush