@extends('layouts.master')

@push('css')
<style>
#cart-table th {
    padding: 5px 10px;
}
#cart-table td {
    padding: 0px 10px; 
    height: 35px;
}
#cart-table select {
    height: 25px;
    border: 1px solid #a9a9a9;
}
</style>
@endpush

@section('content')

<div class="app-title">
    <div>
        <h1><i class="fa fa-shopping-cart"></i> Edit Order</h1>
    </div>
    <div>
        <a href="{{ route('orders') }}" class="btn btn-dark">
            <i class="fa fa-arrow-left"></i> Back
        </a>
    </div>
</div>
  
<div class="row">
    <div class="col-md-12">
      <div class="tile">
        <div class="tile-body">
          
            <form action="{{ route('order.update') }}" id="update-form" method="post">
                @csrf

                <input type="hidden" name="order_id" value="{{ $order->id }}">

                <div class="row mb-2">
                    <div class="col-md-4">
                        <h4 class=""> <i class="fa fa-user"></i> Customer</h4><hr>
                        <div class="form-group">
                            <label for="">Customer Name </label>
                            <select name="customer" id="customer_id" class="form-control">
                                <option value="" hidden>--- Select Customer ---</option>
                                @foreach ($customers as $customer)
                                    <option value="{{ $customer->id }}" {{ $customer->id == $order->customer_id ? 'selected' : '' }}>
                                        {{ $customer->company_name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Project Name </label>
                            <select name="project" id="project_id" class="form-control">
                                <option value="" hidden>--- Select Project ---</option>
                                @foreach ($order->customer->projects as $_project)
                                    <option value="{{ $_project->id }}" {{ $_project->id == $order->project_id ? 'selected' : '' }}>
                                        {{ $_project->project_name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="customer_phone">Customer Phone</label>
                            <input type="text" name="customer_phone" value="{{ $order->customer->phone }}" id="customer_phone" readonly class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="customer_address">Customer Address</label>
                            <textarea name="customer_address" id="customer_address" readonly class="form-control">{{ $order->customer->address }}</textarea>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="clearfix border-bottom mb-3 pb-2">
                            <h4 class="float-left"> <i class="fa fa-shopping-cart"></i> Orders</h4>
                            <a href="{{ route('order.show', $order->id) }}" class="btn btn-primary btn-sm float-right">
                                <i class="fa fa-eye"></i> Show
                            </a>
                        </div>
                        @if (session('message'))
                            {!! session('message') !!}
                        @endif
                        <table class="table table-bordered" id="cart-table">
                            <thead>
                                <tr>
                                    <th>Product Name</th>
                                    <th>Rate</th>
                                    <th>Quantity</th>
                                    <th>Amount</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody id="rows">
                                @foreach ($order->orderProducts as $op)
                                    <tr>
                                        <td>
                                            <input type="hidden" name="old_product_id[]" value="{{ $op->id }}">
                                            {{ $op->product->product_name }}
                                        </td>
                                        <td><span class="product_price">{{ $op->rate }}</span></td>
                                        <td>
                                            <input type="number" name="old_product_quantity[]" value="{{ $op->quantity }}" class="quantity">
                                        </td>
                                        <td><span class="amount">{{ $op->amount }}</span></td>
                                        <td> --- </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        @if ($products->count())    
                            <p class="text-right">
                                <button type="button" id="add-new-btn" class="btn btn-info btn-sm">
                                    <i class="fa fa-plus"></i> Add New Product
                                </button>
                            </p>
                        @endif
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="delivery_date">Delivery Date</label>
                                    <input type="date" name="delivery_date" id="delivery_date" value="{{ $order->delivery_date }}" class="form-control">
                                </div>
                                <div class="">
                                    <label class="d-block">Order Status <span class="text-danger">*</span> </label>
                                    <label for="pending" class="mr-3">
                                        <input type="radio" name="status" id="pending" value="1" {{ $order->status == 1 ? 'checked' : '' }}> Pending
                                    </label>
                                    <label for="archive">
                                        <input type="radio" name="status" id="archive" value="2" {{ $order->status == 2 ? 'checked' : '' }}> Archive
                                    </label>
                                </div>
                            </div>
                            <div class="col">
                                <label for="delivery_address">Delivery Address</label>
                                <textarea name="delivery_address" id="delivery_address" class="form-control">{{ $order->delivery_address }}</textarea>
                            </div>
                        </div>
                        <p class="mt-3">
                            <input type="submit" value="Update" class="btn btn-primary btn-block">
                        </p>
                    </div>
                </div>

            </form>

        </div>
      </div>
    </div>
  </div>

@endsection

@push('js')
<script>

    $(document).on('submit', '#update-form', function(e) {
        var elements = $(this).find('input[type=number], select');
        var error = 0;
        for (let i = 0; i < elements.length; i++) {
            const el = elements[i];
            if (el.value == '') {
                error++;
                el.style.border = '1px solid red';
            } else {
                el.style.border = '2px solid #ced4da';
            }
        }
        if (error > 0) {
            return false;
        }
        return true;
    });

    $(document).on('click', '#add-new-btn', function(e) {
        e.preventDefault();
        var row = `
            <tr>
                <td>
                    <select name="new_product_id[]" class="product_id">
                        <option value="" hidden>--- Select Product ---</option>
                        @foreach ($products as $product)
                            <option value="{{ $product->id }}">{{ $product->product_name }}</option>
                        @endforeach
                    </select>
                </td>
                <td><span class="product_price">0.00</span></td>
                <td>
                    <input type="number" name="new_product_quantity[]" value="0" class="quantity">
                </td>
                <td><span class="amount">0.00</span></td>
                <td>
                    <button type="button" class="btn btn-sm btn-danger remove-btn"> 
                        <i class="fa fa-times"></i> 
                    </button>
                </td>
            </tr>
        `;
        $('#rows').append(row);
    });

    $(document).on('click', '.remove-btn', function(e) {
        e.preventDefault();
        if (confirm('Are you sure?')) {
            $(this).closest('tr').remove();
        }
    });

    $(document).on('keyup focusout change', '.quantity', function() {
      if ('' != $(this).val()) {
        var price = $(this).parent().parent().find('.product_price');
        var amount = $(this).parent().parent().find('.amount');
        var total_amount = price.text() * $(this).val();
        amount.text(total_amount.toFixed(2));
      } else {
        $(this).parent().parent().find('.amount').text('');
      }
    });

    $(document).on('change', '.product_id', function() {
      if ('' != $(this).val()) {
          var route = '{{ route('product.show', ':id') }}';
          var _url = route.replace(':id', $(this).val());
          $.ajax({
              url: _url,
              method: 'GET',
              success: function(response) {
                var price = $(this).parent().parent().find('.product_price');
                var quantity = $(this).parent().parent().find('.quantity');
                price.text(response.product_price);
                var total_amount = price.text() * quantity.val();
                $(this).parent().parent().find('.amount').text(total_amount.toFixed(2));
              }.bind(this)
          });
      }
    });

    $(document).on('change', '#customer_id', function() {
      if ('' != $(this).val()) {
          var route = '{{ route('customer.show', ':id') }}';
          var _url = route.replace(':id', $(this).val());
          $.ajax({
              url: _url,
              method: 'GET',
              success: function(response) {
                  $('#customer_ID').val(response.customer_id);
                  $('#customer_phone').val(response.phone);
                  $('#customer_address').val(response.address);
                  if (response.projects.length > 0) {
                    var projects = '<option value="" hidden>--- Select Project ---</option>';
                      for (let i = 0; i < response.projects.length; i++) {
                        const project = response.projects[i];
                        projects += '<option value="'+ project.id +'">'+ project.project_name +'</option>';
                      }
                    $('#project_id').html(projects);
                  }
                  $('.product select, .product input, .product button').prop('disabled', false);
              }
          });
      }
    });
</script>
@endpush