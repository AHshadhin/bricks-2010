@extends('layouts.master')

@push('css')
    
@endpush

@section('content')

<div class="app-title">
    <div>
        <h1>
          <i class="fa fa-list"></i> 
          @if ($mode == 'a')
            Archive Orders
          @elseif ($mode == 'p')
            Pending Orders
          @else
            Orders
          @endif
        </h1>
    </div>
    <div>
        <a href="{{ route('order.create') }}" class="btn btn-primary">
            <i class="fa fa-plus"></i> New Order
        </a>
    </div>
</div>

@if (session('message'))
    {!! session('message') !!}
@endif
  
<div class="row">
    <div class="col-md-12">
      <div class="tile">
        <div class="tile-body">

          <div class="mb-1">
            <label for="with_details">Record Type : </label>
            <select id="with_details" style="border: 1px solid #c6c9cc">
              <option value="">Without Details</option>
              <option value="with_details" {{ isset($with_details) && $with_details != null ? 'selected' : '' }}>With Details</option>
            </select>
          </div>

          <div class="table-responsive">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Order Number</th>
                  <th>Customer</th>
                  <th>Phone</th>
                  <th>Project</th>
                  <th>Order Date</th>
                  <th>Delivery Date</th>
                  <th>Status</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($orders as $key => $order)
                  <tr>
                    <td>{{ ++$key }}</td>
                    <td>{{ $order->invoice_number }}</td>
                    <td>{{ optional($order->customer)->company_name }}</td>
                    <td>{{ optional($order->customer)->phone }}</td>
                    <td>{{ optional($order->project)->project_name }}</td>
                    <td>{{ date('D d F, Y', strtotime($order->order_date)) }}</td>
                    <td>{{ date('D d F, Y', strtotime($order->delivery_date)) }}</td>
                    <td>
                        @if ($order->status == 1)
                            <span class="badge badge-warning">Pending</span>
                        @elseif ($order->status == 2)
                            <span class="badge badge-success">Archive</span>
                        @elseif ($order->status == 4)
                            <span class="badge badge-danger">Processing</span>
                        @endif
                    </td>
                    <td>
                        <div class="btn-group">
                            <a href="{{ route('order.delivery', $order->id) }}" title="Delivery" class="btn btn-info btn-sm">
                                Delivery
                            </a>
                            <a href="{{ route('order.invoice', $order->id) }}" title="Invoice" class="btn btn-dark btn-sm">
                                <i class="fa fa-file"></i>
                            </a>
                            <a href="{{ route('order.show', $order->id) }}" title="View" class="btn btn-secondary btn-sm">
                                <i class="fa fa-eye"></i>
                            </a>
                            @if ($order->deliveryProducts->sum('quantity') < 1)
                              <a href="{{ route('order.edit', $order->id) }}" title="Edit" class="btn btn-info btn-sm">
                                  <i class="fa fa-edit"></i>
                              </a>
                            @endif
                            @can('admin-only', Auth::user())
                            <form action="{{ route('order.destroy') }}" method="post">
                                @csrf
                                <input type="hidden" name="id" value="{{ $order->id }}">
                                <button type="submit" title="Delete" class="delete_button btn btn-danger btn-sm"> 
                                    <i class="fa fa-trash"></i> 
                                </button>
                            </form>
                            @endcan
                        </div>
                    </td>
                  </tr>
                  @if (isset($with_details) && $with_details != null)
                  <tr>
                    <td colspan="9">
                      <div class="row">
                        <div class="col-md-6">
                            <table class="table">
                                <tr>
                                    <th>Product</th>
                                    <th>Rate</th>
                                    <th>Quantity</th>
                                    <th>Amount</th>
                                </tr>
                                @php($sub_total = 0)
                                @foreach ($order->orderProducts as $k => $p)
                                    @php($sub_total += $p->rate * $p->quantity)
                                    <tr>
                                        <td>{{ $p->product->product_name }}</td>
                                        <td>{{ $p->rate }}</td>
                                        <td>{{ $p->quantity }}</td>
                                        <td>{{ number_format($p->rate * $p->quantity, 2) }}</td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                        <div class="col-md-6">
                            <table class="table-borderless">
                                <tr>
                                    <th class="w-25">Sub Total</th>
                                    <th>:</th>
                                    <th>{{ number_format($sub_total, 2) }}</th>
                                </tr>
                                <tr>
                                    <th>TAX</th>
                                    <th>:</th>
                                    <th>{{ ($sub_total * $order->tax) / 100 }}</th>
                                </tr>
                                <tr>
                                    <th>Grand Total</th>
                                    <th>:</th>
                                    <th>{{ number_format($sub_total + (($sub_total * $order->tax) / 100), 2) }}</th>
                                </tr>
                            </table>
                        </div>
                      </div>
                    </td>
                  </tr>
                  @endif
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection

@push('js')
<script type="text/javascript" src="{{ asset('assets/') }}/js/sweetalert2@9.js"></script>
<script type="text/javascript">

    $('#with_details').on('change', function() {
        var thisVal = $(this).val();
        if (thisVal == 'with_details') {
          window.location = "{{ route('orders', 'with-details') }}";
        } else {
          window.location = "{{ route('orders') }}";
        }
    });

    $(document).on('click', '.delete_button', function (e) {
        e.preventDefault();
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                $(this).closest('form').submit();
            }
        });
    });
</script>
@endpush