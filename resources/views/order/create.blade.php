@extends('layouts.master')

@push('css')
<link rel="stylesheet" href="{{ asset('assets/jquery-ui/jquery-ui.css') }}">
<style>
    .quantity {
        width: 70px;
        padding: 0px 7px;
        border: 1px solid #DEE2E6;
    }
</style>
@endpush

@section('content')

<div class="app-title">
    <div>
        <h1><i class="fa fa-shopping-cart"></i> New Order</h1>
    </div>
    <div>
        <a href="{{ route('orders') }}" class="btn btn-dark">
            <i class="fa fa-arrow-left"></i> Back
        </a>
    </div>
</div>
  
<div class="row">
    <div class="col-md-12">
      <div class="tile">
        <div class="tile-body">
          
            <div class="row mb-2">
                <div class="col-md-6">
                    <h4 class=""> <i class="fa fa-user"></i> Customer</h4><hr>
                    <div class="form-group row">
                        <label class="col-md-3" for="">Customer </label>
                        <div class="col-md-9">
                            <select name="customer_id" id="customer_id" class="form-control">
                                <option value="" hidden>--- Select Customer ---</option>
                                @foreach ($customers as $customer)
                                    <option value="{{ $customer->id }}">{{ $customer->company_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3" for="customer_ID">Customer ID</label>
                        <div class="col-md-9">
                            <input type="text" name="customer_ID" id="customer_ID" readonly class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3" for="customer_phone">Customer Phone</label>
                        <div class="col-md-9">
                            <input type="text" name="customer_phone" id="customer_phone" readonly class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3" for="customer_address">Customer Address</label>
                        <div class="col-md-9">
                            <textarea name="customer_address" id="customer_address" readonly class="form-control"></textarea>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 product">
                    <h4 class=""> <i class="fa fa-shopping-cart"></i> Order</h4><hr>
                    <div class="form-group row">
                        <label class="col-md-3" for="">Project </label>
                        <div class="col-md-9">
                            <select name="project_id" id="project" class="form-control" disabled>
                                <option value="" hidden>--- Select Project ---</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3" for="product_id">Product</label>
                        <div class="col-md-9">
                            <input type="hidden" id="product_name">
                            <select name="product_id" id="product_id" disabled class="form-control">
                                <option value="" hidden>--- Select Product ---</option>
                                @foreach ($products as $product)
                                    <option value="{{ $product->id }}">{{ $product->product_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3" for="product_price">Rate</label>
                        <div class="col-md-9">
                            <input type="number" name="product_price" id="product_price" disabled class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3" for="product_quantity">Quantity</label>
                        <div class="col-md-9">
                            <input type="number" name="product_quantity" id="product_quantity" min="1" disabled class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-9 offset-md-3">
                            <button type="button" id="add-to-cart-btn" disabled class="btn btn-info">
                                <i class="fa fa-plus-circle"></i> Add to Cart
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <div id="message"></div>

            <div class="row mt-2">
                <div class="col-md-7">
                    <table class="table table-bordered" id="cart-table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Product Name</th>
                                <th>Rate</th>
                                <th>Quantity</th>
                                <th>Amount</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="text-center text-danger">
                                <td colspan="6">No product available in this cart!</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-2">
                    <table>
                        <tr>
                            <th>Sub Total</th>
                            <th style="width:10px">:</th>
                            <td id="sub_total">0.00</td>
                        </tr>
                        <tr>
                            <th>TAX</th>
                            <th>:</th>
                            <td>{{ $tax }} %</td>
                        </tr>
                        <tr>
                            <th>Grand Total</th>
                            <th>:</th>
                            <td id="grand_total">0.00</td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-3">
                    <div class="d-block">
                        <span class="text-danger">Delivery Date : </span>
                        <input type="text" id="delivery_date" disabled autocomplete="off" class="form-control form-control-sm">
                    </div>
                    <div class="d-block mt-2">
                        <span class="text-danger">Delivery Address : </span>
                        <textarea name="delivery_address" id="delivery_address" disabled class="form-control"></textarea>
                    </div>
                    <button type="button" id="confirm-order-btn" disabled class="btn btn-success mt-2">
                        Confirm Order
                    </button>
                </div>
            </div>

        </div>
      </div>
    </div>
  </div>

@endsection

@push('js')
<script src="{{ asset('assets/jquery-ui/jquery-ui.js') }}"></script>
<script>
    $(function() {
        $("#delivery_date").datepicker({
            dateFormat: 'dd-mm-yy',
            changeMonth: true,
            changeYear: true
        });
    });


    var cart = [];
    var tax = {{ $tax }};

    $(document).on('click', '#confirm-order-btn', function() {
        var customer_id = $('#customer_id');
        var project_id = $('#project');
        var delivery_date = $('#delivery_date');
        var delivery_address = $('#delivery_address');
        if ('' == customer_id.val()) {
            alert('Please select a customer');
        } else if ('' == project_id.val()) {
            alert('Please select a project');
        } else if (cart.length < 1) {
            alert('Cart is empty. Please add to cart some products.')
        } else if ('' == delivery_date.val()) {
            alert('Please select delivery date');
        } else if ('' == delivery_address.val()) {
            alert('Please write delivery address');
        } else {

            $.ajax({
                url: '{{ route('order.confirm') }}',
                method: 'POST',
                data: {
                    _token: '{{ csrf_token() }}',
                    customer_id: customer_id.val(),
                    project_id: project_id.val(),
                    delivery_date: delivery_date.val(),
                    delivery_address: delivery_address.val(),
                    cart: cart,
                    tax: tax
                },
                success: function(response) {

                    if (response.success) {
                        var route = '{{ route('order.invoice', ':id') }}';
                        window.location = route.replace(':id', response.order_id);
                    } else if (!response.success) {
                        var message = '<div class="alert alert-danger">'+ response.message +'</div>';
                        $('#message').html(message);
                        setTimeout(() => {
                            $('#message').html('');
                        }, 5000);
                    }
                }
            });
        }
    });

    function clearInputs() {
        $('#product_id').val('');
        $('#product_name').val('');
        $('#product_price').val('');
        $('#product_quantity').val('');
        $('#customer_id').val('');
        $('#delivery_date').val('');
        $('#customer_phone').val('');
        $('#customer_address').val('');
        $('#delivery_address').val('');
        cart = [];
        getCart();

    }

    $(document).on('click', '#add-to-cart-btn', function() {
        if (
          '' == $('#product_id').val() || 
          '' == $('#product_name').val() ||
          '' == $('#product_price').val() ||
          '' == $('#product_quantity').val()
        ) {
          alert('Please fill out all fields');
        } else {
              var new_product = {
                  product_id : $('#product_id').val(),
                  product_name : $('#product_name').val(),
                  product_price : $('#product_price').val(),
                  product_quantity : $('#product_quantity').val()
              };
              if (cart.length) {
                  var a = false;
                  for (let i = 0; i < cart.length; i++) {
                      const el = cart[i];
                      if (el.product_id == new_product.product_id) {
                          var oq = parseInt(el.product_quantity);
                          var nq = parseInt(new_product.product_quantity);
                          el.product_quantity = oq + nq;
                          a = true;
                          break;
                      }
                  }
                  if (a == false) {
                      cart.push(new_product);
                  }
              } else {
                  cart.push(new_product);
              }
              $('#product_id').val('');
              $('#product_name').val('');
              $('#product_price').val('');
              $('#product_quantity').val('');
              $('.product button').prop('disabled', true);
        }
        getCart();
    });

    $(document).on('change focusout', '.quantity', function() {
        if (cart.length) {
            for (let i = 0; i < cart.length; i++) {
                const el = cart[i];
                if (el.product_id == $(this).data('id')) {
                  el.product_quantity = parseInt($(this).val());
                  getCart();
                  break;
                }
            }
        }
    });

    $(document).on('click', '.remove-item', function() {
        if (cart.length) {
            for (let i = 0; i < cart.length; i++) {
                const el = cart[i];
                if (el.product_id == $(this).data('id')) {
                    cart.splice(i, 1);
                    break;
                }
            }
        }
        getCart();
    });

    function getCart() {
      if (cart.length) {
            var rows = '';
            var j = 0;
            var total_amount = 0;
            for (let i = 0; i < cart.length; i++) {
                const el = cart[i];
                total_amount += (el.product_price * el.product_quantity) * 1;
                var tr = '<tr>';
                tr += '<td>' + (++j) + '</td>';
                tr += '<td>' + el.product_name + '</td>';
                tr += '<td>' + el.product_price + '</td>';
                tr += '<td><input type="number" data-id="'+ el.product_id +'"';
                tr += 'value="' + el.product_quantity + '" class="quantity"></td>';
                tr += '<td>' + (el.product_price * el.product_quantity) + '</td>';
                tr += '<td><button data-id="'+ el.product_id +'"';
                tr += 'class="remove-item btn btn-sm btn-danger"> ';
                tr += '<i class="fa fa-times"></i> </button></td>';
                tr += '</tr>';
                rows += tr;
            }
            $('#delivery_date').prop('disabled', false);
            $('#delivery_address').prop('disabled', false);
            $('#confirm-order-btn').prop('disabled', false);

            $('#cart-table tbody').html(rows);

            $('#sub_total').html(total_amount.toFixed(2));
            var _tax_amount = (total_amount * tax) / 100;
            $('#grand_total').html((total_amount + _tax_amount).toFixed(2));

        } else {

           $('#cart-table tbody').html(`
           <tr class="text-center text-danger">
                <td colspan="6">No product available in this cart!</td>
            </tr>
           `);

           $('#sub_total').html('0.00');
           $('#grand_total').html('0.00');
           $('#delivery_date').prop('disabled', true);
           $('#delivery_address').prop('disabled', true);
           $('#confirm-order-btn').prop('disabled', true);
        }
    }

    $(document).on('change', '#product_id', function() {
      if ('' != $(this).val()) {
          var route = '{{ route('product.show', ':id') }}';
          var _url = route.replace(':id', $(this).val());
          $.ajax({
              url: _url,
              method: 'GET',
              success: function(response) {
                  $('#product_price').val(response.product_price);
                  $('#product_name').val(response.product_name);
                  $('.product button').prop('disabled', false);
              }
          });
      }
    });

    $(document).on('change', '#customer_id', function() {
      if ('' != $(this).val()) {
          var route = '{{ route('customer.show', ':id') }}';
          var _url = route.replace(':id', $(this).val());
          $.ajax({
              url: _url,
              method: 'GET',
              success: function(response) {
                  $('#customer_ID').val(response.customer_id);
                  $('#customer_phone').val(response.phone);
                  $('#customer_address').val(response.address);
                  if (response.projects.length > 0) {
                    var projects = '<option value="" hidden>--- Select Project ---</option>';
                      for (let i = 0; i < response.projects.length; i++) {
                        const project = response.projects[i];
                        projects += '<option value="'+ project.id +'">'+ project.project_name +'</option>';
                      }
                    $('#project').html(projects);
                  }
                  $('#project, .product select, .product input, .product button').prop('disabled', false);
              }
          });
      }
    });
</script>
@endpush