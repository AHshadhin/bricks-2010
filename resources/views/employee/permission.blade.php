@extends('layouts.master')

@push('css')
<style>
    .menu {
        list-style: none;
        padding-left: 20px;
    }
    label {
        cursor: pointer;
    }
</style>
@endpush

@section('content')

<div class="app-title">
    <div>
        <h1><i class="fa fa-lock"></i> Employee Permission</h1>
    </div>
</div>

@if (session('message'))
    {!! session('message') !!}
@endif
  
<div class="row">
    <div class="col-md-12">
      <div class="tile">
        <div class="tile-body">

            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-4">
                    <label for="employee"> <input type="checkbox" name="" id="employee" class="menu-head"> Employee </label>
                    <ul class="menu">
                        <li> <label for="employee-list"> <input type="checkbox" name="" id="employee-list"> Employee List </label> </li>
                        <li> <label for="employee-create"> <input type="checkbox" name="" id="employee-create"> Employee Create </label> </li>
                        <li> <label for="employee-edit"> <input type="checkbox" name="" id="employee-edit"> Employee Edit </label> </li>
                        <li> <label for="employee-delete"> <input type="checkbox" name="" id="employee-delete"> Employee Delete </label> </li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-4">
                    <label for="visit"> <input type="checkbox" name="" id="visit" class="menu-head"> Visit </label>
                    <ul class="menu">
                        <li> <label for="visit-list"> <input type="checkbox" name="" id="visit-list"> Visit List </label> </li>
                        <li> <label for="visit-create"> <input type="checkbox" name="" id="visit-create"> Visit Create </label> </li>
                        <li> <label for="visit-edit"> <input type="checkbox" name="" id="visit-edit"> Visit Edit </label> </li>
                        <li> <label for="visit-delete"> <input type="checkbox" name="" id="visit-delete"> Visit Delete </label> </li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-4">
                    <label for="customer"> <input type="checkbox" name="" id="customer" class="menu-head"> Customer </label>
                    <ul class="menu">
                        <li> <label for="customer-list"> <input type="checkbox" name="" id="customer-list"> Customer List </label> </li>
                        <li> <label for="customer-create"> <input type="checkbox" name="" id="customer-create"> Customer Create </label> </li>
                        <li> <label for="customer-edit"> <input type="checkbox" name="" id="customer-edit"> Customer Edit </label> </li>
                        <li> <label for="customer-delete"> <input type="checkbox" name="" id="customer-delete"> Customer Delete </label> </li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-4">
                    <label for="c-project"> <input type="checkbox" name="" id="c-project" class="menu-head"> Customer Project </label>
                    <ul class="menu">
                        <li> <label for="project-list"> <input type="checkbox" name="" id="project-list"> Project List </label> </li>
                        <li> <label for="project-create"> <input type="checkbox" name="" id="project-create"> Project Create </label> </li>
                        <li> <label for="project-edit"> <input type="checkbox" name="" id="project-edit"> Project Edit </label> </li>
                        <li> <label for="project-delete"> <input type="checkbox" name="" id="project-delete"> Project Delete </label> </li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-4">
                    <label for="product"> <input type="checkbox" name="" id="product" class="menu-head"> Product </label>
                    <ul class="menu">
                        <li> <label for="product-list"> <input type="checkbox" name="" id="product-list"> Product List </label> </li>
                        <li> <label for="product-create"> <input type="checkbox" name="" id="product-create"> Product Create </label> </li>
                        <li> <label for="product-edit"> <input type="checkbox" name="" id="product-edit"> Product Edit </label> </li>
                        <li> <label for="product-delete"> <input type="checkbox" name="" id="product-delete"> Product Delete </label> </li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-4">
                    <label for="e-post"> <input type="checkbox" name="" id="e-post" class="menu-head"> Employee Post </label>
                    <ul class="menu">
                        <li> <label for="post-list"> <input type="checkbox" name="" id="post-list"> Post List </label> </li>
                        <li> <label for="post-create"> <input type="checkbox" name="" id="post-create"> Post Create </label> </li>
                        <li> <label for="post-edit"> <input type="checkbox" name="" id="post-edit"> Post Edit </label> </li>
                        <li> <label for="post-delete"> <input type="checkbox" name="" id="post-delete"> Post Delete </label> </li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-4">
                    <label for="e-department"> <input type="checkbox" name="" id="e-department" class="menu-head"> Employee Department </label>
                    <ul class="menu">
                        <li> <label for="e-department-list"> <input type="checkbox" name="" id="e-department-list"> Department List </label> </li>
                        <li> <label for="e-department-create"> <input type="checkbox" name="" id="e-department-create"> Department Create </label> </li>
                        <li> <label for="e-department-edit"> <input type="checkbox" name="" id="e-department-edit"> Department Edit </label> </li>
                        <li> <label for="e-department-delete"> <input type="checkbox" name="" id="e-department-delete"> Department Delete </label> </li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-4">
                    <label for="company"> <input type="checkbox" name="" id="company" class="menu-head"> For Company </label>
                    <ul class="menu">
                        <li> <label for="company-list"> <input type="checkbox" name="" id="company-list"> Company List </label> </li>
                        <li> <label for="company-create"> <input type="checkbox" name="" id="company-create"> Company Create </label> </li>
                        <li> <label for="company-edit"> <input type="checkbox" name="" id="company-edit"> Company Edit </label> </li>
                        <li> <label for="company-delete"> <input type="checkbox" name="" id="company-delete"> Company Delete </label> </li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-4">
                    <label for="transport"> <input type="checkbox" name="" id="transport" class="menu-head"> Mode of Transport </label>
                    <ul class="menu">
                        <li> <label for="transport-list"> <input type="checkbox" name="" id="transport-list"> Transport List </label> </li>
                        <li> <label for="transport-create"> <input type="checkbox" name="" id="transport-create"> Transport Create </label> </li>
                        <li> <label for="transport-edit"> <input type="checkbox" name="" id="transport-edit"> Transport Edit </label> </li>
                        <li> <label for="transport-delete"> <input type="checkbox" name="" id="transport-delete"> Transport Delete </label> </li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-4">
                    <label for="customer-type"> <input type="checkbox" name="" id="customer-type" class="menu-head"> Customer Type </label>
                    <ul class="menu">
                        <li> <label for="customer-type-list"> <input type="checkbox" name="" id="customer-type-list"> Customer Type List </label> </li>
                        <li> <label for="customer-type-create"> <input type="checkbox" name="" id="customer-type-create"> Customer Type Create </label> </li>
                        <li> <label for="customer-type-edit"> <input type="checkbox" name="" id="customer-type-edit"> Customer Type Edit </label> </li>
                        <li> <label for="customer-type-delete"> <input type="checkbox" name="" id="customer-type-delete"> Customer Type Delete </label> </li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-4">
                    <label for="job-type"> <input type="checkbox" name="" id="job-type" class="menu-head"> Job Type </label>
                    <ul class="menu">
                        <li> <label for="job-type-list"> <input type="checkbox" name="" id="job-type-list"> Job Type List </label> </li>
                        <li> <label for="job-type-create"> <input type="checkbox" name="" id="job-type-create"> Job Type Create </label> </li>
                        <li> <label for="job-type-edit"> <input type="checkbox" name="" id="job-type-edit"> Job Type Edit </label> </li>
                        <li> <label for="job-type-delete"> <input type="checkbox" name="" id="job-type-delete"> Job Type Delete </label> </li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-4">
                    <label for="report"> <input type="checkbox" name="" id="report" class="menu-head"> Report </label>
                    <ul class="menu">
                        <li> <label for="visit-report"> <input type="checkbox" name="" id="visit-report"> Visit Report </label> </li>
                        <li> <label for="order-report"> <input type="checkbox" name="" id="order-report"> Order Report </label> </li>
                    </ul>
                </div>
            </div>
          
        </div>
      </div>
    </div>
  </div>

@endsection

@push('js')
  <script>
      $('.menu-head').on('change', function() {
          if ($(this).is(':checked')) {
            $(this).parent().siblings('.menu').find('input[type=checkbox]').prop('checked', true);
          } else {
            $(this).parent().siblings('.menu').find('input[type=checkbox]').prop('checked', false);
          }
      });
  
      $('input[type=checkbox]:not(.menu-head)').on('change', function() {
          if ($(this).is(':checked')) {
            $(this).parents('.menu').parent().find('.menu-head').prop('checked', true);
          } else {
            var check = [];
            $(this).parents('.menu').find('input[type=checkbox]').each((i, el) => {
                check.push(el.checked);
            });
            if (true != isInArray(true, check)) {
                $(this).parents('.menu').parent().find('.menu-head').prop('checked', false);
            }
          }
      });

    function isInArray(value, array) {
        return array.indexOf(value) > -1;
    }
  </script>
@endpush