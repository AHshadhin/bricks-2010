@extends('layouts.master')

@push('css')
<link rel="stylesheet" href="{{ asset('assets/jquery-ui/jquery-ui.css') }}">
@endpush

@section('content')

<div class="app-title">
    <div>
        <h1><i class="fa fa-user-plus"></i> Add Employee</h1>
    </div>
    <div>
        <a href="{{ route('employee.index') }}" class="btn btn-dark">
            <i class="fa fa-arrow-left"></i> Back
        </a>
    </div>
</div>
  
<div class="row">
    <div class="col-md-12">
      <div class="tile">
        <div class="tile-body">
          
            <form action="{{ route('employee.store') }}" method="post" enctype="multipart/form-data">
                @csrf

                <div class="row">

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="employee_id">Employee ID <span class="text-danger">*</span> </label>
                            <input type="text" name="employee_id" id="employee_id" value="{{ $new_id }}" readonly class="form-control">
                            @error('employee_id')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="employee_name">Employee Name <span class="text-danger">*</span> </label>
                            <input type="text" name="employee_name" id="employee_name" value="{{ old('employee_name') }}" class="form-control">
                            @error('employee_name')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="employee_picture">Employee Picture</label>
                            <input type="file" name="employee_picture" id="employee_picture" class="form-control-file">
                            @error('employee_picture')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="employee_father">Employee Father Name <span class="text-danger">*</span> </label>
                            <input type="text" name="employee_father" id="employee_father" value="{{ old('employee_father') }}" class="form-control">
                            @error('employee_father')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="employee_date_of_birth">Employee Date Of Birth <span class="text-danger">*</span> </label>
                            <input type="text" name="employee_date_of_birth" id="employee_date_of_birth" value="{{ old('employee_date_of_birth') }}" class="form-control" autocomplete="off">
                            @error('employee_date_of_birth')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="employee_blood_group">Employee Blood Group</label>
                            <select name="employee_blood_group" id="employee_blood_group" class="form-control">
                                <option value="" hidden>--- Select Blood Group ---</option>
                                @foreach ($blood_groups as $bg)
                                    <option value="{{ $bg }}" {{ old('employee_blood_group') == $bg ? 'selected' : '' }}>{{ $bg }}</option>
                                @endforeach
                            </select>
                            @error('employee_blood_group')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="employee_phone">Employee Phone <span class="text-danger">*</span> </label>
                            <input type="text" name="employee_phone" id="employee_phone" value="{{ old('employee_phone') }}" class="form-control">
                            @error('employee_phone')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="employee_address">Employee Address <span class="text-danger">*</span> </label>
                            <textarea name="employee_address" id="employee_address" class="form-control">{{ old('employee_address') }}</textarea>
                            @error('employee_address')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="employee_post">Employee Post <span class="text-danger">*</span> </label>
                            <select name="employee_post" id="employee_post" class="form-control">
                                <option value="" hidden>--- Select Post ---</option>
                                @foreach ($employeePosts as $employeePost)
                                    <option value="{{ $employeePost->id }}" {{ old('employee_post') == $employeePost->id ? 'selected' : '' }}>{{ $employeePost->post_name }}</option>
                                @endforeach
                            </select>
                            @error('employee_post')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="employee_department">Employee Department <span class="text-danger">*</span> </label>
                            <select name="employee_department" id="employee_department" class="form-control">
                                <option value="" hidden>--- Select Department ---</option>
                                @foreach ($employeeDepartments as $employeeDepartment)
                                    <option value="{{ $employeeDepartment->id }}" {{ old('employee_department') == $employeeDepartment->id ? 'selected' : '' }}>{{ $employeeDepartment->department_name }}</option>
                                @endforeach
                            </select>
                            @error('employee_department')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="employee_join_date">Employee Join Date <span class="text-danger">*</span> </label>
                            <input type="text" name="employee_join_date" id="employee_join_date" value="{{ old('employee_join_date') }}" class="form-control" autocomplete="off">
                            @error('employee_join_date')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="password">Employee Password <span class="text-danger">*</span> </label>
                            <input type="text" name="password" id="password" value="123456" class="form-control">
                            @error('password')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="col-md-6 offset-md-3">
                    <input type="submit" value="Submit" class="btn btn-info btn-block">
                </div>

            </form>

        </div>
      </div>
    </div>
  </div>

@endsection

@push('js')
<script src="{{ asset('assets/jquery-ui/jquery-ui.js') }}"></script>
  <script>
      $(function() {
            $("#employee_join_date, #employee_date_of_birth").datepicker({
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true
            });
        });
    $("#employee_picture").change(function() {
        readURL(this, 'employee_picture_preview');
    });
  </script>
@endpush