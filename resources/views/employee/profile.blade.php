@extends('layouts.master')

@push('css')
    
@endpush

@section('content')

<div class="app-title">
    <div>
        @if (Auth::user()->isAdmin == false)
        <h1><i class="fa fa-user"></i> My Information</h1>
        @else
        <h1><i class="fa fa-user"></i> Edit Information</h1>
        @endif
    </div>
    <div>
        <a href="{{ url()->previous() }}" class="btn btn-dark">
            <i class="fa fa-arrow-left"></i> Back
        </a>
    </div>
</div>
  
<div class="row">
    <div class="col-md-12">
      <div class="tile">
        <div class="tile-body">
            
            <div class="row">
                @if (Auth::user()->isAdmin == false)
                <div class="col-md-6">
                    <h4 class="mb-3">
                        <i class="fa fa-user"></i> Employement Information
                    </h4><hr>
                    <p>
                        <img src="{{ asset($employee->employee_picture) }}" height="100" alt="">
                    </p>
                    <table class="table table-borderless">
                        <tr>
                            <th>ID</th>
                            <th>:</th>
                            <td>{{ $employee->employee_id }}</td>
                        </tr>
                        <tr>
                            <th>Name</th>
                            <th>:</th>
                            <td>{{ $employee->employee_name }}</td>
                        </tr>
                        <tr>
                            <th>Father Name</th>
                            <th>:</th>
                            <td>{{ $employee->employee_father }}</td>
                        </tr>
                        <tr>
                            <th>Date of Birth</th>
                            <th>:</th>
                            <td>{{ date('D d F, Y', strtotime($employee->employee_date_of_birth)) }}</td>
                        </tr>
                        <tr>
                            <th>Blood Group</th>
                            <th>:</th>
                            <td>{{ $employee->employee_blood_group }}</td>
                        </tr>
                        <tr>
                            <th>Post</th>
                            <th>:</th>
                            <td>{{ $employee->employee_post->post_name }}</td>
                        </tr>
                        <tr>
                            <th>Department</th>
                            <th>:</th>
                            <td>{{ $employee->employee_department->department_name }}</td>
                        </tr>
                        <tr>
                            <th>Joining Date</th>
                            <th>:</th>
                            <td>{{ date('D d F, Y', strtotime($employee->employee_join_date)) }}</td>
                        </tr>
                        <tr>
                            <th>Phone</th>
                            <th>:</th>
                            <td>{{ $employee->employee_phone }}</td>
                        </tr>
                        <tr>
                            <th>Address</th>
                            <th>:</th>
                            <td>{{ $employee->employee_address }}</td>
                        </tr>
                    </table>
                </div>
                @endif
                <div class="col-md-6 {{ Auth::user()->isAdmin == true ? 'offset-md-3' : '' }}">
                    
                    @if (session('message'))
                        {!! session('message') !!}
                    @endif

                    <h4 class="mb-3">
                        <i class="fa fa-key"></i> Change Image
                    </h4><hr>

                    <form action="{{ route('employee.image.change') }}" method="post" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group row">
                            <label for="image" class="col-md-3">Image <span class="text-danger">*</span> </label>
                            <div class="col-md-9">
                                <input type="file" name="image" id="image" class="form-control-file">
                                @error('image')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-9 offset-md-3">
                                <input type="submit" value="Save changes" class="btn btn-primary">
                            </div>
                        </div>
                    </form>

                    <h4 class="mb-3 mt-5">
                        <i class="fa fa-key"></i> Edit Password
                    </h4><hr>

                    <form action="{{ route('password.new') }}" method="post">
                        @csrf

                        <div class="form-group row">
                            <label for="current_password" class="col-md-4">Current Password <span class="text-danger">*</span> </label>
                            <div class="col-md-8">
                                <input type="password" name="current_password" id="current_password" class="form-control">
                                @error('current_password')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="new_password" class="col-md-4">New Password <span class="text-danger">*</span> </label>
                            <div class="col-md-8">
                                <input type="password" name="new_password" id="new_password" class="form-control">
                                @error('new_password')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="confirm_password" class="col-md-4">Confirm Password <span class="text-danger">*</span> </label>
                            <div class="col-md-8">
                                <input type="password" name="confirm_password" id="confirm_password" class="form-control">
                                @error('confirm_password')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-8 offset-md-4">
                                <input type="submit" value="Save changes" class="btn btn-primary">
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
      </div>
    </div>
  </div>

@endsection

@push('js')
  
@endpush