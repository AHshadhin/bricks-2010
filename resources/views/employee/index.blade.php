@extends('layouts.master')

@push('css')
    
@endpush

@section('content')

<div class="app-title">
    <div>
        <h1><i class="fa fa-users"></i> Employees</h1>
    </div>
    <div>
        <a href="{{ route('employee.create') }}" class="btn btn-primary">
            <i class="fa fa-plus"></i> Add New Employee
        </a>
    </div>
</div>

@if (session('message'))
    {!! session('message') !!}
@endif
  
<div class="row">
    <div class="col-md-12">
      <div class="tile">
        <div class="tile-body">
          <div class="table-responsive">
            <table class="table table-hover table-bordered" id="sampleTable">
              <thead>
                <tr>
                  <th>#</th>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Photo</th>
                  <th>Department</th>
                  <th>Phone</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($employees as $key => $employee)
                  <tr>
                    <td>{{ ++$key }}</td>
                    <td>{{ $employee->employee_id }}</td>
                    <td>
                        {{ $employee->employee_name }}
                        <small class="d-block text-muted">{{ $employee->employee_post->post_name }}</small>
                    </td>
                    <td>
                        <img src="{{ asset($employee->employee_picture) }}" height="50" alt="Photo">
                    </td>
                    <td>{{ $employee->employee_department->department_name }}</td>
                    <td>{{ $employee->employee_phone }}</td>
                    <td>
                        <div class="btn-group">
                            <button type="button" data-id="{{ $employee->id }}" class="show-modal btn btn-dark btn-sm">
                                <i class="fa fa-eye"></i>
                            </button>
                            <a href="{{ route('employee.edit', $employee->id) }}" class="btn btn-info btn-sm">
                                <i class="fa fa-edit"></i>
                            </a>
                            <form action="{{ route('employee.destroy', $employee->id) }}" method="post">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="delete_button btn btn-danger btn-sm"> 
                                    <i class="fa fa-trash"></i> 
                                </button>
                            </form>
                        </div>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <div class="modal-header p-3">
            <h5 class="modal-title" id="exampleModalLabel">Employee Information</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          
          <div class="my-3 ml-4 mr-4">
            <p class="text-center">
                <img src="" alt="Photo" id="e_photo" height="100">
            </p>
            <table class="table table-borderless _table">
                <tr>
                    <th>Employee ID</th>
                    <th>:</th>
                    <td id="e_id"></td>
                </tr>
                <tr>
                    <th>Employee Name</th>
                    <th>:</th>
                    <td id="e_name"></td>
                </tr>
                <tr>
                    <th>Employee Fathers Name</th>
                    <th>:</th>
                    <td id="e_father"></td>
                </tr>
                <tr>
                    <th>Employee Date of Birth</th>
                    <th>:</th>
                    <td id="e_dob"></td>
                </tr>
                <tr>
                    <th>Employee Blood Group</th>
                    <th>:</th>
                    <td id="e_bg"></td>
                </tr>
                <tr>
                    <th>Employee Phone</th>
                    <th>:</th>
                    <td id="e_phone"></td>
                </tr>
                <tr>
                    <th>Employee Address</th>
                    <th>:</th>
                    <td id="e_address"></td>
                </tr>
                <tr>
                    <th>Employee Post</th>
                    <th>:</th>
                    <td id="e_post"></td>
                </tr>
                <tr>
                    <th>Employee Department</th>
                    <th>:</th>
                    <td id="e_department"></td>
                </tr>
                <tr>
                    <th>Employee Join Date</th>
                    <th>:</th>
                    <td id="e_jd"></td>
                </tr>
                <tr>
                    <th>Saved By</th>
                    <th>:</th>
                    <td id="e_sb"></td>
                </tr>
                <tr>
                    <th>Updated By</th>
                    <th>:</th>
                    <td id="e_ub"></td>
                </tr>
            </table>
          </div>
      </div>
    </div>
  </div>

@endsection

@push('js')
<script type="text/javascript" src="{{ asset('assets/') }}/js/plugins/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="{{ asset('assets/') }}/js/plugins/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="{{ asset('assets/') }}/js/sweetalert2@9.js"></script>
<script type="text/javascript">

    $('#sampleTable').DataTable();

    $(document).on('click', '.show-modal', function() {
        var this_id = $(this).data('id');
        if ('' != this_id) {
            var route = '{{ route('employee.show', ':id') }}';
            var _url = route.replace(':id', this_id);
            $.ajax({
                url: _url,
                method: 'GET',
                success: function(response) {
                    $('#e_photo').attr('src', response.employee_picture);
                    $('#e_id').text(response.employee_id);
                    $('#e_name').text(response.employee_name);
                    $('#e_father').text(response.employee_father);
                    $('#e_post').text(response.employee_post.post_name);
                    $('#e_department').text(response.employee_department.department_name);
                    $('#e_jd').text(response.employee_join_date);
                    $('#e_dob').text(response.employee_date_of_birth);
                    $('#e_address').text(response.employee_address);
                    $('#e_phone').text(response.employee_phone);
                    $('#e_bg').text(response.employee_blood_group);
                    $('#e_sb').text(response.saved_by.username);
                    if (response.updated_by != null) {
                        if (response.updated_by.username != null) {
                            $('#e_ub').text(response.updated_by.username);
                        } else {
                            $('#e_ub').text(response.updated_by.employee_name);
                        }
                    }

                    $('._table').children('tbody').children('tr').children('td').each(function() {
                        if ($(this).text() == '') {
                            $(this).closest('tr').remove();
                        }
                    });

                    $('.bd-example-modal-lg').modal('show');
                }
            });
        }
    });

    $(document).on('click', '.delete_button', function (e) {
        e.preventDefault();
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                $(this).closest('form').submit();
            }
        });
    });
</script>
@endpush