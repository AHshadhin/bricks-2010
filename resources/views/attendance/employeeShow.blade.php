@extends('layouts.master')

@push('css')
<style>
.info {
    position: relative;
}    
.date-wrapper {
    position: absolute;
    top: 0;
    right: 0;
}
.date-text {
    font-weight: bold;
}
.date {
    border: 1px solid #ced4da;
    padding: 3px 7px;
    border-radius: 5px;
    margin-left: 5px;
}
.time-item {
    display: inline-block;
    margin-right: 20px;
    font-weight: bold;
    font-size: 18px;
    letter-spacing: 1px;
}
.time-item:last-child {
    margin-right: 0;
}
.time-item label {
    cursor: pointer;
}
.time-wrapper {
    text-align: center;
    margin-top: 20px;
    margin-bottom: 10px;
}
</style>    
@endpush

@section('content')

<div class="app-title">
    <div>
        <h1><i class="fa fa-product-hunt"></i> Attendance</h1>
    </div>
</div>

@if (session('message'))
    {!! session('message') !!}
@endif
  
<div class="row">
    <div class="col-md-12">
      <div class="tile">
        <div class="tile-body">

        <div class="row">
            <div class="col-md-6 offset-md-3">

                <div class="info">
                    <table class="table table-borderless">
                        <tr>
                            <th style="width:100px">ID</th>
                            <th style="width:20px">:</th>
                            <td>{{ Auth::user()->employee_id }}</td>
                        </tr>
                        <tr>
                            <th>Name</th>
                            <th>:</th>
                            <td>{{ Auth::user()->employee_name }}</td>
                        </tr>
                        <tr>
                            <th>Designation</th>
                            <th>:</th>
                            <td>{{ optional(Auth::user()->employee_post)->post_name }}</td>
                        </tr>
                    </table>

                    <div class="date-wrapper">
                        <span class="date-text">Date : </span>
                        <span class="date">{{ date('d-m-Y') }}</span>
                    </div>

                </div>

                <div class="mt-5">
                    @if (session('error'))
                        <div class="alert alert-danger">{{ session('error') }}</div>
                    @elseif (session('success'))
                        <div class="alert alert-success">{{ session('success') }}</div>
                    @endif

                    <form action="{{ route('attendance.receive') }}" method="POST" class="border">
                        @csrf
                        <div class="time-wrapper">
                            <div class="time-item">
                                <label for="inTime">
                                    IN <input type="checkbox" name="in" id="inTime" {{ $in_exist ? 'checked disabled' : '' }}> 
                                    @if ($in_exist) {{ date('h:i:s A', strtotime($in_exist->in_time)) }} @endif
                                </label>
                            </div>
                            <div class="time-item">
                                <label for="outTime">
                                    OUT <input type="checkbox" name="out" id="outTime" {{ $out_exist ? 'checked disabled' : '' }}>
                                    @if ($out_exist) {{ date('h:i:s A', strtotime($out_exist->out_time)) }} @endif
                                </label>
                            </div>
                        </div>

                        <div class="text-center mb-3">
                            <button class="btn btn-primary" {{ $in_exist && $out_exist ? 'disabled' : '' }}>Save</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>

        <h5 class="mt-5 text-center">Current Month's Attendance</h5>
        <div class="table-responsive">
            <table class="table table-hover table-bordered text-center" id="sampleTable">
                <thead>
                <tr>
                    <th>SL</th>
                    <th>IN</th>
                    <th>OUT</th>
                    <th>Worked Hours</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($attendances as $k => $attendance)
                        <tr>
                            <td>{{ ++$k }}</td>
                            <td>{{ date('Y-m-d h:i:s a', strtotime($attendance->in_time)) }}</td>
                            <td>
                                @if ($attendance->out_time)
                                    {{ date('Y-m-d h:i:s a', strtotime($attendance->out_time)) }}
                                @endif
                            </td>
                            <td>
                                @php
                                    $datetime1 = new DateTime($attendance->in_time);
                                    $out_time = $attendance->out_time ?? date('H:i:s');
                                    $datetime2 = new DateTime($out_time);
                                    $interval = $datetime1->diff($datetime2);
                                    echo $interval->format('%h:%i:%s');
                                @endphp
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        </div>
      </div>
    </div>
  </div>

@endsection

@push('js')
<script type="text/javascript" src="{{ asset('assets/') }}/js/plugins/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="{{ asset('assets/') }}/js/plugins/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
    $('#sampleTable').DataTable();
</script>
@endpush