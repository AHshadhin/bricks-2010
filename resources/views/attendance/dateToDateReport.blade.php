<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Attendance Report | {{ $_settings->company_name }}</title>
    <style>
        body {font-family: sans-serif}
        h2, p { margin: 0; }
        h2 { margin-bottom: 10px; }
        table { width: 100%; border-collapse: collapse; margin-top: 30px; }
        table tr th, table tr td { border: 1px solid #8f8f8f; padding: 5px; }
    </style>
</head>
<body>

    <h2 style="text-align:center">
        {{ $_settings->company_name }}
    </h2>
    <p style="text-align:center">
        {{ $_settings->address }}
    </p>
    <hr>
    <p style="text-align:center">
        Attendance List ({{ date('M d, Y', strtotime($fromDate)) }} - {{ date('M d, Y', strtotime($toDate)) }})
    </p>

    <table>
        <thead>
            <tr>
                <th colspan="7">
                    {{ date('l, d F, Y') }}
                </th>
            </tr>
            <tr>
                <th>SL.</th>
                <th>E-ID</th>
                <th>Name</th>
                <th>Designation</th>
                <th>Mobile</th>
                <th>IN</th>
                <th>OUT</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($attendances as $k => $attendance)
                <tr>
                    <td style="text-align:center">{{ ++$k }}</td>
                    <td style="text-align:center">
                        {{ optional($attendance->employee)->employee_id }}
                    </td>
                    <td>{{ optional($attendance->employee)->employee_name }}</td>
                    <td>{{ optional($attendance->employee->employee_post)->post_name }}</td>
                    <td style="text-align:center">
                        {{ optional($attendance->employee)->employee_phone }}
                    </td>
                    <td style="text-align:center">
                        {{ date('h:i:s', strtotime($attendance->in_time)) }}
                    </td>
                    <td style="text-align:center">
                        @if ($attendance->out_time)
                            {{ date('h:i:s', strtotime($attendance->out_time)) }}
                        @else
                        @endif
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

</body>
</html>