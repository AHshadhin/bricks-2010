<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Employee Attendance Report | {{ $_settings->company_name }}</title>
    <style>
        body {font-family: sans-serif}
        h2, p { margin: 0; }
        h2 { margin-bottom: 10px; }
        .attendance-table { width: 100%; border-collapse: collapse; margin-top: 10px; }
        .attendance-table tr th, .attendance-table tr td { border: 1px solid #8f8f8f; padding: 5px; }
    </style>
</head>
<body>

    <h2 style="text-align:center">
        {{ $_settings->company_name }}
    </h2>
    <p style="text-align:center">
        {{ $_settings->address }}
    </p>
    <hr>
    <p style="text-align:center">
        Attendance List ({{ date('M d, Y', strtotime($fromDate)) }} - {{ date('M d, Y', strtotime($toDate)) }})
    </p>

    <div style="margin-top: 30px">
        <table>
            <tr>
                <th style="text-align:left">E-ID</th>
                <th style="width: 25px">:</th>
                <th style="text-align:left">
                    {{ $employee->employee_id }}
                </th>
            </tr>
            <tr>
                <th style="text-align:left">Name</th>
                <th>:</th>
                <th style="text-align:left">
                    {{ $employee->employee_name }}
                </th>
            </tr>
            <tr>
                <th style="text-align:left">Designation</th>
                <th>:</th>
                <th style="text-align:left">
                    {{ optional($employee->employee_post)->post_name }}
                </th>
            </tr>
            <tr>
                <th style="text-align:left">Department</th>
                <th>:</th>
                <th style="text-align:left">
                    {{ optional($employee->employee_department)->department_name }}
                </th>
            </tr>
        </table>
    </div>

    <table class="attendance-table">
        <thead>
            <tr>
                <th>SL.</th>
                <th>Attendance Date</th>
                <th>Week Day</th>
                <th>IN</th>
                <th>OUT</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($attendances as $k => $attendance)
                <tr>
                    <td style="text-align:center">{{ ++$k }}</td>
                    <td style="text-align:center">
                        {{ date('d F Y', strtotime($attendance->in_time)) }}
                    </td>
                    <td style="text-align:center">
                        {{ date('l', strtotime($attendance->in_time)) }}
                    </td>
                    <td style="text-align:center">
                        {{ date('h:i:s', strtotime($attendance->in_time)) }}
                    </td>
                    <td style="text-align:center">
                        @if ($attendance->out_time)
                            {{ date('h:i:s', strtotime($attendance->out_time)) }}
                        @else
                        @endif
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

</body>
</html>