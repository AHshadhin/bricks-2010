@extends('layouts.master')

@push('css')
<link rel="stylesheet" href="{{ asset('assets/jquery-ui/jquery-ui.css') }}">    
@endpush

@section('content')

<div class="app-title">
    <div>
        <h1><i class="fa fa-product-hunt"></i> Attendance</h1>
    </div>
</div>

@if (session('message'))
    {!! session('message') !!}
@endif
  
<div class="row">
    <div class="col-md-12">
      <div class="tile">
        <div class="tile-body">

        <form action="{{ route('attendance.search') }}" target="_blank" method="GET">
            <div class="row">
                <div class="col-md-3">
                    <input type="text" name="from_date" id="from" class="form-control" placeholder="From">
                </div>
                <div class="col-md-3">
                <input type="text" name="to_date" id="to" class="form-control" placeholder="To">
                </div>
                <div class="col-md-3">
                    <select name="employee"class="form-control">
                        <option value="">--- Employee ---</option>
                        @foreach ($employees as $employee)
                            <option value="{{ $employee->id }}">{{ $employee->employee_name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-3">
                    <input type="submit" value="Search" class="btn btn-primary">
                </div>
            </div>
        </form>


        <h5 class="mt-5 text-center">Today's Attendance</h5>
        <div class="table-responsive">
            <table class="table table-hover table-bordered text-center" id="sampleTable">
                <thead>
                <tr>
                    <th>SL</th>
                    <th>Employee Name</th>
                    <th>IN</th>
                    <th>OUT</th>
                    <th>Worked Hours</th>
                    <th>Location Map</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($attendances as $k => $attendance)
                        <tr>
                            <td>{{ ++$k }}</td>
                            <td>{{ optional($attendance->employee)->employee_name }}</td>
                            <td>{{ date('Y-m-d h:i:s a', strtotime($attendance->in_time)) }}</td>
                            <td>
                                @if ($attendance->out_time)
                                    {{ date('Y-m-d h:i:s a', strtotime($attendance->out_time)) }}
                                @endif
                            </td>
                            <td>
                                @php
                                    $datetime1 = new DateTime($attendance->in_time);
                                    $out_time = $attendance->out_time ?? date('H:i:s');
                                    $datetime2 = new DateTime($out_time);
                                    $interval = $datetime1->diff($datetime2);
                                    echo $interval->format('%h:%i:%s');
                                @endphp
                            </td>
                            <td>
                                @if ($attendance->in_latitude && $attendance->in_longitude)
                                <button data-latitude="{{ $attendance->in_latitude }}" data-longitude="{{ $attendance->in_longitude }}" class="show-modal btn btn-info btn-sm">IN</button>
                                @endif
                                
                                @if ($attendance->out_latitude && $attendance->out_longitude)
                                <button data-latitude="{{ $attendance->out_latitude }}" data-longitude="{{ $attendance->out_longitude }}" class="show-modal btn btn-info btn-sm">OUT</button>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        </div>
      </div>
    </div>
  </div>

  <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <div class="modal-header p-3">
            <h5 class="modal-title" id="exampleModalLabel">Location Map (<span id="A_type"></span>)</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

          <div class="mx-3 mb-4" id="map">
            <iframe class="w-100" style="min-height:300px"></iframe>
          </div>

      </div>
    </div>
  </div>

@endsection

@push('js')
<script type="text/javascript" src="{{ asset('assets/') }}/js/plugins/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="{{ asset('assets/') }}/js/plugins/dataTables.bootstrap.min.js"></script>
<script src="{{ asset('assets/jquery-ui/jquery-ui.js') }}"></script>

<script type="text/javascript">

    $(document).on('click', '.show-modal', function() {
        var thisText = $(this).text();
        if (thisText == 'IN') $('#A_type').text('IN');
        else $('#A_type').text('OUT');
        var latitude = $(this).data('latitude');
        var longitude = $(this).data('longitude');
        $('#map iframe').show().attr('src', 'https://maps.google.com/maps?q='+ latitude +','+longitude +'&hl=es;z=14&output=embed');
        $('.bd-example-modal-lg').modal('show');
    });

    $('#sampleTable').DataTable();

    $("#from, #to").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true
    }).datepicker("setDate", new Date());

</script>
@endpush