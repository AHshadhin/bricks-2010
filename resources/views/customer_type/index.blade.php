@extends('layouts.master')

@push('css')
    
@endpush

@section('content')

<div class="app-title">
    <div>
        <h1><i class="fa fa-tasks"></i> Customer Types</h1>
    </div>
</div>

  
<div class="row">
    <div class="col-md-12">
      <div class="tile">
        <div class="tile-body">

  
            <div class="row">
                <div class="col-md-6 offset-md-3">
                    @if (isset($customer_type))
                    <form action="{{ route('customer.type.update') }}" method="post">
                        @csrf
                        <input type="hidden" name="id" value="{{ $customer_type->id }}">
                    @else
                    <form action="{{ route('customer.type.store') }}" method="post">
                        @csrf 
                    @endif

                        <div class="form-group row">
                            <label for="type" class="col-md-3">Customer Type</label>
                            <div class="col-md-9">
                                <input type="text" name="type" id="type" value="{{ $customer_type->type ?? '' }}" class="form-control">
                                @error('type')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-9 offset-md-3">
                                <input type="submit" value="{{ isset($customer_type) ? 'Update' : 'Submit' }}" class="btn btn-primary">
                            </div>
                        </div>
                    </form>
                </div>
            </div>


            @if (session('message'))
                {!! session('message') !!}
            @endif

          <div class="table-responsive mt-3">
            <table class="table table-hover table-bordered" id="sampleTable">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Type</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($customer_types as $key => $customer_type)
                  <tr>
                    <td>{{ ++$key }}</td>
                    <td>{{ $customer_type->type }}</td>
                    <td>
                        <div class="btn-group">
                            <a href="{{ route('customer.type.edit', $customer_type->id) }}" class="btn btn-info btn-sm">
                                <i class="fa fa-edit"></i>
                            </a>
                            @can('admin-only', Auth::user())
                            <form action="{{ route('customer.type.delete', $customer_type->id) }}" method="post">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="delete_button btn btn-danger btn-sm"> 
                                    <i class="fa fa-trash"></i> 
                                </button>
                            </form>
                            @endcan
                        </div>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection

@push('js')
<script type="text/javascript" src="{{ asset('assets/') }}/js/plugins/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="{{ asset('assets/') }}/js/plugins/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="{{ asset('assets/') }}/js/sweetalert2@9.js"></script>
<script type="text/javascript">


    $('#sampleTable').DataTable();

    $(document).on('click', '.delete_button', function (e) {
        e.preventDefault();
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                $(this).closest('form').submit();
            }
        });
    });
</script>
@endpush