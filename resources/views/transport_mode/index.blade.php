@extends('layouts.master')

@push('css')
    
@endpush

@section('content')

<div class="app-title">
    <div>
        <h1><i class="fa fa-truck"></i> Mode of Transport</h1>
    </div>
</div>

  
<div class="row">
    <div class="col-md-12">
      <div class="tile">
        <div class="tile-body">

  
            <div class="row">
                <div class="col-md-6 offset-md-3">
                    @if (isset($transport))
                    <form action="{{ route('transport.update') }}" method="post">
                        @csrf
                        <input type="hidden" name="id" value="{{ $transport->id }}">
                    @else
                    <form action="{{ route('transport.store') }}" method="post">
                        @csrf 
                    @endif

                        <div class="form-group row">
                            <label for="transport_type" class="col-md-3">Transport Type</label>
                            <div class="col-md-9">
                                <input type="text" name="transport_type" id="transport_type" value="{{ $transport->transport_name ?? '' }}" class="form-control">
                                @error('transport_type')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-9 offset-md-3">
                                <input type="submit" value="{{ isset($transport) ? 'Update' : 'Submit' }}" class="btn btn-primary">
                            </div>
                        </div>
                    </form>
                </div>
            </div>


            @if (session('message'))
                {!! session('message') !!}
            @endif

          <div class="table-responsive mt-3">
            <table class="table table-hover table-bordered" id="sampleTable">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Transport Type</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($transports as $key => $t)
                  <tr>
                    <td>{{ ++$key }}</td>
                    <td>{{ $t->transport_name }}</td>
                    <td>
                        <div class="btn-group">
                            <a href="{{ route('transport.edit', $t->id) }}" class="btn btn-info btn-sm">
                                <i class="fa fa-edit"></i>
                            </a>
                            @can('admin-only', Auth::user())
                            <form action="{{ route('transport.delete', $t->id) }}" method="post">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="delete_button btn btn-danger btn-sm"> 
                                    <i class="fa fa-trash"></i> 
                                </button>
                            </form>
                            @endcan
                        </div>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection

@push('js')
<script type="text/javascript" src="{{ asset('assets/') }}/js/plugins/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="{{ asset('assets/') }}/js/plugins/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="{{ asset('assets/') }}/js/sweetalert2@9.js"></script>
<script type="text/javascript">


    $('#sampleTable').DataTable();

    $(document).on('click', '.delete_button', function (e) {
        e.preventDefault();
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                $(this).closest('form').submit();
            }
        });
    });
</script>
@endpush