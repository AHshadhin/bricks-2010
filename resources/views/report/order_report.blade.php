@extends('layouts.master')

@push('css')
<link rel="stylesheet" href="{{ asset('assets/jquery-ui/jquery-ui.css') }}">
@endpush

@section('content')

<div class="app-title">
    <div>
        <h1><i class="fa fa-file"></i> Order Report</h1>
    </div>
    <div>
        <a href="{{ route('home') }}" class="btn btn-dark">
            <i class="fa fa-arrow-left"></i> Back
        </a>
    </div>
</div>

@if (session('message'))
    {!! session('message') !!}
@endif
  
<div class="row">
    <div class="col-md-12">
      <div class="tile">
        <div class="tile-body" style="font-size: 16px">
          
            <div class="row">
                <div class="col-md-3 mb-1">
                    <div class="row">
                        <label for="" class="col-md-4">Employee</label>
                        <div class="col-md-8">
                            <select id="employee_id" class="form-control">
                                <option value="">All</option>
                                @foreach ($employees as $employee)
                                    <option value="{{ $employee->id }}">
                                        {{ ucfirst($employee->employee_name ?? $employee->username) }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 mb-1">
                    <div class="row">
                        <label for="" class="col-md-4">Customer</label>
                        <div class="col-md-8">
                            <select id="customer_id" class="form-control">
                                <option value="">All</option>
                                @foreach ($customers as $customer)
                                    <option value="{{ $customer->id }}">
                                        {{ $customer->company_name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 mb-1">
                    <div class="row">
                        <label for="" class="col-md-4">Product</label>
                        <div class="col-md-8">
                            <select id="product_id" class="form-control">
                                <option value="">All</option>
                                @foreach ($products as $product)
                                    <option value="{{ $product->id }}">
                                        {{ $product->product_name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 mb-1">
                    <div class="row">
                        <label for="" class="col-md-4">R. Type</label>
                        <div class="col-md-8">
                            <select id="record_type" class="form-control">
                                <option value="without">Without Details</option>
                                <option value="with">With Details</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 mb-1">
                    <div class="row">
                        <label for="" class="col-md-4">From</label>
                        <div class="col-md-8">
                            <input type="text" id="from_date" autocomplete="off" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="col-md-3 mb-1">
                    <div class="row">
                        <label for="" class="col-md-4">To</label>
                        <div class="col-md-8">
                            <input type="text" id="to_date" autocomplete="off" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="col-md-3 mb-1">
                    <button id="btn-search" class="btn btn-primary">Search</button>
                    <button id="btn-print" onclick="printDiv('report-table')" style="display:none" class="btn btn-dark">
                        <i class="fa fa-print"></i>
                    </button>
                </div>
            </div>

            <div class="table-responsive mt-4" id="report-table"></div>

        </div>
      </div>
    </div>
  </div>
@endsection

@push('js')
<script type="text/javascript" src="{{ asset('assets/') }}/js/plugins/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="{{ asset('assets/') }}/js/plugins/dataTables.bootstrap.min.js"></script>
<script src="{{ asset('assets/jquery-ui/jquery-ui.js') }}"></script>
  <script>

        $('#btn-search').on('click', function() {
            var employee_id = $('#employee_id');
            var customer_id = $('#customer_id');
            var product_id = $('#product_id');
            var record_type = $('#record_type');
            var from_date = $('#from_date');
            var to_date = $('#to_date');

            $.ajax({
                url: "{{ route('report.order') }}",
                method: "POST",
                data: {
                    _token: '{{ csrf_token() }}',
                    employee_id: employee_id.val(),
                    customer_id: customer_id.val(),
                    product_id: product_id.val(),
                    from_date: from_date.val(),
                    to_date: to_date.val()
                },
                success: function(response) {
                    // console.log(response);
                    if (response.length) {
                        $('#btn-print').show();
                        var table = `
                        <div>
                            <h2 style="text-align:center; margin:4px 0px; padding;0">{{ $_settings->company_name }}</h2>
                            <h4 style="text-align:center; margin:4px 0px; padding;0">Order List</h4>
                            <p style="text-align:center; margin:4px 0px; padding;0">From : `+ from_date.val() +` --- To : `+ to_date.val() +`</p>
                        </div>
                        <table style="border:1px solid gray; border-collapse:collapse; width:100%; margin-bottom: 10px">
                            <tr>
                                <th style="border:1px solid gray; padding: 2px 7px">Invoice No.</th>
                                <th style="border:1px solid gray; padding: 2px 7px; width: 102px">Date</th>
                                <th style="border:1px solid gray; padding: 2px 7px">Customer Name</th>
                                <th style="border:1px solid gray; padding: 2px 7px">Project Name</th>
                                <th style="border:1px solid gray; padding: 2px 7px">Employee Name</th>
                                <th style="border:1px solid gray; padding: 2px 7px">Sub Total</th>
                                <th style="border:1px solid gray; padding: 2px 7px">VAT</th>
                                <th style="border:1px solid gray; padding: 2px 7px">Grand Total</th>
                            </tr>
                        `;
                        for (let i = 0; i < response.length; i++) {
                            const el = response[i];
                            if (el.order_products.length) {
                                table += `
                                    <tr>
                                        <td style="border:1px solid gray; padding: 2px 7px">`+ el.invoice_number +`</td>
                                        <td style="border:1px solid gray; padding: 2px 7px">`+ el.order_date +`</td>
                                        <td style="border:1px solid gray; padding: 2px 7px">`+ el.customer.company_name +`</td>
                                        <td style="border:1px solid gray; padding: 2px 7px">`+ el.project.project_name +`</td>
                                        <td style="border:1px solid gray; padding: 2px 7px">`+ (el.saved_by.employee_name ? el.saved_by.employee_name : el.saved_by.username) +`</td>
                                        <td style="border:1px solid gray; padding: 2px 7px">`+ el.sub_total +`</td>
                                        <td style="border:1px solid gray; padding: 2px 7px">`+ (el.tax != null ? el.tax : 0.00) +`</td>
                                        <td style="border:1px solid gray; padding: 2px 7px">`+ el.grand_total +`</td>
                                    <tr>
                                `;
                                if (record_type.val() == 'with') {
                                    var details = '';
                                    var subTotal = 0;
                                    for (let j = 0; j < el.order_products.length; j++) {
                                        const item = el.order_products[j];
                                        subTotal += item.rate * item.quantity;
                                        details += '<tr>';
                                        details += '<td>' + (item.product != null ? item.product.product_name : '') + '</td>';
                                        details += '<td>' + item.rate + '</td>';
                                        details += '<td>' + item.quantity + '</td>';
                                        details += '<td>' + (item.rate * item.quantity).toFixed(2) + '</td>';
                                        details += '</tr>';
                                    }
                                    table += `
                                    <tr>
                                        <td colspan="9">
                                            <div class="row">
                                            <div class="col-md-6">
                                                <table class="table">
                                                    <tr>
                                                        <th>Product</th>
                                                        <th>Rate</th>
                                                        <th>Quantity</th>
                                                        <th>Amount</th>
                                                    </tr>
                                                    `+ details +`
                                                </table>
                                            </div>
                                            <div class="col-md-6">
                                                <table class="table-borderless">
                                                    <tr>
                                                        <th class="w-25">Sub Total</th>
                                                        <th>:</th>
                                                        <th>`+ (subTotal).toFixed(2) +`</th>
                                                    </tr>
                                                    <tr>
                                                        <th>TAX</th>
                                                        <th>:</th>
                                                        <th>`+ ((subTotal * el.tax) / 100).toFixed(2) +`</th>
                                                    </tr>
                                                    <tr>
                                                        <th>Grand Total</th>
                                                        <th>:</th>
                                                        <th>`+ (subTotal + ((subTotal * el.tax) / 100)).toFixed(2) +`</th>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        </td>
                                    </tr>
                                    `;
                                }
                            }
                        }
                        table += `<table>`;
                        $('#report-table').html(table);
                    } else {
                        $('#btn-print').hide();
                        var m = `<p class="text-center text-danger">No data found</p>`;
                        $('#report-table').html(m);
                    }
                }
            });
        });

      $(function() {
            $("#from_date, #to_date").datepicker({
                dateFormat: 'dd-mm-yy',
                changeMonth: true,
                changeYear: true
            }).datepicker("setDate", new Date());
        });
   </script>
   <script>
    function printT(el) {
        var rp = document.body.innerHTML;
        var pc = document.getElementById(el).innerHTML;
        document.body.innerHTML = pc;
        window.print();
        document.body.innerHTML = rp;
        // location.reload();
    }
    function printDiv(div) 
    {
        var divToPrint=document.getElementById(div);
        var newWin=window.open('','Print-Window');
        newWin.document.open();
        newWin.document.write(
            `
            <html>
                <body onload="window.print()">` + divToPrint.innerHTML + `</body>
            </html>
        `
        );
        newWin.document.close();
        newWin.close();
    }
    </script>
@endpush