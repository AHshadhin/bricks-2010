@extends('layouts.master')

@push('css')
<link rel="stylesheet" href="{{ asset('assets/jquery-ui/jquery-ui.css') }}">
@endpush

@section('content')

<div class="app-title">
    <div>
        <h1><i class="fa fa-file"></i> Visit Report</h1>
    </div>
    <div>
        <a href="{{ route('home') }}" class="btn btn-dark">
            <i class="fa fa-arrow-left"></i> Back
        </a>
    </div>
</div>

@if (session('message'))
    {!! session('message') !!}
@endif
  
<div class="row">
    <div class="col-md-12">
      <div class="tile">
        <div class="tile-body" style="font-size: 16px">
          
            <form action="{{ route('report.visit') }}" method="get">
                <div class="row">
                    <div class="col-md-4">
                        <div class="row">
                            <label for="" class="col-md-3">Employee</label>
                            <div class="col-md-9">
                                <select name="employee_id" id="employee_id" class="form-control">
                                    <option value="">All</option>
                                    @foreach ($employees as $employee)
                                        <option value="{{ $employee->id }}">
                                            {{ ucfirst($employee->employee_name ?? $employee->username) }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="row">
                            <label for="" class="col-md-2">From</label>
                            <div class="col-md-10">
                                <input type="text" name="from_date" id="from_date" autocomplete="off" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="row">
                            <label for="" class="col-md-2">To</label>
                            <div class="col-md-10">
                                <input type="text" name="to_date" id="to_date" autocomplete="off" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <button type="submit" class="btn btn-primary">Search</button>
                        <button id="btn-print" onclick="printDiv('report-table')" class="btn btn-dark">
                            <i class="fa fa-print"></i>
                        </button>
                    </div>
                </div>
            </form>

            <div id="report-table">
                <div class="mt-4">
                    <h4 style="text-align:center">
                        Visit List
                    </h4>
                    <p style="text-align:center">
                        <b>From :</b> <span class="mr-3">{{ (request()->get('from_date') != null) ? date('d-m-Y', strtotime(request()->get('from_date'))) : '' }}</span> 
                        <b>To :</b> {{ (request()->get('to_date') != null) ? date('d-m-Y', strtotime(request()->get('to_date'))) : '' }}
                    </p>
                </div>
    
                <div class="table-responsive">
                    <table id="visit__table" style="width:100%;border:1px solid gray;border-collapse:collapse">
                        <thead>
                            <tr style="border:1px solid gray">
                                <th style="border:1px solid gray;padding:5px;text-align:center"> # </th>
                                <th style="border:1px solid gray;padding:5px;text-align:center">Name of Employee</th>
                                <th style="border:1px solid gray;padding:5px;text-align:center">Employee ID</th>
                                <th style="border:1px solid gray;padding:5px;text-align:center">For Company</th>
                                <th style="border:1px solid gray;padding:5px;text-align:center">Customer Name</th>
                                <th style="border:1px solid gray;padding:5px;text-align:center">Customer ID</th>
                                <th style="border:1px solid gray;padding:5px;text-align:center">Job Type</th>
                                <th style="border:1px solid gray;padding:5px;text-align:center">Date</th>
                                <th style="border:1px solid gray;padding:5px;text-align:center">Authorising Person</th>
                                <th style="border:1px solid gray;padding:5px;text-align:center">Mode of Transport</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (isset($visits))
                                @foreach ($visits as $vk => $visit)
                                    <tr>
                                        <td style="border:1px solid gray;padding:5px;text-align:center">{{ ++$vk }}</td>
                                        <td style="border:1px solid gray;padding:5px;text-align:center">{{ $visit->saved_by->employee_name ? $visit->saved_by->employee_name : $visit->saved_by->username }}</td>
                                        <td style="border:1px solid gray;padding:5px;text-align:center">{{ $visit->saved_by->employee_id ? $visit->saved_by->employee_id : '---' }}</td>
                                        <td style="border:1px solid gray;padding:5px;text-align:center">{{ $visit->company_name ? $visit->company_name : $visit->company->name }}</td>
                                        <td style="border:1px solid gray;padding:5px;text-align:center">{{ $visit->customer->company_name }}</td>
                                        <td style="border:1px solid gray;padding:5px;text-align:center">{{ $visit->customer->customer_id }}</td>
                                        <td style="border:1px solid gray;padding:5px;text-align:center">{{ $visit->job_type->type }}</td>
                                        <td style="border:1px solid gray;padding:5px;text-align:center">{{ $visit->visit_date }}</td>
                                        <td style="border:1px solid gray;padding:5px;text-align:center">{{ $visit->reference_by->employee_name ? $visit->reference_by->employee_name : $visit->reference_by->username }}</td>
                                        <td style="border:1px solid gray;padding:5px;text-align:center">{{ $visit->transport_mode->transport_name }}</td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
      </div>
    </div>
  </div>
@endsection

@push('js')
<script type="text/javascript" src="{{ asset('assets/') }}/js/plugins/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="{{ asset('assets/') }}/js/plugins/dataTables.bootstrap.min.js"></script>
<script src="{{ asset('assets/jquery-ui/jquery-ui.js') }}"></script>
  <script>

        $('#visit__table').DataTable({
            "bSort" : false,
            "paging": false,
            "info": false,
        "aoColumnDefs": [
            { "bSortable": false, "aTargets": [] }
        ],
        initComplete: function () {
                this.api().columns([1,2,3,4,5,6,7,8,9]).every( function () {
                    var column = this;
                    var select = $('<select><option value="">All</option></select>')
                        .appendTo( $(column.header()) )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );
    
                            column
                                .search( val ? '^'+val+'$' : '', true, false )
                                .draw();
                        } );
    
                    column.data().unique().sort().each( function ( d, j ) {
                        select.append( '<option>'+d+'</option>' )
                    } );
                } );
            }
        });


      $(function() {
            $("#from_date, #to_date").datepicker({
                dateFormat: 'dd-mm-yy',
                changeMonth: true,
                changeYear: true
            }).datepicker("setDate", new Date());
        });
   </script>
   <script>
    function printDiv(div) 
    {
        var divToPrint=document.getElementById(div);
        var newWin=window.open('','Print-Window');
        newWin.document.open();
        newWin.document.write(
            `
            <html>
                <head>
                    
                </head>
                <body onload="window.print()">` + divToPrint.innerHTML + `</body>
            </html>
        `
        );
        newWin.document.close();
        newWin.close();
    }
    </script>
@endpush