@extends('layouts.master')

@push('css')
    
@endpush

@section('content')

<div class="app-title">
    <div>
        <h1><i class="fa fa-tasks"></i> Customer Projects</h1>
    </div>
    <div>
        <a href="{{ route('project.create') }}" class="btn btn-primary">
            <i class="fa fa-plus"></i> Add New Project
        </a>
    </div>
</div>

@if (session('message'))
    {!! session('message') !!}
@endif
  
<div class="row">
    <div class="col-md-12">
      <div class="tile">
        <div class="tile-body">
          <div class="table-responsive">
            <table class="table table-hover table-bordered" id="sampleTable">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Customer</th>
                  <th>Project Name</th>
                  <th>Description</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($customer_projects as $key => $customer_project)
                  <tr>
                    <td>{{ ++$key }}</td>
                    <td>
                        {{ $customer_project->customer->company_name }}
                        <small class="d-block">{{ $customer_project->customer->phone }}</small>
                    </td>
                    <td>{{ $customer_project->project_name }}</td>
                    <td>{{ $customer_project->project_description ?? '---' }}</td>
                    <td>
                        <div class="btn-group">
                            <a href="{{ route('project.edit', $customer_project->id) }}" class="btn btn-info btn-sm">
                                <i class="fa fa-edit"></i>
                            </a>
                            @can('admin-only', Auth::user())
                            <form action="{{ route('project.destroy', $customer_project->id) }}" method="post">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="delete_button btn btn-danger btn-sm"> 
                                    <i class="fa fa-trash"></i> 
                                </button>
                            </form>
                            @endcan
                        </div>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection

@push('js')
<script type="text/javascript" src="{{ asset('assets/') }}/js/plugins/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="{{ asset('assets/') }}/js/plugins/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="{{ asset('assets/') }}/js/sweetalert2@9.js"></script>
<script type="text/javascript">

    $('#sampleTable').DataTable();

    $(document).on('click', '.delete_button', function (e) {
        e.preventDefault();
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                $(this).closest('form').submit();
            }
        });
    });
</script>
@endpush