@extends('layouts.master')

@push('css')
    
@endpush

@section('content')

<div class="app-title">
    <div>
        <h1><i class="fa fa-tasks"></i> Edit Project</h1>
    </div>
    <div>
        <a href="{{ route('project.index') }}" class="btn btn-dark">
            <i class="fa fa-arrow-left"></i> Back
        </a>
    </div>
</div>
  
<div class="row">
    <div class="col-md-6 offset-md-3">
      <div class="tile">
        <div class="tile-body">
          
            <form action="{{ route('project.update', $project->id) }}" method="post">
                @csrf
                @method('PUT')

                <div class="form-group row">
                    <label class="col-md-3" for="customer">Customer <span class="text-danger">*</span> </label>
                    <div class="col-md-9">
                        <select name="customer" id="customer" class="form-control">
                            <option value="" hidden>--- Select Customer ---</option>
                            @foreach ($customers as $customer)
                                <option value="{{ $customer->id }}" {{ $project->customer_id == $customer->id ? 'selected' : '' }}>
                                    {{ $customer->company_name }}
                                </option>
                            @endforeach
                        </select>
                        @error('customer')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-3" for="project_name">Project Name <span class="text-danger">*</span> </label>
                    <div class="col-md-9">
                        <input type="text" name="project_name" id="project_name" value="{{ $project->project_name }}" class="form-control">
                        @error('project_name')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-3" for="project_description">Description </label>
                    <div class="col-md-9">
                        <textarea name="project_description" id="project_description" class="form-control">{{ $project->project_description }}</textarea>
                        @error('project_description')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-9 offset-md-3">
                        <input type="submit" value="Update" class="btn btn-info">
                    </div>
                </div>

            </form>

        </div>
      </div>
    </div>
  </div>

@endsection

@push('js')

@endpush