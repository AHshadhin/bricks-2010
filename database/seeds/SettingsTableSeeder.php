<?php

use App\Models\Setting;
use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $s = new Setting;
        $s->company_name = 'Company Name';
        $s->phone_1 = '01234567890';
        $s->email_1 = 'email@mail.com';
        $s->address = 'Company Address';
        $s->save();
    }
}
