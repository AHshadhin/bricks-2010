<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('invoice_number');
            $table->date('order_date');
            $table->unsignedInteger('customer_id');
            $table->unsignedInteger('project_id');
            $table->date('delivery_date');
            $table->text('delivery_address')->nullable();
            $table->decimal('tax', 8, 2)->nullable();
            $table->tinyInteger('status')->default(1)->comment('1 (pending), 2 (archive), 3 (delete), 4(processing)');
            $table->unsignedInteger('saved_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
