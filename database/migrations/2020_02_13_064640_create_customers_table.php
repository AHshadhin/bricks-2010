<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('customer_id');
            $table->string('company_name');
            $table->string('contact_person');
            $table->string('area')->nullable();
            $table->text('address');
            $table->string('phone');
            $table->unsignedInteger('customer_type_id');
            $table->unsignedInteger('marketing_officer_id');
            $table->boolean('isApproved')->default(0);
            $table->boolean('isDelete')->default(0);
            $table->unsignedInteger('saved_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
