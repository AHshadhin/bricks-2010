<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVisitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visits', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('visit_date');
            $table->unsignedInteger('customer_id')->nullable();
            $table->unsignedInteger('company_id')->nullable();
            $table->unsignedInteger('job_type_id')->nullable();
            $table->unsignedInteger('reference_by_id')->nullable();
            $table->unsignedInteger('transport_mode_id')->nullable();
            $table->text('comment')->nullable();
            $table->string('location')->nullable();
            $table->boolean('isDelete')->default(0);
            $table->unsignedInteger('saved_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visits');
    }
}
