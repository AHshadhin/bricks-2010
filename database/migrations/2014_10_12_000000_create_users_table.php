<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('username')->nullable();
            $table->string('employee_id')->nullable();
            $table->boolean('isAdmin')->default(0)->comment('0 (employee), 1 (admin)');
            $table->string('password');
            $table->string('employee_name')->nullable();
            $table->string('employee_father')->nullable();
            $table->unsignedInteger('employee_post_id')->nullable();
            $table->unsignedInteger('employee_department_id')->nullable();
            $table->date('employee_join_date')->nullable();
            $table->date('employee_date_of_birth')->nullable();
            $table->text('employee_address')->nullable();
            $table->string('employee_phone')->nullable();
            $table->string('employee_blood_group')->nullable();
            $table->text('employee_picture')->nullable();
            $table->boolean('isDelete')->default(0);
            $table->unsignedInteger('saved_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
