<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'AuthController@showLoginForm')->name('login');
Route::post('/login', 'AuthController@loginProcess')->name('login.process');
Route::post('/logout', 'AuthController@logoutProcess')->name('logout.process');

Route::group(['middleware' => 'auth'], function() {

    Route::get('/home', 'HomeController@index')->name('home');

    Route::resource('/employee', 'EmployeeController')->middleware('admin');
    
    // Route::view('/p', 'employee.permission');
    // Route::get('/pdf', function () {
    //     $pdf = PDF::loadView('attendance.pdf', []);
	//     return $pdf->stream('document.pdf');
    // });

    Route::get('/attendance', 'AttendanceController@getAttendances')->name('attendance');
    Route::post('/attendance/receive', 'AttendanceController@attendanceStore')->name('attendance.receive');
    Route::get('/attendance/search', 'AttendanceController@attendanceSearch')->name('attendance.search');

    Route::get('/profile', 'EmployeeController@profile')->name('employee.profile');
    Route::post('/password-reset', 'EmployeeController@resetPassword')->name('password.new');
    Route::post('/image/change', 'EmployeeController@imageChange')->name('employee.image.change');

    Route::resource('/customer', 'CustomerController');
    Route::get('/customer/{id}/approve', 'CustomerController@approve')->name('customer.approve');

    Route::resource('/project', 'CustomerProjectController');

    Route::get('/setup/types', 'CustomerTypeController@index')->name('customer.types');
    Route::post('/setup/type/store', 'CustomerTypeController@store')->name('customer.type.store');
    Route::get('/setup/type/{id}/edit', 'CustomerTypeController@edit')->name('customer.type.edit');
    Route::post('/setup/type/update', 'CustomerTypeController@update')->name('customer.type.update');
    Route::delete('/setup/type/{id}/delete', 'CustomerTypeController@destroy')->name('customer.type.delete');

    Route::resource('/product', 'ProductController');

    Route::get('/visits', 'VisitController@index')->name('visits');
    Route::get('/visit/create', 'VisitController@create')->name('visit.create');
    Route::post('/visit/store', 'VisitController@store')->name('visit.store');
    Route::get('/visit/{id}/show', 'VisitController@show')->name('visit.show');
    Route::get('/visit/{id}/edit', 'VisitController@edit')->name('visit.edit');
    Route::put('/visit/update', 'VisitController@update')->name('visit.update');
    Route::delete('/visit/{id}/destroy', 'VisitController@destroy')->name('visit.destroy');

    Route::get('/orders/{with_details?}', 'OrderController@index')->name('orders');
    Route::get('/order/create', 'OrderController@create')->name('order.create');
    Route::post('/order/confirm', 'OrderController@confirmOrder')->name('order.confirm');
    Route::get('/order/{id}/invoice', 'OrderController@invoice')->name('order.invoice');

    Route::get('/order/{id}/show', 'OrderController@show')->name('order.show');
    Route::get('/order/{id}/edit', 'OrderController@edit')->name('order.edit');
    Route::post('/order/update', 'OrderController@update')->name('order.update');
    Route::post('/order/delete', 'OrderController@destroy')->name('order.destroy');

    Route::get('/order/{id}/delivery', 'OrderDeliveryController@deliveryForm')->name('order.delivery');
    Route::post('/order/delivery', 'OrderDeliveryController@deliveryProcess')->name('order.delivery.process');

    // oid = order id, bid = delivery batch id
    Route::get('/order/{oid}/{bid}/chalan', 'OrderDeliveryController@chalan')->name('order.delivery.chalan');

    Route::get('/pending-orders', 'OrderController@pending')->name('order.pending');
    Route::get('/archive-orders', 'OrderController@archive')->name('order.archive');

    Route::group(['middleware'=>'admin'], function() {

        Route::resource('/setup/post', 'EmployeePostController')->except('show');
        Route::resource('/setup/department', 'EmployeeDepartmentController')->except('show');
        Route::resource('/setup/company', 'CompanyController')->except('show');
        
        Route::get('/settings', 'SettingsController@otherSettings')->name('settings');
        Route::post('/settings/update', 'SettingsController@settingsUpdate')->name('settings.update');

        Route::get('/setup/transports', 'TransportModeController@index')->name('transports');
        Route::post('/setup/transport/store', 'TransportModeController@store')->name('transport.store');
        Route::get('/setup/transport/{id}/edit', 'TransportModeController@edit')->name('transport.edit');
        Route::post('/setup/transport/update', 'TransportModeController@update')->name('transport.update');
        Route::delete('/setup/transport/{id}/delete', 'TransportModeController@destroy')->name('transport.delete');

        Route::get('/setup/job-types', 'JobTypeController@index')->name('job.types');
        Route::post('/setup/job-type/store', 'JobTypeController@store')->name('job.type.store');
        Route::get('/setup/job-type/{id}/edit', 'JobTypeController@edit')->name('job.type.edit');
        Route::post('/setup/job-type/update', 'JobTypeController@update')->name('job.type.update');
        Route::delete('/setup/job-type/{id}/delete', 'JobTypeController@destroy')->name('job.type.delete');
    });

    Route::get('/report/visit-report', 'ReportController@visitReport')->name('report.visit-report');
    Route::get('/report/visit', 'ReportController@visitReportProcess')->name('report.visit');

    Route::get('/report/order-report', 'ReportController@orderReport')->name('report.order-report');
    Route::post('/report/order', 'ReportController@orderReportProcess')->name('report.order');

});


