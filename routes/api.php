<?php

use App\User;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('/login', 'Api\AuthController@login');

Route::group(['middleware' => 'api.auth', 'namespace' => 'Api'], function() {

    Route::get('/getEmployeeList', 'EmployeeController@getEmployees');

    Route::get('/singleEmployeeData/{id}', 'EmployeeController@getEmployee');

    Route::post('/employee/password-reset', 'EmployeeController@resetPassword');

    Route::get('/getProductList', 'ProductController@getProducts');

    Route::post('/getOrderList', 'OrderController@getOrders');
    Route::post('/addNewOrderData', 'OrderController@storeOrder');
    Route::post('/updateOrderData', 'OrderController@updateOrder');
    
    Route::get('/getJobTypeList', 'JobTypeController@getjobtype');
    Route::post('/addNewJobType', 'JobTypeController@addNewJobType');

    Route::get('/getTransportTypeList', 'TransportModeController@transporttype');
    Route::post('/addNewTransportMode', 'TransportModeController@addNewTransportMode');

    Route::get('/singleCusomerType', 'CustomerTypeController@getcustomertype');

    Route::get('/getCompanyList', 'CompanyController@getcompany');
    Route::post('/addNewCompany', 'CompanyController@addNewCompany');
    
    Route::get('/getVisitList', 'VisitController@getAllVisitInfo');
    Route::post('/addNewVisitData', 'VisitController@storeNewVisitInfo');
    Route::get('/singleVisitData/{id}', 'VisitController@getVisitInfo');
    Route::post('/updateVisitData', 'VisitController@updateVisitInfo');
    Route::get('/deleteVisitData/{id}', 'VisitController@deleteVisitInfo');


    Route::get('/getCustomerTypeList', 'CustomerController@getCustomerTypes');

    Route::get('/customers', 'CustomerController@getCustomers');
    Route::post('/customer/store', 'CustomerController@storeCustomerInfo');
    Route::get('/customer/{id}', 'CustomerController@getCustomerInfo');
    Route::post('/customer/update', 'CustomerController@updateCustomerInfo');
    Route::get('/customer/{id}/delete', 'CustomerController@deleteCustomerInfo');

    Route::get('/getProjectList/{customerId}', 'CustomerProjectController@getprojects');
    Route::post('/addCustomerProject', 'CustomerProjectController@storeProjectInfo');

    Route::get('/getCurrentMonthAttendance', 'AttendanceController@currentMonthAttendances');
    Route::post('/attendanceIn', 'AttendanceController@attendanceIN');
    Route::post('/attendanceOut', 'AttendanceController@attendanceOUT');
    
});



