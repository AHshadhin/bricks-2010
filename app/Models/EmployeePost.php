<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class EmployeePost extends Model
{
    protected $fillable = ['post_name', 'post_description', 'isDelete'];

    public function employee() {
        return $this->hasOne(User::class);
    }
}
