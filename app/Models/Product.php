<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'product_id',
        'product_name',
        'product_price',
        'isDelete',
        'saved_by',
        'updated_by'
    ];

    public function savedBy() {
        return $this->belongsTo(User::class, 'saved_by', 'id');
    }

    public function updatedBy() {
        return $this->belongsTo(User::class, 'updated_by', 'id');
    }
}
