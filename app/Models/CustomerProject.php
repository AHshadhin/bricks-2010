<?php

namespace App\Models;

use App\Models\Customer;
use Illuminate\Database\Eloquent\Model;

class CustomerProject extends Model
{
    protected $fillable = ['customer_id', 'project_name', 'project_description', 'isDelete', 'saved_by'];

    public function customer() {
        return $this->belongsTo(Customer::class);
    }
}
