<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmployeeDepartment extends Model
{
    protected $fillable = ['department_name', 'department_description', 'isDelete'];

    public function employee() {
        return $this->hasOne(User::class);
    }
}
