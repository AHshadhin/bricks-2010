<?php

namespace App\Models;

use App\User;
use App\Models\CustomerType;
use App\Models\CustomerProject;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = [
        'customer_id',
        'company_name',
        'contact_person',
        'area',
        'address',
        'phone',
        'customer_type_id',
        'marketing_officer_id',
        'isApproved',
        'isDelete',
        'saved_by',
        'updated_by'
    ];

    public function savedBy() {
        return $this->belongsTo(User::class, 'saved_by', 'id');
    }

    public function updatedBy() {
        return $this->belongsTo(User::class, 'updated_by', 'id');
    }

    public function projects() {
        return $this->hasMany(CustomerProject::class);
    }

    public function customer_type() {
        return $this->belongsTo(CustomerType::class);
    }

    public function marketing_officer() {
        return $this->belongsTo(User::class, 'marketing_officer_id', 'id');
    }
}
