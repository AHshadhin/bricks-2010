<?php

namespace App\Models;

use App\User;
use App\Models\Company;
use App\Models\JobType;
use App\Models\Customer;
use App\Models\TransportMode;
use Illuminate\Database\Eloquent\Model;

class Visit extends Model
{
    protected $fillable = [
        'visit_date',
        'customer_id',
        'company_id',
        'job_type_id',
        'reference_by_id',
        'transport_mode_id',
        'comment',
        'location',
        'isDelete',
        'saved_by',
        'updated_by'
    ];

    public function savedBy() {
        return $this->belongsTo(User::class, 'saved_by', 'id');
    }

    public function updatedBy() {
        return $this->belongsTo(User::class, 'updated_by', 'id');
    }

    public function referenceBy() {
        return $this->belongsTo(User::class, 'reference_by_id', 'id');
    }

    public function transportMode() {
        return $this->belongsTo(TransportMode::class);
    }

    public function company() {
        return $this->belongsTo(Company::class);
    }

    public function jobType() {
        return $this->belongsTo(JobType::class);
    }

    public function customer() {
        return $this->belongsTo(Customer::class);
    }
}
