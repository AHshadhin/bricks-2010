<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransportMode extends Model
{
    protected $fillable = ['transport_name', 'isDelete'];
}
