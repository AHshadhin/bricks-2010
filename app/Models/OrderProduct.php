<?php

namespace App\Models;

use App\Models\Order;
use App\Models\OrderDelivery;
use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
    protected $fillable = [
        'order_id',
        'product_id',
        'quantity',
        'rate',
        'amount'
    ];

    public function order() {
        return $this->belongsTo(Order::class);
    }

    public function product() {
        return $this->belongsTo(Product::class);
    }

    public function deliveryProducts() {
        return $this->hasMany(OrderDelivery::class);
    }
}
