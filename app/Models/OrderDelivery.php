<?php

namespace App\Models;

use App\User;
use App\Models\Order;
use App\Models\Product;
use App\Models\OrderProduct;
use Illuminate\Database\Eloquent\Model;

class OrderDelivery extends Model
{
    protected $fillable = ['order_id', 'order_product_id', 'quantity', 'delivery_batch', 'saved_by'];

    public function order() {
        return $this->belongsTo(Order::class);
    }

    public function orderProduct() {
        return $this->belongsTo(OrderProduct::class);
    }

    public function savedBy() {
        return $this->belongsTo(User::class, 'saved_by', 'id');
    }
}
