<?php

namespace App\Models;

use App\User;
use App\Models\Customer;
use App\Models\OrderProduct;
use App\Models\OrderDelivery;
use App\Models\CustomerProject;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'order_id',
        'order_date',
        'customer_id',
        'project_id',
        'delivery_date',
        'tax',
        'status',
        'saved_by',
        'updated_by'
    ];

    public function customer() {
        return $this->belongsTo(Customer::class);
    }

    public function orderProducts() {
        return $this->hasMany(OrderProduct::class);
    }

    public function project() {
        return $this->belongsTo(CustomerProject::class);
    }

    public function deliveryProducts() {
        return $this->hasMany(OrderDelivery::class);
    }

    public function savedBy() {
        return $this->belongsTo(User::class, 'saved_by', 'id');
    }
}
