<?php

namespace App;

use App\Models\EmployeePost;
use App\Models\EmployeeDepartment;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'employee_id',
        'role',
        'password',
        'employee_name',
        'employee_father',
        'employee_post_id',
        'employee_department_id',
        'employee_join_date',
        'employee_date_of_birth',
        'employee_address',
        'employee_phone',
        'employee_blood_group',
        'employee_picture',
        'isDelete',
        'saved_by',
        'updated_by'
    ];

    public function savedBy() {
        return $this->belongsTo(User::class, 'saved_by', 'id');
    }

    public function updatedBy() {
        return $this->belongsTo(User::class, 'updated_by', 'id');
    }

    public function employee_post() {
        return $this->belongsTo(EmployeePost::class);
    }

    public function employee_department() {
        return $this->belongsTo(EmployeeDepartment::class);
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        
    ];
}
