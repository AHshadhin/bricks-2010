<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\Customer;
use App\Models\CustomerType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class CustomerController extends Controller
{
    public function index() {
        if ($this->isAdmin()) {
            $customers = Customer::where('isDelete', 0)->latest()->get();
        } else {
            $customers = Customer::where('isDelete', 0)->where('saved_by', Auth::id())->latest()->get();
        }
        return view('customer.index', compact('customers'));
    }

    public function create() {
        return view('customer.create', [
            'new_id' => $this->generateCustomerId(),
            'customer_types' => CustomerType::where('isDelete', 0)->get(),
            'employees' => User::where('isAdmin', 0)->where('isDelete', 0)->latest()->get()
        ]);
    }

    public function store(Request $request) {
        $this->validateCustomerInfo($request);

        $newCustomer = [
            'customer_id' => $this->generateCustomerId(),
            'company_name' => $request->company_name,
            'contact_person' => $request->contact_person,
            'area' => $request->area,
            'address' => $request->address,
            'phone' => $request->phone,
            'customer_type_id' => $request->customer_type,
            'saved_by' => Auth::user()->id
        ];

        if ($this->isAdmin()) {
            $newCustomer['isApproved'] = 1;
            $newCustomer['marketing_officer_id'] = $request->marketing_officer;
        } else {
            $newCustomer['marketing_officer_id'] = Auth::user()->id;
        }

        try {
            Customer::create($newCustomer);
            $this->message('success', 'Customer info save successfully.');
        } catch (\Exception $e) {
            $this->message('error', $e->getMessage());
        }
        return redirect()->route('customer.index');
    }

    private function validateCustomerInfo($request) {
        $request->validate([
            'company_name' => 'required',
            'contact_person' => 'required',
            'phone' => 'required|min:11|max:15',
            'address' => 'required',
            'customer_type' => 'required'
        ]);

        if ($this->isAdmin()) {
            $request->validate([
                'marketing_officer' => 'required'
            ]);
        }
    }

    public function show($id) {
        $customer = Customer::with('projects', 'customer_type', 'marketing_officer')->findOrFail($id);
        $customer->saved_by = $customer->savedBy;
        $customer->updated_by = $customer->updatedBy;
        $customer->customer_type_name = $customer->customer_type->type;
        $customer->marketing_officer = $customer->marketing_officer; 
        return response()->json($customer);
    }

    public function edit($id) {
        $customer = Customer::findOrFail($id);
        $customer_types = CustomerType::where('isDelete', 0)->get();
        $employees = User::where('isAdmin', 0)->where('isDelete', 0)->latest()->get();
        if ($this->isOwn($customer->saved_by) || $this->isAdmin()) {
            return view('customer.edit', compact('customer', 'customer_types', 'employees'));
        }
        abort(403);
    }

    public function update(Request $request, $id) {
        $this->validateCustomerInfo($request);
        $customer = Customer::findOrFail($id);
        if ($this->isOwn($customer->saved_by) || $this->isAdmin()) {
            try {
                $customer->company_name = $request->company_name;
                $customer->contact_person = $request->contact_person;
                $customer->phone = $request->phone;
                $customer->area = $request->area;
                $customer->address = $request->address;
                $customer->customer_type_id = $request->customer_type;
                if ($this->isAdmin()) {
                    $customer->marketing_officer_id = $request->marketing_officer;
                } else {
                    $customer->marketing_officer_id = Auth::user()->id;
                }
                $customer->updated_by = Auth::user()->id;
                $customer->save();
    
                $this->message('success', 'Customer info update successfully.');
            } catch (\Exception $e) {
                $this->message('error', $e->getMessage());
            }
            return redirect()->route('customer.index');
        }
        abort(403);
    }

    public function destroy($id) {
        if (Gate::allows('admin-only', Auth::user())) {
            try {
                $customer = Customer::findOrFail($id);
                $customer->isDelete = 1;
                $customer->save();
    
                $this->message('success', 'Customer info delete successfully');
            } catch (\Exception $e) {
                $this->message('error', $e->getMessage());
            }
            return redirect()->route('customer.index');
        }
        return redirect()->route('home');
    }

    public function approve($id) {
        if (Gate::allows('admin-only', Auth::user())) {
            try {
                $customer = Customer::findOrFail($id);
                $customer->isApproved = 1;
                $customer->save();
    
                $this->message('success', 'Customer successfully approved');
            } catch (\Exception $e) {
                $this->message('error', $e->getMessage());
            }
            return redirect()->route('customer.index');
        }
        return redirect()->route('home');
    }
}
