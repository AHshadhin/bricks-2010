<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\Customer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    // \Request::route()->getName();

    public function imageUpload($request, $name, $destination)
    {
        if ($request->hasFile($name)) {
            $thumbnail_image = $request->file($name);
            $original_name_split = explode('.', $thumbnail_image->getClientOriginalName());
            $generated_name  = $original_name_split[0].'_'.time().uniqid();
            $thumbnail_name = $generated_name.'.'.$thumbnail_image->getClientOriginalExtension();
            $_thubnail_path = $thumbnail_image->move($destination, $thumbnail_name);
            $thubnail_path = str_replace('\\', '/', $_thubnail_path);
            return $thubnail_path ?? false;
        }
    }

    public function message($type, $message)
    {
        if ('success' == $type) {
            $_message = '<div class="alert alert-success mb-3">' . $message . '</div>';
        } elseif ('error' == $type) {
            $_message = '<div class="alert alert-danger mb-3">' . $message . '</div>';
        }
        Session::flash('message', $_message);
    }

    public function isAdmin() {
        if (Auth::user()->isAdmin == 1) {
            return true;
        }
        return false;
    }

    public function isOwn($id) {
        if (Auth::id() == $id) {
            return true;
        }
        return false;
    }

    public function apiIsAdmin() {
        $user = User::find(app('currentUserId'));
        if ($user->isAdmin == 1) {
            return true;
        }
        return false;
    }

    public function generateEmployeeId() {
        $oldCodes = User::pluck('employee_id');
        if (count(array_filter((array) $oldCodes)) > 0) {
            if ($oldCodes->max()) {
                $oldMaxCode = preg_replace('/[A-Z]+/', '', $oldCodes->max());
                $prefix = preg_replace('/[0-9]+/', '', $oldCodes->max());
                $newCode = $prefix . (str_pad($oldMaxCode + 1, 4, 0, STR_PAD_LEFT));
            } else {
                $newCode = 'E0001';
            }
        } else {
            $newCode = 'E0001';
        }
        return $newCode;
    }

    public function generateCustomerId() {
        $oldCodes = Customer::pluck('customer_id');
        if (count(array_filter((array) $oldCodes)) > 0) {
            if ($oldCodes->max()) {
                $oldMaxCode = preg_replace('/[A-Z]+/', '', $oldCodes->max());
                $prefix = preg_replace('/[0-9]+/', '', $oldCodes->max());
                $newCode = $prefix . (str_pad($oldMaxCode + 1, 4, 0, STR_PAD_LEFT));
            } else {
                $newCode = 'C0001';
            }
        } else {
            $newCode = 'C0001';
        }
        return $newCode;
    }

}
