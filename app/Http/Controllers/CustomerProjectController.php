<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\Request;
use App\Models\CustomerProject;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class CustomerProjectController extends Controller
{
    public function index() {
        if ($this->isAdmin()) {
            $customer_projects = CustomerProject::where('isDelete', 0)->latest()->get();
        } else {
            $customer_projects = CustomerProject::where('isDelete', 0)
            ->where('saved_by', Auth::id())->latest()->get();
        }
        return view('customer_project.index', compact('customer_projects'));
    }

    public function create() {
        $customers = $this->getCustomers();
        return view('customer_project.create', compact('customers'));
    }

    private function getCustomers() {
        if ($this->isAdmin()) {
            $customers = Customer::where('isDelete', 0)->get();
        } else {
            $customers = Customer::where('isDelete', 0)->get();
        }
        return $customers;
    }

    public function store(Request $request) {
        $request->validate([
            'customer' => 'required|numeric',
            'project_name' => 'required'
        ]);

        try {
            CustomerProject::create([
                'customer_id' => $request->customer,
                'project_name' => $request->project_name,
                'project_description' => $request->project_description,
                'saved_by' => Auth::id()
            ]);
            $this->message('success', 'Project info save successfully.');
        } catch (\Exception $e) {
            $this->message('error', $e->getMessage());
        }
        return redirect()->route('project.index');
    }

    public function edit($id) {
        $project = CustomerProject::findOrFail($id);
        if ($this->isOwn($project->customer->saved_by) || $this->isAdmin()) {
            $customers = $this->getCustomers();
            return view('customer_project.edit', compact('project', 'customers'));
        }
        abort(403);
    }

    public function update(Request $request, $id) {
        $request->validate([
            'customer' => 'required|numeric',
            'project_name' => 'required'
        ]);

        $project = CustomerProject::findOrFail($id);
        try {
            $project->customer_id = $request->customer;
            $project->project_name = $request->project_name;
            $project->project_description = $request->project_description;
            $project->save();

            $this->message('success', 'Project info update successfully.');
        } catch (\Exception $e) {
            $this->message('error', $e->getMessage());
        }
        return redirect()->route('project.index');
    }

    public function show() {

    }

    public function destroy($id) {
        if (Gate::allows('admin-only', Auth::user())) {
            try {
                $project = CustomerProject::findOrFail($id);
                $project->isDelete = 1;
                $project->save();
    
                $this->message('success', 'Project info delete successfully');
            } catch (\Exception $e) {
                $this->message('error', $e->getMessage());
            }
            return redirect()->route('project.index');
        }
        return redirect()->route('home');
    }
}
