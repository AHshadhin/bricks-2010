<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    public function otherSettings() {
        $settings = Setting::first();
        return view('setting.settings', compact('settings'));
    }

    public function settingsUpdate(Request $request) {
        $request->validate([
            'tax' => 'required|numeric',
            'company_name' => 'required',
            'phone_1' => 'required',
            'email_1' => 'required',
            'address' => 'required'
        ]);

        $settings = Setting::first();
        $settings->tax = $request->tax;
        $settings->company_name = $request->company_name;
        $settings->phone_1 = $request->phone_1;
        $settings->phone_2 = $request->phone_2;
        $settings->email_1 = $request->email_1;
        $settings->email_2 = $request->email_2;
        $settings->address = $request->address;
        $settings->save();

        $this->message('success', 'Settings successfully updated');
        return redirect()->back();
    }
}
