<?php

namespace App\Http\Controllers;

use App\Models\CustomerType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CustomerTypeController extends Controller
{
    public function index() {
        $customer_types = CustomerType::where('isDelete', 0)->latest()->get();
        return view('customer_type.index', compact('customer_types'));
    }

    public function store(Request $request) {
        $request->validate([
            'type' => 'required|max:100'
        ]);

        try {
            CustomerType::create([
                'type' => $request->type
            ]);
            $this->message('success', 'Customer type save successfully');
        } catch (\Exception $e) {
            $this->message('error', $e->getMessage());
        }

        return redirect()->route('customer.types');
    }

    public function edit($id) {
        $customer_type = CustomerType::findOrFail($id);
        $customer_types = CustomerType::where('isDelete', 0)->latest()->get();
        return view('customer_type.index', compact('customer_types', 'customer_type'));
    }

    public function update(Request $request) {
        $request->validate([
            'id' => 'required',
            'type' => 'required|max:100'
        ]);

        try {
            $type = CustomerType::findOrFail($request->id);
            $type->update([
                'type' => $request->type
            ]);
            $this->message('success', 'Customer type update successfully');
        } catch (\Exception $e) {
            $this->message('error', $e->getMessage());
        }

        return redirect()->route('customer.types');
    }

    public function destroy($id) {

        try {
            $type = CustomerType::findOrFail($id);
            $type->update([
                'isDelete' => 1
            ]);
            $this->message('success', 'Customer type delete successfully');
        } catch (\Exception $e) {
            $this->message('error', $e->getMessage());
        }

        return redirect()->route('customer.types');
    }
}
