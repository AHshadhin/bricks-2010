<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Order;
use App\Models\Product;
use App\Models\Setting;
use App\Models\Customer;
use App\Models\OrderProduct;
use Illuminate\Http\Request;
use App\Models\CustomerProject;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class OrderController extends Controller
{
    public function index($with_details = null) {
        // if ($this->isAdmin()) {
        //     $orders = Order::with('customer')
        //     ->where('status', '!=', 3)
        //     ->latest()
        //     ->get();
        // } else {
        //     $orders = Order::with('customer')
        //     ->where('status', '!=', 3)
        //     ->where('saved_by', Auth::id())
        //     ->latest()
        //     ->get();
        // }
        $orders = Order::with('customer')
            ->where('status', '!=', 3)
            ->latest()
            ->get();
        $mode = '';
        return view('order.index', compact('orders', 'mode', 'with_details'));
    }

    public function pending() {
        if ($this->isAdmin()) {
            $orders = Order::with('customer')
            ->where('status', 1)
            ->latest()
            ->get();
        } else {
            $orders = Order::with('customer')
            ->where('status', 1)
            ->where('saved_by', Auth::id())
            ->latest()
            ->get();
        }
        $mode = 'p';
        return view('order.index', compact('orders', 'mode'));
    }

    public function archive() {
        if ($this->isAdmin()) {
            $orders = Order::with('customer')
            ->where('status', 2)
            ->latest()
            ->get();
        } else {
            $orders = Order::with('customer')
            ->where('status', 2)
            ->where('saved_by', Auth::id())
            ->latest()
            ->get();
        }
        $mode = 'a';
        return view('order.index', compact('orders', 'mode'));
    }

    public function create() {

        if ($this->isAdmin()) {
            $customers = Customer::where('isDelete', 0)->where('isApproved', 1)->latest()->get();
        } else {
            $customers = Customer::where('isDelete', 0)
            ->where('isApproved', 1)->where('saved_by', Auth::id())->latest()->get();
        }
                                
        $products = Product::where('isDelete', 0)
        ->latest()
        ->get();

        $tax = Setting::first()->tax;

        return view('order.create', compact('customers', 'products', 'tax'));
    }

    public function confirmOrder(Request $request) {
    
        if (!empty($request->customer_id) && !empty($request->delivery_date) && count($request->cart) > 0) {
            try {

                DB::beginTransaction();

                $order = new Order;
                $order->invoice_number = $this->generateInvoiceId();
                $order->order_date = date('Y-m-d');
                $order->customer_id = $request->customer_id;
                $order->project_id = $request->project_id;
                $order->delivery_date = date('Y-m-d', strtotime($request->delivery_date));
                $order->delivery_address = $request->delivery_address;
                $order->tax = $request->tax ? $request->tax : 0;
                $order->saved_by = Auth::id();
                $order->save();

                if (is_array($request->cart) && count($request->cart) > 0) {
                    foreach ($request->cart as $key => $item) {
                        $op = new OrderProduct;
                        $op->order_id = $order->id;
                        $op->product_id = $item['product_id'];
                        $op->quantity = $item['product_quantity'];
                        $op->rate = $item['product_price'];
                        $op->amount = $item['product_price'] * $item['product_quantity'];
                        $op->save();
                    }
                }

                DB::commit();

                return response()->json([
                    'success' => true,
                    'message' => 'Order successfully confirmed.',
                    'order_id' => $order->id
                ]);
            } catch (\Exception $e) {

                DB::rollback();

                return response()->json([
                    'success' => false,
                    'message' => $e->getMessage()
                ]);
            }
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Please fill out all empty fields.'
            ]);
        }
    }

    public function invoice($id) {
        $order = Order::with(['customer', 'orderProducts'])->findOrFail($id);
        if ($this->isOwn($order->saved_by) || $this->isAdmin()) {
            $sub_total =  $order->orderProducts()->sum('amount');
            $tax = $order->tax;
            return view('order.invoice', compact('order', 'sub_total', 'tax'));
        }
        abort(403);
    }

    public function generateInvoiceId(){
        $invoice = date('Y').date("m")."00001";
        $sales = Order::select('id')->latest()->first();
        if($sales != null){
            $newSalesId = $sales->id + 1;
            $zeros = array('0', '00', '000', '0000');
            $invoice = date('Y').date('m') . (strlen($newSalesId) > count($zeros) ? $newSalesId : $zeros[count($zeros) - strlen($newSalesId)] . $newSalesId);
        }
        return $invoice;
    }

    public function show($id) {
        $order = Order::with('customer', 'orderProducts')->findOrFail($id);
        $order->order_date = date('D d F, Y', strtotime($order->order_date));
        $order->delivery_date = date('D d F, Y', strtotime($order->delivery_date));
        return view('order.show', compact('order'));
    }

    public function edit($id) {

        $customers = Customer::where('isDelete', 0)
        ->where('isApproved', 1)
        ->latest()->get();
        
        $order = Order::with('orderProducts', 'customer')->findOrFail($id);
        $existIDs =  $order->orderProducts->pluck('product_id')->toArray();
        $products = Product::where('isDelete', 0)->whereNotIn('id', $existIDs)->latest()->get();

        if ($order->deliveryProducts->sum('quantity') < 1) {
            if ($this->isOwn($order->saved_by) || $this->isAdmin()) {
                return view('order.edit', compact('order', 'products', 'customers', 'projects'));
            }
        }
        
        abort(403);
    }

    public function update(Request $request) {
      
        $request->validate([
            'customer' => 'required',
            'project' => 'required',
            'order_id' => 'required',
            'delivery_date' => 'required'
        ]);

        $i = 0;
        $oldData = [];
        
        if (is_array($request->old_product_id) && count($request->old_product_id) > 0) {
            foreach ($request->old_product_id as $key => $value) {
                if (empty($value) || empty($request->old_product_quantity[$key])) {
                    $this->message('error', 'Fields must not be empty');
                    return redirect()->back();
                }
                $oldData[$i]['old_product_id'] = $value;
                $oldData[$i]['old_product_quantity'] = $request->old_product_quantity[$key];
                $i++;
            }
        }

        $j = 0;
        $newData = [];

        if (is_array($request->new_product_id) && count($request->new_product_id) > 0) {
            foreach ($request->new_product_id as $key => $value) {
                if (empty($value) || empty($request->new_product_quantity[$key])) {
                    $this->message('error', 'Fields must not be empty');
                    return redirect()->back();
                }
                $newData[$i]['new_product_id'] = $value;
                $newData[$i]['new_product_quantity'] = $request->new_product_quantity[$key];
                $j++;
            }
        }

        try {

            DB::beginTransaction();
            
            $order = Order::findOrFail($request->order_id);
            $order->customer_id = $request->customer;
            $order->project_id = $request->project;
            $order->delivery_date = $request->delivery_date;
            $order->delivery_address = $request->delivery_address;
            $order->status = $request->status;
            $order->updated_by = Auth::id();
            $order->save();
    
            if (count($newData) > 0) {
                foreach ($newData as $k1 => $item) {
                    $p = Product::findOrFail($item['new_product_id']);
                    OrderProduct::create([
                        'order_id' => $order->id,
                        'product_id' => $item['new_product_id'],
                        'quantity' => $item['new_product_quantity'],
                        'rate' => $p->product_price,
                        'amount' => $p->product_price * $item['new_product_quantity']
                    ]);
                }
            }

            if (count($oldData) > 0) {
                foreach ($oldData as $k2 => $item2) {
                    $op = OrderProduct::findOrFail($item2['old_product_id']);
                    $op->update([
                        'quantity' => $item2['old_product_quantity'],
                        'amount' => $op->product->product_price * $item2['old_product_quantity']
                    ]);
                }
            }

            DB::commit();

            $this->message('success', 'Order successfully updated.');
            return redirect()->back();
            
        } catch (\Exception $e) {

            DB::rollback();

            $this->message('error', $e->getMessage());
            return redirect()->back(); 
        }

        $this->message('error', 'Something went wrong');
        return redirect()->back(); 
    }

    public function destroy(Request $request) {
        if (Gate::allows('admin-only', Auth::user())) {
            try {
                $order = Order::findOrFail($request->id);
                $order->status = 3;
                $order->save();
    
                $this->message('success', 'Order info delete successfully');
            } catch (\Exception $e) {
                $this->message('error', $e->getMessage());
            }
            return redirect()->route('orders');
        }
        return redirect()->route('home');
    }

}
