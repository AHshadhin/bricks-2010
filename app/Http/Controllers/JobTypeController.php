<?php

namespace App\Http\Controllers;

use App\Models\JobType;
use Illuminate\Http\Request;

class JobTypeController extends Controller
{
    public function index() {
        $job_types = JobType::where('isDelete', 0)->latest()->get();
        return view('job_type.index', compact('job_types'));
    }

    public function store(Request $request) {
        $request->validate([
            'job_type' => 'required|max:100'
        ]);

        try {
            JobType::create([
                'type' => $request->job_type
            ]);
            $this->message('success', 'Job type save successfully');
        } catch (\Exception $e) {
            $this->message('error', $e->getMessage());
        }

        return redirect()->route('job.types');
    }

    public function edit($id) {
        $job_type = JobType::findOrFail($id);
        $job_types = JobType::where('isDelete', 0)->latest()->get();
        return view('job_type.index', compact('job_types', 'job_type'));
    }

    public function update(Request $request) {
        $request->validate([
            'id' => 'required',
            'job_type' => 'required|max:100'
        ]);

        try {
            $job_type = JobType::findOrFail($request->id);
            $job_type->update([
                'type' => $request->job_type
            ]);
            $this->message('success', 'Job type update successfully');
        } catch (\Exception $e) {
            $this->message('error', $e->getMessage());
        }

        return redirect()->route('job.types');
    }

    public function destroy($id) {

        try {
            $job_type = JobType::findOrFail($id);
            $job_type->update([
                'isDelete' => 1
            ]);
            $this->message('success', 'Job type delete successfully');
        } catch (\Exception $e) {
            $this->message('error', $e->getMessage());
        }

        return redirect()->route('job.types');
    }
}
