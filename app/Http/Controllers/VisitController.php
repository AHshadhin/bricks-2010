<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\Visit;
use App\Models\Company;
use App\Models\JobType;
use App\Models\Customer;
use Illuminate\Http\Request;
use App\Models\TransportMode;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class VisitController extends Controller
{
    public function index() {
        // if ($this->isAdmin()) {
        //     $visits = Visit::where('isDelete', 0)->latest()->get();
        // } else {
        //     $visits = Visit::where('isDelete', 0)
        //     ->where('saved_by', Auth::id())->latest()->get();
        // }
        $visits = Visit::with('savedBy')->where('isDelete', 0)->latest()->get();
        return view('visit.index', compact('visits'));
    }

    public function create() {
        return view('visit.create', [
            'employees' => User::where('isAdmin', 0)->where('isDelete', 0)->latest()->get(),
            'transports' => TransportMode::where('isDelete', 0)->latest()->get(),
            'companies' => Company::where('isDelete', 0)->latest()->get(),
            'job_types' => JobType::where('isDelete', 0)->latest()->get(),
            'customers' => Customer::where('isDelete', 0)->latest()->get()
        ]);
    }

    public function store(Request $request) {
       
        $this->validateVisitInfo($request);

        try {
            Visit::create([
                'visit_date' => date('Y-m-d', strtotime($request->visit_date)),
                'customer_id' => $request->customer,
                'company_id' => $request->company,
                'job_type_id' => $request->job_type,
                'reference_by_id' => $request->authorising_person,
                'transport_mode_id' => $request->mode_of_transport,
                'comment' => $request->comment,
                'saved_by' => Auth::user()->id
            ]);
            $this->message('success', 'Visit info save successfully.');
        } catch (\Exception $e) {
            $this->message('error', $e->getMessage());
        }
        return redirect()->route('visits');
    }

    private function validateVisitInfo($request) {

        $request->validate([
            'visit_date' => 'required|date',
            'customer' => 'required|numeric',
            'authorising_person' => 'required',
            'mode_of_transport' => 'required',
            'job_type' => 'required'
        ]);
    }

    public function show($id) {
        $visit = Visit::findOrFail($id);
        $visit->visit_date = date('D d F, Y', strtotime($visit->visit_date));
        $visit->job_type = $visit->jobType;
        $visit->reference_by = $visit->referenceBy;
        $visit->transport_mode = $visit->transportMode;
        $visit->company = $visit->company;
        $visit->saved_by = $visit->savedBy;
        $visit->updated_by = $visit->updatedBy;
        return response()->json($visit);
    }

    public function edit($id) {
        $visit = Visit::findOrFail($id);
        $employees = User::where('isAdmin', 0)->where('isDelete', 0)->latest()->get();
        $transports = TransportMode::where('isDelete', 0)->latest()->get();
        $companies = Company::where('isDelete', 0)->latest()->get();
        $job_types = JobType::where('isDelete', 0)->latest()->get();
        $customers = Customer::where('isDelete', 0)->latest()->get();
        if ($this->isOwn($visit->saved_by) || $this->isAdmin()) {
            return view('visit.edit', compact('visit', 'employees', 'transports', 'companies', 'job_types', 'customers'));
        }
        abort(403);
    }

    public function update(Request $request) {
        $this->validateVisitInfo($request);

        try {
            $visit = Visit::findOrFail($request->id);
            $visit->visit_date = date('Y-m-d', strtotime($request->visit_date));
            $visit->customer_id = $request->customer;
            $visit->company_id = $request->company;
            $visit->job_type_id = $request->job_type;
            $visit->reference_by_id = $request->authorising_person;
            $visit->transport_mode_id = $request->mode_of_transport;
            $visit->comment = $request->comment;
            $visit->updated_by = Auth::user()->id;
            $visit->save();

            $this->message('success', 'Visit info update successfully.');
        } catch (\Exception $e) {
            $this->message('error', $e->getMessage());
        }
        return redirect()->route('visits');
    }

    public function destroy($id) {
        if (Gate::allows('admin-only', Auth::user())) {
            try {
                $visit = Visit::findOrFail($id);
                $visit->isDelete = 1;
                $visit->save();
    
                $this->message('success', 'Visit info delete successfully');
            } catch (\Exception $e) {
                $this->message('error', $e->getMessage());
            }
            return redirect()->route('visits');
        }
        return redirect()->route('home');
    }
}
