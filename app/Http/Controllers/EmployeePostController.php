<?php

namespace App\Http\Controllers;

use App\Models\EmployeePost;
use Illuminate\Http\Request;

class EmployeePostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = EmployeePost::where('isDelete', 0)->latest()->get();
        return view('setting.post.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('setting.post.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'post_name' => 'required|max:180'
        ]);

        try {

            EmployeePost::create($request->all());

            $this->message('success', 'Employee post added successfully');
            return redirect()->route('post.index');

        } catch (\Exception $e) {

            $this->message('error', $e->getMessage());
            return redirect()->back();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EmployeePost  $employeePost
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employeePost = EmployeePost::findOrFail($id);
        return view('setting.post.edit', compact('employeePost'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EmployeePost  $employeePost
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $employeePost = EmployeePost::findOrFail($id);
        
        try {

            $employeePost->update($request->all());

            $this->message('success', 'Employee post update successfully');
            return redirect()->route('post.index');

        } catch (\Exception $e) {

            $this->message('error', $e->getMessage());
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EmployeePost  $employeePost
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employeePost = EmployeePost::findOrFail($id);

        try {

            $employeePost->update(['isDelete' => 1]);

            $this->message('success', 'Employee post delete successfully');
            return redirect()->route('post.index');

        } catch (\Exception $e) {

            $this->message('error', $e->getMessage());
            return redirect()->back();
        }
    }
}
