<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TransportMode;

class TransportModeController extends Controller
{
    public function index() {
        $transports = TransportMode::where('isDelete', 0)->latest()->get();
        return view('transport_mode.index', compact('transports'));
    }

    public function store(Request $request) {
        $request->validate([
            'transport_type' => 'required|max:100'
        ]);

        try {
            TransportMode::create([
                'transport_name' => $request->transport_type
            ]);
            $this->message('success', 'Transport type save successfully');
        } catch (\Exception $e) {
            $this->message('error', $e->getMessage());
        }

        return redirect()->route('transports');
    }

    public function edit($id) {
        $transport = TransportMode::findOrFail($id);
        $transports = TransportMode::where('isDelete', 0)->latest()->get();
        return view('transport_mode.index', compact('transports', 'transport'));
    }

    public function update(Request $request) {
        $request->validate([
            'id' => 'required',
            'transport_type' => 'required|max:100'
        ]);

        try {
            $transport = TransportMode::findOrFail($request->id);
            $transport->update([
                'transport_name' => $request->transport_type
            ]);
            $this->message('success', 'Transport type update successfully');
        } catch (\Exception $e) {
            $this->message('error', $e->getMessage());
        }

        return redirect()->route('transports');
    }

    public function destroy($id) {

        try {
            $transport = TransportMode::findOrFail($id);
            $transport->update([
                'isDelete' => 1
            ]);
            $this->message('success', 'Transport type delete successfully');
        } catch (\Exception $e) {
            $this->message('error', $e->getMessage());
        }

        return redirect()->route('transports');
    }
}
