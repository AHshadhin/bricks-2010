<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\EmployeePost;
use Illuminate\Http\Request;
use App\Models\EmployeeDepartment;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = User::where('isAdmin', 0)->where('isDelete', 0)->latest()->get();
        return view('employee.index', compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $employeePosts = EmployeePost::where('isDelete', 0)->get();
        $employeeDepartments = EmployeeDepartment::where('isDelete', 0)->get();
        $blood_groups = ['A+', 'O+', 'B+', 'AB+', 'A-', 'O-', 'B-', 'AB-'];
        $new_id = $this->generateEmployeeId();
        return view('employee.create', compact('blood_groups', 'new_id', 'employeePosts', 'employeeDepartments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateEmployeeInfo($request);

        try {
            User::create([
                'employee_id' => $this->generateEmployeeId(),
                'password' => bcrypt($request->password),
                'employee_name' => $request->employee_name,
                'employee_father' => $request->employee_father,
                'employee_post_id' => $request->employee_post,
                'employee_department_id' => $request->employee_department,
                'employee_join_date' => date('Y-m-d', strtotime($request->employee_join_date)),
                'employee_date_of_birth' => date('Y-m-d', strtotime($request->employee_date_of_birth)),
                'employee_blood_group' => $request->employee_blood_group,
                'employee_address' => $request->employee_address,
                'employee_phone' => $request->employee_phone,
                'employee_picture' => $this->imageUpload($request, 'employee_picture', 'uploads/employees'),
                'saved_by' => Auth::user()->id
            ]);
            $this->message('success', 'Employee info save successfully.');
        } catch (\Exception $e) {
            $this->message('error', $e->getMessage());
        }

        return redirect()->route('employee.index');
    }

    private function validateEmployeeInfo($request) {
        $request->validate([
            'employee_id' => 'required',
            'password' => 'required',
            'employee_name' => 'required',
            'employee_father' => 'required',
            'employee_post' => 'required',
            'employee_department' => 'required',
            'employee_join_date' => 'required|date',
            'employee_date_of_birth' => 'date',
            'employee_address' => 'required',
            'employee_phone' => 'required|max:15|min:11',
            'employee_picture' => 'image|mimes:png,jpg,jpeg'
        ]);
    }
    

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $employee = User::with('employee_post', 'employee_department')->findOrFail($id);
        $employee->employee_join_date = date('D d F, Y', strtotime($employee->employee_join_date));
        $employee->employee_date_of_birth = date('D d F, Y', strtotime($employee->employee_date_of_birth));
        $employee->saved_by = $employee->savedBy;
        $employee->updated_by = $employee->updatedBy;
        return response()->json($employee);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employeePosts = EmployeePost::where('isDelete', 0)->get();
        $employeeDepartments = EmployeeDepartment::where('isDelete', 0)->get();
        $blood_groups = ['A+', 'O+', 'B+', 'AB+', 'A-', 'O-', 'B-', 'AB-'];
        $employee = User::findOrFail($id);
        return view('employee.edit', compact('employee', 'blood_groups', 'employeePosts', 'employeeDepartments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validateEmployeeUpdateInfo($request);

        try {
            $employee = User::findOrFail($id);
            $employee->password = bcrypt($request->password);
            $employee->employee_name = $request->employee_name;
            $employee->employee_father = $request->employee_father;
            $employee->employee_post_id = $request->employee_post;
            $employee->employee_department_id = $request->employee_department;
            $employee->employee_join_date = date('Y-m-d', strtotime($request->employee_join_date));
            $employee->employee_date_of_birth = date('Y-m-d', strtotime($request->employee_date_of_birth));
            $employee->employee_blood_group = $request->employee_blood_group;
            $employee->employee_address = $request->employee_address;
            $employee->employee_phone = $request->employee_phone;
            if ($request->hasFile('employee_picture')) {
                if (file_exists($employee->employee_picture)) {
                    unlink($employee->employee_picture);
                }
                $employee->employee_picture = $this->imageUpload($request, 'employee_picture', 'uploads/employees');
            }
            $employee->updated_by = Auth::user()->id;
            $employee->save();
            
            $this->message('success', 'Employee info successfully updated');
        } catch (\Exception $e) {
            $this->message('error', $e->getMessage());
        }
        return redirect()->route('employee.index');
    }

    private function validateEmployeeUpdateInfo($request) {
        $request->validate([
            'employee_name' => 'required',
            'employee_father' => 'required',
            'employee_post' => 'required',
            'employee_department' => 'required',
            'employee_join_date' => 'required|date',
            'employee_date_of_birth' => 'date',
            'employee_address' => 'required',
            'employee_phone' => 'required|max:15|min:11',
            'employee_picture' => 'image|mimes:png,jpg,jpeg'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $employee = User::findOrFail($id);
            $employee->isDelete = 1;
            $employee->save();

            $this->message('success', 'Employee info delete successfully');
        } catch (\Exception $e) {
            $this->message('error', $e->getMessage());
        }
        return redirect()->route('employee.index');
    }

    public function profile() {
        $employee = Auth::user();
        return view('employee.profile', compact('employee'));
    }

    public function resetPassword(Request $request) {
        $request->validate([
            'current_password' => 'required',
            'new_password' => 'required|same:confirm_password',
            'confirm_password' => 'required'
        ]);

        $user = Auth::user();

        if (!Hash::check($request->current_password, $user->password)) {
            $this->message('error', 'Current password was incorrect!');
            return redirect()->back();
        }

        $user->password = bcrypt($request->confirm_password);
        $user->save();

        $this->message('success', 'Password successfully reset');
        return redirect()->back();
    }

    public function imageChange(Request $request) {
        $request->validate([
            'image' => 'required|image|mimes:png,jpg,jpeg|max:5000'
        ]);

        $user = Auth::user();

        if (!empty($user->employee_picture) && file_exists($user->employee_picture)) {
            unlink($user->employee_picture);
        }

        $imagePath = $this->imageUpload($request, 'image', 'uploads/employees');
        if ($imagePath) {
            $user->employee_picture = $imagePath;
            $user->save();

            $this->message('success', 'Image successfully changed');
            return redirect()->back();
        }
        $this->message('error', 'Something went wrong!');
        return redirect()->back();
    }
}
