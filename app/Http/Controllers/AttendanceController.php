<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\Attendance;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PDF;

class AttendanceController extends Controller
{
    public function getAttendances()
    {
        if ($this->isAdmin()) {

            $employees = User::where('isAdmin', 0)->where('isDelete', 0)->latest()->get();
            $attendances = Attendance::whereDate('in_time', date('Y-m-d'))->get();

            return view('attendance.adminShow', compact('employees', 'attendances'));

        } else {

            $attendances = Attendance::where('employee_id', Auth::id())
                ->whereMonth('in_time', date('m'))->get();

            $in_exist = Attendance::whereDate('in_time', date('Y-m-d'))
                ->where('employee_id', Auth::id())->first();
            $out_exist = Attendance::whereDate('out_time', date('Y-m-d'))
                ->where('employee_id', Auth::id())->first();

            return view('attendance.employeeShow', compact('attendances', 'in_exist', 'out_exist'));
        }
    }

    public function attendanceStore(Request $request)
    {
        if (isset($request->in)) {
            $this->attendanceIN();
        }

        if (isset($request->out)) {
            $this->attendanceOUT();
        }

        return back();
    }

    public function attendanceIN()
    {
        $check = Attendance::whereDate('in_time', date('Y-m-d'))
                ->where('employee_id', Auth::id())->count();

        if ($check) {
            return back()->with('error', '\'IN\' time already received.');
        }

        $a = new Attendance;
        $a->employee_id = Auth::id();
        $a->in_time = date('Y-m-d H:i:s');
        $a->save();

        return back()->with('success', 'Attendance received successfully.');
    }

    public function attendanceOUT()
    {
        $today = Attendance::whereDate('in_time', date('Y-m-d'))
            ->where('employee_id', Auth::id())->first();

        if (!empty($today->out_time)) {
            return back()->with('error', '\'OUT\' time already received.');
        }

        if ($today) {

            $today->out_time = date('Y-m-d H:i:s');
            $today->save();

            return back()->with('success', 'Attendance received successfully.');
        }

        return back()->with('error', '\'IN\' time not received.');
    }

    public function attendanceSearch()
    {
        $fromDate = request('from_date');
        $toDate = request('to_date');
        $employeeId = request('employee');

        if (!empty($fromDate) && !empty($toDate) && !empty($employeeId)) {

            $employee = User::where('id', $employeeId)->first();

            $attendances = Attendance::whereDate('in_time', '>=', $fromDate)
                            ->whereDate('in_time', '<=', $toDate)
                            ->where('employee_id', $employeeId)->get();

            $pdf = PDF::loadView('attendance.employeeReport', [
                'attendances' => $attendances,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'employee' => $employee
            ]);

            $fileName = $employee->employee_name . '_Attendance_' . $fromDate . '_' . $toDate;

        } else {

            $attendances = Attendance::whereDate('in_time', '>=', $fromDate)
                            ->whereDate('in_time', '<=', $toDate)->get();

            $pdf = PDF::loadView('attendance.dateToDateReport', [
                'attendances' => $attendances,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
            ]);

            $fileName = 'Attendance_List' . $fromDate . '_' . $toDate;
        }

        return $pdf->stream($fileName . '.pdf');
    }
}
