<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\Order;
use App\Models\Visit;
use App\Models\Product;
use App\Models\Customer;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function visitReport()
    {
        $employees = User::where('isAdmin', 0)->where('isDelete', 0)->latest()->get();
        return view('report.visit_report', compact('employees'));
    }

    public function visitReportProcess()
    {
        $from_date = request()->get('from_date');
        $to_date = request()->get('to_date');
        $employee_id = request()->get('employee_id');

        $from_date = date('Y-m-d', strtotime($from_date));
        $to_date = date('Y-m-d', strtotime($to_date));

        $visit_query = Visit::query();

        if ($employee_id) {
            $visit_query = $visit_query->where('saved_by', $employee_id);
        }

        $visit_query = $visit_query->whereBetween('visit_date', [$from_date, $to_date]);
        $visits = $visit_query->where('isDelete', 0)->get();

        $visits->map(function ($visit) {
            $visit->visit_date = date('d-m-Y', strtotime($visit->visit_date));
            $visit->customer = $visit->customer;
            $visit->job_type = $visit->jobType;
            $visit->company = $visit->company;
            $visit->reference_by = $visit->referenceBy;
            $visit->transport_mode = $visit->transportMode;
            $visit->saved_by = $visit->savedBy;
            $visit->saved_by->username = ucfirst($visit->savedBy->username);
            return $visit;
        });

        $employees = User::where('isAdmin', 0)->where('isDelete', 0)->latest()->get();
        return view('report.visit_report', compact('employees', 'visits'));
    }

    public function orderReport()
    {
        $employees = User::where('isAdmin', 0)->where('isDelete', 0)->latest()->get();
        $customers = Customer::where('isDelete', 0)->latest()->get();
        $products = Product::where('isDelete', 0)->latest()->get();
        return view('report.order_report', compact('employees', 'customers', 'products'));
    }

    public function orderReportProcess(Request $request)
    {
        $from_date = date('Y-m-d', strtotime($request->from_date));
        $to_date = date('Y-m-d', strtotime($request->to_date));

        $order_query = Order::with('customer', 'orderProducts.product');

        if ($request->employee_id) {
            $order_query = $order_query->where('saved_by', $request->employee_id);
        }

        if ($request->customer_id) {
            $order_query = $order_query->where('customer_id', $request->customer_id);
        }

        if ($request->product_id) {
            $productId = $request->product_id;
            $order_query = $order_query->with(['orderProducts' => function($q) use ($productId) {
                $q->where('product_id', $productId);
            }]);
        }

        $order_query = $order_query->whereBetween('order_date', [$from_date, $to_date]);
        $orders = $order_query->where('status', '!=', 3)->get();

        $orders->map(function ($order) {
            $order->order_date = date('d-m-Y', strtotime($order->order_date));
            $order->customer = $order->customer;
            $order->project = $order->project;
            $order->saved_by = $order->savedBy;
            $order->sub_total = $order->orderProducts->sum('amount');
            $order->grand_total = $order->sub_total + (($order->sub_total * $order->tax) / 100);
        });

        return $orders;
    }
}
