<?php

namespace App\Http\Controllers;

use App\Models\EmployeeDepartment;
use Illuminate\Http\Request;

class EmployeeDepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departments = EmployeeDepartment::where('isDelete', 0)->latest()->get();
        return view('setting.department.index', compact('departments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('setting.department.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'department_name' => 'required|max:180'
        ]);

        try {

            EmployeeDepartment::create($request->all());

            $this->message('success', 'Employee department added successfully');
            return redirect()->route('department.index');

        } catch (\Exception $e) {

            $this->message('error', $e->getMessage());
            return redirect()->back();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EmployeeDepartment  $employeeDepartment
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employeeDepartment = EmployeeDepartment::findOrFail($id);
        return view('setting.department.edit', compact('employeeDepartment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EmployeeDepartment  $employeeDepartment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $employeeDepartment = EmployeeDepartment::findOrFail($id);
        
        try {

            $employeeDepartment->update($request->all());

            $this->message('success', 'Employee department added successfully');
            return redirect()->route('department.index');

        } catch (\Exception $e) {

            $this->message('error', $e->getMessage());
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EmployeeDepartment  $employeeDepartment
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employeeDepartment = EmployeeDepartment::findOrFail($id);

        try {

            $employeeDepartment->update(['isDelete' => 1]);

            $this->message('success', 'Employee department delete successfully');
            return redirect()->route('department.index');

        } catch (\Exception $e) {

            $this->message('error', $e->getMessage());
            return redirect()->back();
        }
    }
}
