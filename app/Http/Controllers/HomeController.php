<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use App\Models\Order;
use App\Models\Visit;
use App\Models\Product;
use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $total_employee = 0;
        $total_product = Product::where('isDelete', 0)->count();
        if ($this->isAdmin()) {
            $total_employee = User::where([ ['isAdmin', '=', 0], ['isDelete', '==', 0] ])->count();
            $total_customer = Customer::where('isDelete', 0)->count();
            $total_visit = Visit::where('isDelete', 0)->count();
            $total_pending_order = Order::where('status', 1)->count();
            $total_archive_order = Order::where('status', 2)->count();
        } else {
            $total_customer = Customer::where('isDelete', 0)->count();
            $total_visit = Visit::where([['isDelete', 0], ['saved_by', '=', Auth::id()]])->count();
            $total_pending_order = Order::where('status', 1)->where('saved_by', Auth::id())->count();
            $total_archive_order = Order::where('status', 2)->where('saved_by', Auth::id())->count();
        }

        $this_year_orders_count = $this->thisYearMonthlySales();
        $todayNewCustomer = $this->todayNewCount(Customer::class);
        $todayNewVisit = $this->todayNewCount(Visit::class);
        $todayNewOrder = $this->todayNewCount(Order::class);
        $todayArciveOrder = $this->todayArciveOrderCount();

        return view('home', compact(
            'total_employee', 'total_product', 
            'total_customer', 'total_visit', 
            'total_pending_order', 'total_archive_order',
            'this_year_orders_count', 'todayNewCustomer',
            'todayNewVisit', 'todayNewOrder', 'todayArciveOrder'
        ));
    }

    private function todayArciveOrderCount() {
        if ($this->isAdmin()) {
            $totalCount = Order::whereDate('created_at', Carbon::today()->toDateString())
            ->where('status', 2)->count();
        } else {
            $totalCount = Order::whereDate('created_at', Carbon::today()->toDateString())
            ->where('status', 2)->where('saved_by', Auth::id())->count();
        }
        return $totalCount;
    }

    private function todayNewCount($model) {
        if ($this->isAdmin()) {
            $totalCount = $model::whereDate('created_at', Carbon::today()->toDateString())->count();
        } else {
            $totalCount = $model::whereDate('created_at', Carbon::today()->toDateString())
            ->where('saved_by', Auth::id())->count();
        }
        return $totalCount;
    }

    private function thisYearMonthlySales() {

        $_orders = Order::whereYear('created_at', Carbon::now()->year)->get();

        if ($this->isAdmin()) {
            $orders = $_orders->map(function($order) {
                return date('m', strtotime($order->created_at));
            })->toArray();
        } else {
            $orders = $_orders->map(function($order) {
                return date('m', strtotime($order->created_at));
            })->where('saved_by', Auth::id())->toArray();
        }

        sort($orders);
        $this_year_orders = array_count_values($orders);
        $monthArray = [0,0,0,0,0,0,0,0,0,0,0,0];
        
        if (count($this_year_orders)) {
            foreach ($this_year_orders as $key => $tyo) {
                $monthArray[ltrim($key, '0') - 1] = $tyo;
            }
        }

        $this_year_orders_count = '[';
        if (count($monthArray)) {
            foreach ($monthArray as $key => $tyo) {
                $this_year_orders_count .= '"' . $tyo . '",';
            }
        }

        $this_year_orders_count = rtrim($this_year_orders_count, ',');
        return $this_year_orders_count .= ']';
    }
}
