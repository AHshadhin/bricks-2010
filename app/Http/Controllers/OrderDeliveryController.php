<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\OrderProduct;
use Illuminate\Http\Request;
use App\Models\OrderDelivery;
use Illuminate\Support\Facades\Auth;

class OrderDeliveryController extends Controller
{
    public function deliveryForm($id) {

        $order                    = Order::with(['orderProducts', 'deliveryProducts'])->findOrFail($id);

        $deliveryProducts         = $order->deliveryProducts->groupBy('delivery_batch');

        return view('order_delivery.index', compact('order', 'deliveryProducts'));
    }

    public function deliveryProcess(Request $request) {

        if(!array_filter((array) $request->order_product)) {
            $this->message('error', 'Please enter a value into at least one of the fields.');
            return redirect()->back();
        }

        $o                        = Order::findOrFail($request->order_id);

        $maxDeliveryBatch         = OrderDelivery::max('delivery_batch');

        $i                        = 0;
        foreach ($request->order_product as $pro_id => $qty) {
            if (!empty($qty)) {
                OrderDelivery::create([
                    'order_id' => $request->order_id, 
                    'order_product_id' => $pro_id, 
                    'quantity' => $qty, 
                    'delivery_batch' => $maxDeliveryBatch + 1,
                    'saved_by' => Auth::id()
                ]);
                $i++;
            }
        }

        $totalDeliveryQty         = OrderDelivery::where('order_id', $request->order_id)->sum('quantity');
        $totalOrderQty            = OrderProduct::where('order_id', $request->order_id)->sum('quantity');
        
        if (($totalDeliveryQty > 0) && ($totalDeliveryQty < $totalOrderQty)) {
            $o->status            = 4;
            $o->save();
        }

        if ($totalDeliveryQty == $totalOrderQty) {
            $o                    = Order::findOrFail($request->order_id);
            $o->status            = 2;
            $o->save();
        }
        
        if ($i > 0) {
            $this->message('success', "{$i} Product delivery success.");
        }
        return redirect()->back();
    }

    public function chalan($order_id, $batch_id) {
        
        $order = Order::with(['customer', 'deliveryProducts' => function($q) use ($batch_id) {
            $q->where('delivery_batch', $batch_id);
        }])->findOrFail($order_id);

        if ($this->isOwn($order->saved_by) || $this->isAdmin()) {

            $tax                  = $order->tax;
            $chalanNumber         = $this->chalanNumber($order_id, $batch_id);
            $chalanDate         = $this->chalanDate($order_id, $batch_id);

            return view('order_delivery.chalan', compact('order', 'tax', 'chalanNumber', 'chalanDate'));
        }
        abort(403);
    }

    public function chalanNumber($order_id, $batch_id) {
        
        $od = OrderDelivery::where([
            ['order_id', '=', $order_id],
            ['delivery_batch', '=', $batch_id]
        ])->first();

        if ($od != null) {
            $n                    = date('Ymd', strtotime($od->created_at)) . $od->delivery_batch;
            return $n;
        }

        return false;
    }

    public function chalanDate($order_id, $batch_id) {
        $od = OrderDelivery::where([
            ['order_id', '=', $order_id],
            ['delivery_batch', '=', $batch_id]
        ])->first();

        if ($od != null) {
            return date('Y-m-d', strtotime($od->created_at));
        }

        return false;
    }
}
