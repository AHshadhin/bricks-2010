<?php

namespace App\Http\Controllers\Api;

use App\Models\Company;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     
    public function getcompany() {
        $companies = Company::where('isDelete', 0)->get();
        if ($companies->count() > 0) {
            return response()->json([
                'success' => true,
                'data' => $companies
            ], 200);
        }
        return response()->json([
            'success' => false,
            'message' => 'Company not found'
        ], 404);
    }

    public function addNewCompany(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()
            ]);
        }
        try {

            Company::create($request->all());

            return response()->json([
                'success' => true,
                'message' => 'Company added successfully'
            ]);

        } catch (\Exception $e) {

            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
    }
    
}
