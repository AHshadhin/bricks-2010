<?php

namespace App\Http\Controllers\Api;

use App\Models\Customer;
use App\Models\Product;
use App\Models\CustomerType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class CustomerController extends Controller
{
    public function getCustomerTypes() {
        $customer_types = CustomerType::where('isDelete', 0)->get();
        if ($customer_types->count() > 0) {
            return response()->json([
                'success' => true,
                'data' => $customer_types
            ]);
        }
        return response()->json([
            'success' => false,
            'message' => 'Customer types not found'
        ]);
    }

    public function getCustomers() {
        // if ($this->apiIsAdmin()) {
        //     $customers = Customer::where('isDelete', 0)->get();
        // } else {
        //     $customers = Customer::with('projects')
        //                 ->where('isDelete', 0)
        //                 ->where('saved_by', app('currentUserId'))
        //                 ->get();
        // }
        $customers = Customer::with('projects')->where('isDelete', 0)->get();
        $products = Product::where('isDelete', 0)->get();
        if ($customers->count() > 0) {
            return response()->json([
                'success' => true,
                'data' => $customers,
                'products' => $products
            ], 200);
        }
        return response()->json([
            'success' => false,
            'message' => 'Customers not found'
        ], 200);
    }

    public function storeCustomerInfo(Request $request) {
        
        $validator = Validator::make($request->all(), [
            'company_name' => 'required',
            'contact_person' => 'required',
            'phone' => 'required|min:11|max:15',
            'address' => 'required',
            'customer_type_id' => 'required',
            'marketing_officer_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()
            ], 200);
        }

        try {
            Customer::create([
                'customer_id' => $this->generateCustomerId(),
                'company_name' => $request->company_name,
                'contact_person' => $request->contact_person,
                'address' => $request->address,
                'phone' => $request->phone,
                'customer_type_id' => $request->customer_type_id,
                'marketing_officer_id' => $request->marketing_officer_id,
                'saved_by' => app('currentUserId')
            ]);
            
            return response()->json([
                'success' => true,
                'message' => 'Customer info save successfully.'
            ], 201);

        } catch (\Exception $e) {
            
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ], 200);
        }
    }

    public function getCustomerInfo($id) {

        $customer = Customer::with('projects')->findOrFail($id);
        $customer->saved_by = $customer->savedBy;
        $customer->updated_by = $customer->updatedBy;

        if ($customer->count() > 0) {
            return response()->json([
                'success' => true,
                'data' => $customer
            ], 200);
        }
        return response()->json([
            'success' => false,
            'message' => 'Customer not found'
        ], 404);
    }

    public function updateCustomerInfo(Request $request) {

        $validator = Validator::make($request->all(), [
            'customer_id' => 'required',
            'company_name' => 'required',
            'contact_person' => 'required',
            'phone' => 'required|min:11|max:15',
            'address' => 'required',
            'customer_type_id' => 'required',
            'marketing_officer_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()
            ], 422);
        }

        try {
            $customer = Customer::findOrFail($request->customer_id);
            $customer->company_name = $request->company_name;
            $customer->contact_person = $request->contact_person;
            $customer->phone = $request->phone;
            $customer->address = $request->address;
            $customer->customer_type_id = $request->customer_type_id;
            $customer->marketing_officer_id = $request->marketing_officer_id;
            $customer->updated_by = app('currentUserId');
            $customer->save();

            return response()->json([
                'success' => true,
                'message' => 'Customer info update successfully.'
            ], 200);

        } catch (\Exception $e) {

            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ], 422);
        }
    }

    public function deleteCustomerInfo($id) {

        try {

            $customer = Customer::findOrFail($id);
            $customer->isDelete = 1;
            $customer->save();

            return response()->json([
                'success' => true,
                'message' => 'Customer info delete successfully.'
            ], 200);

        } catch (\Exception $e) {

            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ], 422);
        }
    }
}
