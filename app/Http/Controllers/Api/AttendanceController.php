<?php

namespace App\Http\Controllers\Api;

use App\Models\Attendance;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AttendanceController extends Controller
{
    public function currentMonthAttendances() {
        $attendances = Attendance::where('employee_id', app('currentUserId'))
            ->whereMonth('in_time', date('m'))->get();

        return response()->json([
            'success' => true,
            'data' => $attendances
        ]);
    }

    public function attendanceIN(Request $request)
    {
        if (!isset($request->time) || empty($request->time)) {
            return response()->json([
                'success' => false,
                'message' => 'In time not found.'
            ]);
        }

        $check = Attendance::whereDate('in_time', date('Y-m-d'))->where('employee_id', app('currentUserId'))->count();

        if ($check) {
            return response()->json([
                'success' => false,
                'message' => 'Attendace already received'
            ]);
        }

        $a = new Attendance;
        $a->employee_id = app('currentUserId');
        $a->in_time = date('Y-m-d H:i:s', strtotime($request->time));
        $a->in_latitude = $request->latitude;
        $a->in_longitude = $request->longitude;
        $a->save();

        return response()->json([
            'success' => true,
            'message' => 'Attendance received successfully.'
        ]);
    }

    public function attendanceOUT(Request $request)
    {
        if (!isset($request->time) || empty($request->time)) {
            return response()->json([
                'success' => false,
                'message' => 'Out time not found.'
            ]);
        }

        $today = Attendance::whereDate('in_time', date('Y-m-d'))
            ->where('employee_id', app('currentUserId'))->first();

        if (!empty($today->out_time)) {
            return response()->json([
                'success' => false,
                'message' => 'OUT time already received.'
            ]);
        }

        if ($today) {

            $today->out_time = date('Y-m-d H:i:s', strtotime($request->time));
            $today->out_latitude = $request->latitude;
            $today->out_longitude = $request->longitude;
            $today->save();

            return response()->json([
                'success' => true,
                'message' => 'Attendance received successfully.'
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'IN time not received.'
        ]);
    }
}
