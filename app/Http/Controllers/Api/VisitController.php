<?php

namespace App\Http\Controllers\Api;

use App\Models\Visit;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class VisitController extends Controller
{
    public function getAllVisitInfo() {
        // if ($this->apiIsAdmin()) {
        //     $visits = Visit::where('isDelete', 0)->get();
        // } else {
        //     $visits = Visit::with([
        //         'customer', 'company', 'jobType', 
        //         'transportMode', 'referenceBy'
        //     ])->where('isDelete', 0)
        //     ->where('saved_by', app('currentUserId'))
        //     ->get();
        // }
        
        $visits = Visit::with([
            'customer', 'company', 'jobType', 
            'transportMode', 'referenceBy'
        ])->where('isDelete', 0)
        ->get();

        if ($visits->count() > 0) {
            return response()->json([
                'success' => true,
                'data' => $visits
            ], 200);
        }
        return response()->json([
            'success' => false,
            'message' => 'Visits not found'
        ], 404);
    }

    public function storeNewVisitInfo(Request $request) {
        
        $validator = Validator::make($request->all(), [
            'visit_date' => 'required',
            'company_id' => 'required',
            'customer_id' => 'required',
            'reference_by_id' => 'required',
            'job_type_id' => 'required',
            'transport_mode_id' => 'required',
            'phone' => 'required|min:11|max:15',
            'address' => 'required',
            'area' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()
            ], 200);
        }

        $location = null;
        if ($request->latitude && $request->longitude) {
            $location = $request->latitude . ',' . $request->longitude;
        }

        try {
            $v = Visit::create([
                'visit_date' => date('Y-m-d', strtotime($request->visit_date)),
                'company_id' => $request->company_id,
                'customer_id' =>$request->customer_id,
                'reference_by_id' => $request->reference_by_id,
                'job_type_id' => $request->job_type_id,
                'transport_mode_id' => $request->transport_mode_id,
                'phone' => $request->phone,
                'address' => $request->address,
                'area' => $request->area,
                'comment' => $request->comment,
                'location' => $location,
                'saved_by' => app('currentUserId')
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Visit info save successfully.'
            ], 200);

        } catch (\Exception $e) {

            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ], 422);
        }
    }

    public function getVisitInfo($id) {
        
        $visit = Visit::findOrFail($id);

        $visit->visit_date = date('D d F, Y', strtotime($visit->visit_date));
        $visit->reference_by = $visit->referenceBy;
        $visit->saved_by = $visit->savedBy;
        $visit->updated_by = $visit->updatedBy;

        return response()->json([
            'success' => true,
            'data' => $visit
        ], 200);
    }

    public function updateVisitInfo(Request $request) {

        $validator = Validator::make($request->all(), [
            'visit_id' => 'required',
            'visit_date' => 'required|date',
            'company_id' => 'required',
            'customer_id' => 'required',
            'reference_by_id' => 'required',
            'job_type_id' => 'required',
            'transport_mode_id' => 'required',
            'phone' => 'required|min:11|max:15',
            'address' => 'required',
            'area' => 'required',
            'location' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()
            ], 422);
        }

        try {
            $visit = Visit::findOrFail($request->visit_id);
            $visit->visit_date = date('Y-m-d', strtotime($request->visit_date));
            $visit->company_id = $request->company_id;
            $visit->customer_id = $request->customer_id;
            $visit->reference_by_id = $request->reference_by_id;
            $visit->transport_mode_id = $request->transport_mode_id;
            $visit->phone = $request->phone;
            $visit->address = $request->address;
            $visit->area = $request->area;
            $visit->location = $request->location;
            $visit->comment = $request->comment;
            $visit->updated_by = app('currentUserId');
            $visit->save();

            return response()->json([
                'success' => true,
                'message' => 'Visit info update successfully.'
            ], 200);

        } catch (\Exception $e) {

            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ], 422);
        }
    }

    public function deleteVisitInfo($id) {
        try {
            
            $visit = Visit::findOrFail($id);
            $visit->isDelete = 1;
            $visit->save();

            return response()->json([
                'success' => true,
                'message' => 'Visit info delete successfully.'
            ], 200);

        } catch (\Exception $e) {

            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ], 422);
        }
    }
}
