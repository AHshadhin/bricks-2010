<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\TransportMode;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class TransportModeController extends Controller
{
    public function transporttype() {
        $transports = TransportMode::where('isDelete', 0)->get();
        if ($transports->count() > 0) {
            return response()->json([
                'success' => true,
                'data' => $transports
            ], 200);
        }
        return response()->json([
            'success' => false,
            'message' => 'Transport Type not found'
        ], 404);
    }

    public function addNewTransportMode(Request $request) {
        $validator = Validator::make($request->all(), [
            'transport_type' => 'required|max:100'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()
            ]);
        }

        try {
            
            TransportMode::create([
                'transport_name' => $request->transport_type
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Transport mode save successfully'
            ]);

        } catch (\Exception $e) {
            
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
    }
    
}
