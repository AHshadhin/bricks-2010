<?php

namespace App\Http\Controllers\Api;

use App\Models\Order;
use App\Models\OrderProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class OrderController extends Controller
{
    public function getOrders(Request $req) {
        $orders = Order::with(['customer', 'project', 'orderProducts.product' => function($q) {
            return $q->select('id', 'product_name');
        }]);

        if ($req->has('customer_id') && $req->filled('customer_id')) {
            $orders->where('customer_id', $req->customer_id);
        }

        if ($this->apiIsAdmin()) {
            $orders = $orders->where('status', '!=', 3)->latest()->get();
        } else {            
            $orders = $orders->where('status', '!=', 3)->where('saved_by', app('currentUserId'))->latest()->get();
        }
        
        if ($orders->count() > 0) {
            return response()->json([
                'success' => true,
                'data' => $orders
            ]);
        }
        
        return response()->json([
            'success' => false,
            'message' => 'Orders not found'
        ]);
    }

    public function storeOrder(Request $request) {
        
        $request->cart = json_decode($request->cart, true);
        
        if (!empty($request->customer_id) && !empty($request->delivery_date) && count($request->cart) > 0) {
            
            try {

                DB::beginTransaction();

                $order = new Order;
                $order->invoice_number = $this->generateInvoiceId();
                $order->order_date = date('Y-m-d');
                $order->customer_id = $request->customer_id;
                $order->project_id = $request->project_id;
                $order->delivery_date = date('Y-m-d', strtotime($request->delivery_date));
                $order->delivery_address = $request->delivery_address;
                $order->tax = $request->tax;
                $order->saved_by = app('currentUserId');
                $order->save();

                if (is_array($request->cart) && count($request->cart) > 0) {
                    foreach ($request->cart as $key => $item) {
                        $op = new OrderProduct;
                        $op->order_id = $order->id;
                        $op->product_id = $item['product_id'];
                        $op->quantity = $item['product_quantity'];
                        $op->rate = $item['product_price'];
                        $op->amount = $item['product_price'] * $item['product_quantity'];
                        $op->save();
                    }
                }

                DB::commit();

                return response()->json([
                    'success' => true,
                    'message' => 'Order successfully confirmed.',
                    'order_id' => $order->id
                ]);
            } catch (\Exception $e) {

                DB::rollback();

                return response()->json([
                    'success' => false,
                    'message' => $e->getMessage()
                ]);
            }
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Please fill out all empty fields.'
            ]);
        }
    }


    public function updateOrder(Request $request) {

        $validator = Validator::make($request->all(), [
            'customer_id' => 'required',
            'project_id' => 'required',
            'order_id' => 'required',
            'delivery_date' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()
            ], 422);
        }

        $i = 0;
        $data = [];
        
        if (is_array($request->product_id) && count($request->product_id) > 0) {
            foreach ($request->product_id as $key => $value) {
                if (empty($value) || empty($request->product_rate[$key]) 
                || empty($request->product_quantity[$key]) 
                || empty($request->product_amount[$key])) {
                    $this->message('error', 'Fields must not be empty');
                    return redirect()->back();
                }
                $data[$i]['product_id'] = $value;
                $data[$i]['product_rate'] = $request->product_rate[$key];
                $data[$i]['product_quantity'] = $request->product_quantity[$key];
                $data[$i]['product_amount'] = $request->product_amount[$key];
                $i++;
            }
        }

        try {

            DB::beginTransaction();

            if (count($data) > 0) {
                $order = Order::findOrFail($request->order_id);
                $order->customer_id = $request->customer_id;
                $order->project_id = $request->project_id;
                $order->delivery_date = $request->delivery_date;
                $order->delivery_address = $request->delivery_address;
                $order->status = $request->status;
                $order->updated_by = app('currentUserId');
                $order->save();
    
                $order->orderProducts()->delete();
                foreach ($data as $k => $item) {
                    OrderProduct::create([
                        'order_id' => $order->id,
                        'product_id' => $item['product_id'],
                        'quantity' => $item['product_quantity'],
                        'rate' => $item['product_rate'],
                        'amount' => $item['product_amount']
                    ]);
                }
            }

            DB::commit();

            return response()->json([
                'success' => true,
                'message' => 'Order successfully updated.'
            ], 200);
            
        } catch (\Exception $e) {

            DB::rollback();

            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ], 422);
        }

        return response()->json([
            'success' => false,
            'message' => 'Something went wrong'
        ], 422);
    }


    public function generateInvoiceId(){
        $invoice = date('Y').date("m")."00001";
        $sales = Order::select('id')->latest()->first();
        if($sales != null){
            $newSalesId = $sales->id + 1;
            $zeros = array('0', '00', '000', '0000');
            $invoice = date('Y').date('m') . (strlen($newSalesId) > count($zeros) ? $newSalesId : $zeros[count($zeros) - strlen($newSalesId)] . $newSalesId);
        }
        return $invoice;
    }
    
}
