<?php

namespace App\Http\Controllers\Api;

use App\Models\CustomerType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class CustomerTypeController extends Controller
{
    public function getcustomertype() {
        $customer_types = CustomerType::where('isDelete', 0)->get();
        if ($customer_types->count() > 0) {
            return response()->json([
                'success' => true,
                'data' => $customer_types
            ], 200);
        }
        return response()->json([
            'success' => false,
            'message' => 'Customer Type not found'
        ], 404);
    }

}
