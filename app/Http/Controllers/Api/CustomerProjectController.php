<?php

namespace App\Http\Controllers\Api;

use App\Models\Customer;
use Illuminate\Http\Request;
use App\Models\CustomerProject;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class CustomerProjectController extends Controller
{
    public function getprojects($customerId) {
        $customer_projects = CustomerProject::where('customer_id', $customerId)->where('isDelete', 0)->get();
        if ($customer_projects->count() > 0) {
            return response()->json([
                'success' => true,
                'data' => $customer_projects
            ]);
        }
        return response()->json([
            'success' => false,
            'message' => 'Project List not found'
        ]);
    }

    public function storeProjectInfo(Request $request) {
        
        $validator = Validator::make($request->all(), [
            'customer' => 'required|numeric',
            'project_name' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()
            ]);
        }

        try {
            CustomerProject::create([
                'customer_id' => $request->customer,
                'project_name' => $request->project_name,
                'project_description' => $request->project_description,
                'saved_by' => app('currentUserId')
            ]);
            
            return response()->json([
                'success' => true,
                'message' => 'Customer project save successfully.'
            ]);

        } catch (\Exception $e) {
            
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
    }
}
