<?php

namespace App\Http\Controllers\Api;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class EmployeeController extends Controller
{
    public function getEmployees() {
        $employees = User::where('isAdmin', 0)->where('isDelete', 0)->get();
        if ($employees->count() > 0) {
            return response()->json([
                'success' => true,
                'data' => $employees
            ], 200);
        }
        return response()->json([
            'success' => false,
            'message' => 'Employees not found'
        ], 404);
    }

    public function getEmployee($id) {
        $employee = User::with('employee_post', 'employee_department')->findOrFail($id);
        if ($employee->count()) {
            return response()->json([
                'success' => true,
                'data' => $employee,
                'base_url' => asset('/')
            ], 200);
        }
        return response()->json([
            'success' => false,
            'message' => 'Employee not found'
        ], 404);
    }

    public function resetPassword(Request $request) {
        $validator = Validator::make($request->all(), [
            'current_password' => 'required',
            'new_password' => 'required|same:confirm_password',
            'confirm_password' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'errors' => $validator->errors()
            ], 422);
        }

        $user = User::findOrFail(app('currentUserId'));

        if (!Hash::check($request->current_password, $user->password)) {
            return response()->json([
                'success' => false,
                'message' => 'Current password was incorrect!'
            ], 422);
        }

        $user->password = bcrypt($request->confirm_password);
        $user->save();

        return response()->json([
            'success' => true,
            'message' => 'Password successfully reset'
        ], 200);
    }
}
