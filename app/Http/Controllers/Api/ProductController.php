<?php

namespace App\Http\Controllers\Api;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public function getProducts() {
        $products = Product::where('isDelete', 0)->get();
        if ($products->count() > 0) {
            return response()->json([
                'success' => true,
                'data' => $products
            ], 200);
        }
        return response()->json([
            'success' => false,
            'message' => 'Products not found'
        ], 404);
    }
}
