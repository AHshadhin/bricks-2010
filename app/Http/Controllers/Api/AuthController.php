<?php

namespace App\Http\Controllers\Api;

use App\User;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        try {
            $login_username   = User::where('username', $request->username)->where('isDelete', 0)->first();
            $login_employee   = User::where('employee_id', $request->username)->where('isDelete', 0)->first();

            if (($login_username != null) && Hash::check($request->password, $login_username->password)) {
                $iat = time(); // time of token issued at
                $nbf = $iat + 10; //not before in seconds
                $exp = $iat + 86400; // expire time of token in seconds
                $key = config('app.tokenKey');
                $token = array(
                    "iat" => $iat,
                    "nbf" => $nbf,
                    "exp" => $exp,
                    "userid" => $login_username->id,
                    "username" => $login_username->username
                );

                return response()->json([
                    'token' => JWT::encode($token, $key),
                    'username' => $login_username->username,
                    'isAdmin' => 1
                ], 200);

            }
            
            if (($login_employee != null) && Hash::check($request->password, $login_employee->password)) {
                $iat = time(); // time of token issued at
                $nbf = $iat + 10; //not before in seconds
                $exp = $iat + 86400; // expire time of token in seconds
                $key = config('app.tokenKey');
                $token = array(
                    "iat" => $iat,
                    "nbf" => $nbf,
                    "exp" => $exp,
                    "userid" => $login_employee->id,
                    "employee_name" => $login_employee->employee_name
                );

                return response()->json([
                    'token' => JWT::encode($token, $key),
                    'user_id' => $login_employee->id,
                    'employee_name' => $login_employee->employee_name,
                    'isAdmin' => 0
                ], 200);
            }

            return response()->json(["User ID or Password was incorrect"], 422);

        } catch (\Exception $ex) {

            return response()->json([
                'message' => $ex->getMessage()
            ], 422);
        }
        
    }

}
