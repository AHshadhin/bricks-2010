<?php

namespace App\Http\Controllers\Api;

use App\Models\JobType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;


class JobTypeController extends Controller
{
    public function getjobtype() {
        $job_types = JobType::where('isDelete', 0)->get();
        if ($job_types->count() > 0) {
            return response()->json([
                'success' => true,
                'data' => $job_types
            ], 200);
        }
        return response()->json([
            'success' => false,
            'message' => 'Job Type not found'
        ], 404);
    }

    public function addNewJobType(Request $request) {

        $validator = Validator::make($request->all(), [
            'job_type' => 'required|max:100'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()
            ]);
        }

        try {

            JobType::create([
                'type' => $request->job_type
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Job type save successfully'
            ]);

        } catch (\Exception $e) {

            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
    }
}
