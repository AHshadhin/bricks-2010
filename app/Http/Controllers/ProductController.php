<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class ProductController extends Controller
{
    public function index() {
        $products = Product::where('isDelete', 0)->latest()->get();
        return view('product.index', compact('products'));
    }

    public function create() {
        return view('product.create', [
            'new_id' => $this->generateProductId()
        ]);
    }

    public function generateProductId() {
        $oldCodes = Product::pluck('product_id');
        if (count(array_filter((array) $oldCodes)) > 0) {
            if ($oldCodes->max()) {
                $oldMaxCode = preg_replace('/[A-Z]+/', '', $oldCodes->max());
                $prefix = preg_replace('/[0-9]+/', '', $oldCodes->max());
                $newCode = $prefix . (str_pad($oldMaxCode + 1, 4, 0, STR_PAD_LEFT));
            } else {
                $newCode = 'P0001';
            }
        } else {
            $newCode = 'P0001';
        }
        return $newCode;
    }

    public function store(Request $request) {
        $request->validate([
            'product_name' => 'required',
            'product_price' => 'required|numeric'
        ]);

        try {
            Product::create([
                'product_id' => $this->generateProductId(),
                'product_name' => $request->product_name,
                'product_price' => $request->product_price,
                'saved_by' => Auth::id(),
            ]);
            $this->message('success', 'Product info save successfully.');
        } catch (\Exception $e) {
            $this->message('error', $e->getMessage());
        }

        return redirect()->back();
    }

    public function show($id) {
        return Product::findOrFail($id);
    }

    public function edit($id) {
        $product = Product::findOrFail($id);
        return view('product.edit', compact('product'));
    }

    public function update(Request $request, $id) {
        $request->validate([
            'product_name' => 'required',
            'product_price' => 'required|numeric'
        ]);

        try {
            $product = Product::findOrFail($id);
            $product->update([
                'product_name' => $request->product_name,
                'product_price' => $request->product_price,
                'updated_by' => Auth::id(),
            ]);
            $this->message('success', 'Product info update successfully.');
        } catch (\Exception $e) {
            $this->message('error', $e->getMessage());
        }

        return redirect()->route('product.index');
    }

    public function destroy($id) {
        if (Gate::allows('admin-only', Auth::user())) {
            try {
                $product = Product::findOrFail($id);
                $product->isDelete = 1;
                $product->save();
                $this->message('success', 'Product info delete successfully');
            } catch (\Exception $e) {
                $this->message('error', $e->getMessage());
            }
            return redirect()->route('product.index');
        }
        return redirect()->route('home');
    }
}
