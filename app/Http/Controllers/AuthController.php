<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class AuthController extends Controller
{
    public function showLoginForm() {
        if (Auth::check()) {
            return redirect()->route('home');
        }
        return view('auth.login');
    }

    public function loginProcess(Request $request) {

        $request->validate([
            'user' => 'required',
            'password' => 'required|min:6'
        ]);

        $login_username   = User::where('username', $request->user)->where('isDelete', 0)->first();

        if (($login_username != null) && Hash::check($request->password, $login_username->password)) {
            Auth::login($login_username);
            return redirect()->intended('home');
        }

        $login_employee   = User::where('employee_id', $request->user)->where('isDelete', 0)->first();

        if (($login_employee != null) && Hash::check($request->password, $login_employee->password)) {
            Auth::login($login_employee);
            return redirect()->intended('home');
        }

        Session::flash('error', 'User ID or Password was incorrect!');
        return back()->withInput($request->only('user'));
    }

    public function logoutProcess() {
        Auth::logout();
        return redirect()->route('login');
    }
}
