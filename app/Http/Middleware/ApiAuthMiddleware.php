<?php

namespace App\Http\Middleware;

use Closure;
use Firebase\JWT\JWT;

class ApiAuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            
            $key = config('app.tokenKey');

            // $jwt = $request->header('Authorization');
            $jwt = $request->bearerToken();

            if (!$jwt) {
                return response()->json(['message' => 'Authorization required'], 403);
            }

            JWT::$leeway = 60; // $leeway in seconds
            $decoded = JWT::decode($jwt, $key, array('HS256'));

            app()->singleton('currentUserId', function () use ($decoded) {
                return $decoded->userid;
            });

            return $next($request);

        } catch (\Exception $ex) {
            
            return response()->json($ex->getMessage());
        }
    }
}
